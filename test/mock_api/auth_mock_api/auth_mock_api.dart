import 'dart:convert';

import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;
import '../../test_constants.dart';
import 'auth_mock_response.dart';

String baseUrl = TestConstants.baseUrl;

class AuthMockApi {

  static void requestMobileOtp(http.Client mockHttpClient) {
    when(mockHttpClient.post(
        Uri.parse("$baseUrl/user_management/customer/auth/login"),
        headers: TestConstants.defaultHeaders,
        body: json.encode({
          "customer": {
            "mobile_number": "01111111111",
            "country_code_id": null
          }
        }))).thenAnswer(
          (inv) async => http.Response(
        AuthMockResponse.requestMobileOtpResponse,
        201,
        request: http.Request('post',
            Uri.parse("$baseUrl/user_management/customer/auth/login")),
      ),
    );
  }

  static void verifyMobileOtp(http.Client mockHttpClient) {
    when(mockHttpClient.post(
        Uri.parse("$baseUrl/user_management/customer/auth/verify_login_otp"),
        headers: TestConstants.defaultHeaders,
        body: json.encode({
          "customer": {
            "mobile_number": "01111111111",
            "country_code_id": null,
            "login_otp": "1007",
            "grant_type": "login_otp"
          }
        }))).thenAnswer(
          (inv) async => http.Response(
            AuthMockResponse.approvedCustomerDetails,
          201,
          request: http.Request(
              'post',
              Uri.parse(
                  "$baseUrl/user_management/customer/auth/verify_login_otp")),
        ),
    );
  }

  static void sendDeviceID(http.Client mockHttpClient) {
    when(mockHttpClient.post(
        Uri.parse("$baseUrl/user_management/customer/device"),
        headers: TestConstants.allHeaders,
        encoding: null,
        body: json.encode({
          "device":{"device_id":"","platform":"android"}
        }))).thenAnswer(
          (inv) async => http.Response(
        '{}',
        201,
        request: http.Request('post',
            Uri.parse("$baseUrl/user_management/customer/device")),
      ),
    );
  }

  static void getCountryCodeMeta(http.Client mockHttpClient) {
    when(mockHttpClient.get(
        Uri.parse("$baseUrl/meta/country_codes"),
        headers: TestConstants.defaultHeaders,
        )).thenAnswer(
          (inv) async => http.Response(
        '{}',
        200,
        request: http.Request('get',
            Uri.parse("$baseUrl/meta/country_codes")),
      ),
    );
  }

  static void logoutUser(http.Client mockHttpClient) {
    when(mockHttpClient.delete(
      Uri.parse("$baseUrl/user_management/customer/auth/logout"),
      headers: TestConstants.allHeaders,
    )).thenAnswer(
          (inv) async => http.Response(
        '',
        204,
        request: http.Request('delete',
            Uri.parse("$baseUrl/user_management/customer/auth/logout")),
      ),
    );
  }
}

