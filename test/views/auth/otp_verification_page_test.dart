library otp_verification_page_test;

import 'package:flutter_test_coe/data/api/api_routes.dart';
import 'package:flutter_test_coe/data/app_repository.dart';
import 'package:flutter_test_coe/data/preference_client.dart';
import 'package:flutter_test_coe/middleware/middleware.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:flutter_test_coe/reducers/reducers.dart';
import 'package:flutter_test_coe/views/auth/otp_verification_page.dart';
import 'package:flutter_test_coe/views/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../mock_api/auth_mock_api/auth_mock_api.dart';
import '../../wrapperWidget.dart';
import 'package:http/http.dart' as http;

import 'otp_verification_page_test.mocks.dart';

@GenerateMocks([http.Client])
void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  AutomatedTestWidgetsFlutterBinding.ensureInitialized();
  late Store<AppState> store;
  final mockHttpClient = MockClient();

  setUp(() async {
    const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getAll') {
        return <String, dynamic>{}; // set initial values here if desired
      }
      return null;
    });
    SharedPreferences.setMockInitialValues({});
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final AppRepository repository = AppRepository(
        httpClient: mockHttpClient,
        preferencesClient: PreferencesClient(prefs: prefs),
        config: ApiRoutes.testConfig);

    store = Store<AppState>(
      reducer,
      middleware: middleware(repository),
      initialState: AppState.initState(),
    );
  });

  group('OTP Verification Screen test', () {

    testWidgets('Invalid otp should throw validation error', (tester) async {

      ///wrapped with another async block as there is a timer initialized in the otp screen
      ///fix added as a workaround to avoid issues with fake timers running
      ///in the background after the test is completed
      await tester.runAsync(() async {
        await tester.pumpWidget(makeTestableWidget(
            store: store,
            child: const OtpVerification(isEmail: false,
              actualMobileNo  : "01111111111", mobileNo: "01***1111",)));

        await tester.pump(const Duration(seconds: 1));

        ///get the widgets (text field and button)
        Finder otpField = find.byKey(const Key('otp_field'));
        Finder button = find.byKey(const Key('submit_otp'));

        await tester.enterText(otpField, "12");

        await tester.pump(const Duration(seconds: 1));

        await tester.tap(button);

        await tester.pump(const Duration(seconds: 1));

        expect(find.text('Invalid OTP'), findsOneWidget);
      });

    });

    testWidgets('Valid OTP should not throw any validation error', (tester) async {
      await tester.pumpWidget(makeTestableWidget(
          store: store,
          child: const OtpVerification(isEmail: false,
            actualMobileNo  : "01111111111", mobileNo: "01***1111",)));

      await tester.pump(const Duration(seconds: 1));

      ///get the widgets (text field and button)
      Finder otpField = find.byKey(const Key('otp_field'));
      Finder button = find.byKey(const Key('submit_otp'));

      await tester.enterText(otpField, "1007");

      await tester.pump(const Duration(seconds: 1));

      AuthMockApi.verifyMobileOtp(mockHttpClient);
      AuthMockApi.sendDeviceID(mockHttpClient);
      AuthMockApi.getCountryCodeMeta(mockHttpClient);

      await tester.tap(button);

      await tester.pump();

      expect(find.byType(HomePage, skipOffstage: false), findsOneWidget);
      
      expect(find.text('Hello! Test', skipOffstage: false), findsOneWidget);
    });

  });
}