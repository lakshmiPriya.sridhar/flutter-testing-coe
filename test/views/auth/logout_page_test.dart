library logout_page_test;
import 'package:flutter_test_coe/data/api/api_routes.dart';
import 'package:flutter_test_coe/data/app_repository.dart';
import 'package:flutter_test_coe/data/preference_client.dart';
import 'package:flutter_test_coe/middleware/middleware.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:flutter_test_coe/reducers/reducers.dart';
import 'package:flutter_test_coe/views/auth/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_test_coe/views/home/home_page.dart';
import 'package:flutter_test_coe/views/home/home_tab/home_tab_page.dart';
import 'package:mockito/annotations.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../../mock_api/auth_mock_api/auth_mock_api.dart';
import '../../test_constants.dart';
import '../../wrapperWidget.dart';
import 'logout_page_test.mocks.dart';

@GenerateMocks([http.Client])
void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  AutomatedTestWidgetsFlutterBinding.ensureInitialized();
  late Store<AppState> store;
  final mockHttpClient = MockClient();

  setUp(() async {
    const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getAll') {
        return <String, dynamic>{
          'token': TestConstants.userAccessToken
        }; // set initial values here if desired
      }
      return null;
    });
    SharedPreferences.setMockInitialValues({
      'token': TestConstants.userAccessToken
    });
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final AppRepository repository = AppRepository(
        httpClient: mockHttpClient,
        preferencesClient: PreferencesClient(prefs: prefs),
        config: ApiRoutes.testConfig);

    store = Store<AppState>(
      reducer,
      middleware: middleware(repository),
      initialState: AppState.initState(),
    );
  });

  group('Logout user', () {
    testWidgets('User should be logged out when the LogOut button is pressed from profile screen',
            (tester) async {
      await tester.pumpWidget(makeTestableWidget(
          store: store,
          child: const HomeTabPage()));

      await tester.pump(const Duration(seconds: 1));

      Finder button = find.byKey(const Key('logout_button'), skipOffstage: false);

      ///scroll down to find the logout button
      await tester.scrollUntilVisible(
        button,
        1400.0,
        scrollable: find.byType(Scrollable),
      );
      await tester.pumpAndSettle();

      //Note: the following drag method can be used if the above method doesn't work:
      //      ///scroll down to find the logout button
      //       Finder scroll = await find.byKey(Key('scroll_view'));
      //       await tester.drag(scroll, Offset(0, -1000));

      expect(find.text('Logout'), findsOneWidget);

      AuthMockApi.logoutUser(mockHttpClient);
      await tester.tap(button);
      await tester.pump();

      expect(find.byType(LoginPage, skipOffstage: false), findsOneWidget);
    });
  });
}
