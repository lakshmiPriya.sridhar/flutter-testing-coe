library login_page_test;
import 'package:flutter_test_coe/data/api/api_routes.dart';
import 'package:flutter_test_coe/data/app_repository.dart';
import 'package:flutter_test_coe/data/preference_client.dart';
import 'package:flutter_test_coe/middleware/middleware.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:flutter_test_coe/reducers/reducers.dart';
import 'package:flutter_test_coe/views/auth/login_page.dart';
import 'package:flutter_test_coe/views/auth/otp_verification_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../../mock_api/auth_mock_api/auth_mock_api.dart';
import '../../wrapperWidget.dart';
import 'login_page_test.mocks.dart';

@GenerateMocks([http.Client])
void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();
  AutomatedTestWidgetsFlutterBinding.ensureInitialized();
  late Store<AppState> store;
  final mockHttpClient = MockClient();

  setUp(() async {
    const MethodChannel('plugins.flutter.io/shared_preferences')
        .setMockMethodCallHandler((MethodCall methodCall) async {
      if (methodCall.method == 'getAll') {
        return <String, dynamic>{}; // set initial values here if desired
      }
      return null;
    });
    SharedPreferences.setMockInitialValues({});
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final AppRepository repository = AppRepository(
        httpClient: mockHttpClient,
        preferencesClient: PreferencesClient(prefs: prefs),
        config: ApiRoutes.testConfig);

    store = Store<AppState>(
      reducer,
      middleware: middleware(repository),
      initialState: AppState.initState(),
    );
  });

  group('Login Screen test', () {
    testWidgets('Invalid mobile number should throw validation error',
        (tester) async {
      await tester.pumpWidget(makeTestableWidget(
          store: store,
          child: const LoginPage()));

      await tester.pump(const Duration(seconds: 1));

      ///get the widgets (text field and button)
      Finder mobileNumber = find.byKey(const Key('mobile_number'));
      Finder button = find.byKey(const Key('login_button'));
      // print("Getting form widget");
      // Finder formWidgetFinder = find.byType(Form, skipOffstage: false);
      // Form formWidget = tester.widget(formWidgetFinder) as Form;
      // GlobalKey<FormState> formKey = formWidget.key as GlobalKey<FormState>;
      // //expect((tester.widget(loginButton) as PrimaryButton).enabled, isFalse);

      // await tester.pump(Duration(seconds: 1));

      await tester.enterText(mobileNumber, "");

      await tester.pump(const Duration(seconds: 1));

      await tester.tap(button);

      await tester.pump(const Duration(seconds: 1));

      expect(find.text('Error! Enter valid Mobile Number'), findsOneWidget);
    });

    testWidgets(
        'Valid mobile number should login the user and navigate to otp '
        'verification screen', (tester) async {
      await tester.pumpWidget(makeTestableWidget(
          store: store,
          child: const LoginPage()));

      await tester.pump(const Duration(seconds: 1));

      Finder mobileNumber = find.byKey(const Key('mobile_number'));
      Finder button = find.byKey(const Key('login_button'));

      /// print("Getting form widget");
      Finder formWidgetFinder = find.byType(Form, skipOffstage: false);
      Form formWidget = tester.widget(formWidgetFinder) as Form;
      GlobalKey<FormState> formKey = formWidget.key as GlobalKey<FormState>;

      await tester.pump(const Duration(seconds: 1));

      await tester.enterText(mobileNumber, "01111111111");

      await tester.pump(const Duration(seconds: 1));

      expect(find.text('01111111111'), findsOneWidget);

      expect(formKey.currentState?.validate(), isTrue);

      AuthMockApi.requestMobileOtp(mockHttpClient);

      await tester.tap(button);
      await tester.pump();

      expect(find.byType(OtpVerification, skipOffstage: false), findsOneWidget);

      expect(find.text('Verification', skipOffstage: false), findsOneWidget);

      ///country code is null, so default value will be displayed
      ///(no country code logic in middleware to get the masked mobile number)
      expect(find.text('01***1111', skipOffstage: false), findsOneWidget);
    });
  });
}
