import 'package:flutter_test_coe/models/app_state.dart';
import 'package:flutter_test_coe/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';



Widget makeTestableWidget(
    {required Store<AppState> store,
    required Widget child}) {
  return MediaQuery(
    data: const MediaQueryData(),
    child: StoreProvider<AppState>(
        store: store,
        child: MaterialApp(
          navigatorKey: store.state.navigator,
          theme: themeData,
          home: SafeArea(child: child),
          debugShowCheckedModeBanner: false,
        )),
  );
}
