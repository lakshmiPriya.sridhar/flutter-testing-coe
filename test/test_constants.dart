class TestConstants {
  static const String baseUrl = "http://localhost.test:443/api/v1";

  static const Map<String, String> defaultHeaders = <String, String>{
    'Content-Type': 'application/json'
  };

  static const Map<String, String> authHeaders = <String, String>{
    'Authorization': 'Bearer YjaSOH01XZ-3Yz-vtP6mTJVEbbBhNtIyXXhTBNc6AJE'
  };

  static Map<String, String> allHeaders = <String, String>{}
    ..addAll(defaultHeaders)
    ..addAll(authHeaders);

  static const userAccessToken = 'YjaSOH01XZ-3Yz-vtP6mTJVEbbBhNtIyXXhTBNc6AJE';

}