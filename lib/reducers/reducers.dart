import 'package:flutter_test_coe/models/models.dart';
import 'package:flutter_test_coe/reducers/auth/auth_reducer.dart';
import 'package:redux/redux.dart';

Reducer<AppState> reducer = combineReducers(<Reducer<AppState>>[
  authReducer,
]);
