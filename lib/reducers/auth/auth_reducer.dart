import 'package:flutter_test_coe/actions/actions.dart';
import 'package:flutter_test_coe/models/app_state.dart';
import 'package:redux/redux.dart';

Reducer<AppState> authReducer = combineReducers(<Reducer<AppState>>[
  TypedReducer<AppState, SaveNeedRestart>(saveNeedRestart),
  TypedReducer<AppState, SetLoader>(setLoader),
  TypedReducer<AppState, SetInitializer>(setInitializer),
  TypedReducer<AppState, SaveUser>(setUser),
  TypedReducer<AppState, LogOutUser>(logOutUser),
  TypedReducer<AppState, SaveCountryCodeMeta>(saveCountryCodeMeta),
  TypedReducer<AppState, SaveUserCountryCode>(saveUserCountryCode),
  TypedReducer<AppState, SaveAppLanguage>(saveAppLanguage),
]);

AppState saveNeedRestart(AppState state, SaveNeedRestart action) {
  final AppStateBuilder b = state.toBuilder();
  b.needRestart = action.needRestart;
  return b.build();
}

AppState saveAppLanguage(AppState state, SaveAppLanguage action) {
  final AppStateBuilder b = state.toBuilder();
  b.locale = action.locale;
  return b.build();
}

//**************************** manage loading status **************************//
AppState setLoader(AppState state, SetLoader action) {
  final AppStateBuilder b = state.toBuilder();
  b.isLoading = action.isLoading;
  return b.build();
}

//**************************** manage initializer status **********************//
AppState setInitializer(AppState state, SetInitializer action) {
  final AppStateBuilder b = state.toBuilder();
  b.isInitializing = action.isInitializing;
  return b.build();
}

//********************************* save-user *********************************//
AppState setUser(AppState state, SaveUser action) {
  final AppStateBuilder b = state.toBuilder();
  b.currentUser = action.userDetails!.toBuilder();
  return b.build();
}

//***************************** log-out ***************************************//
AppState logOutUser(AppState state, LogOutUser action) {
  final AppStateBuilder b = state.toBuilder();
  b
    ..isInitializing = false
    ..currentUser = null;
  return b.build();
}

//****************************** save-country-code-meta ***************************//
AppState saveCountryCodeMeta(AppState state, SaveCountryCodeMeta action) {
  final AppStateBuilder b = state.toBuilder();
  b.countryCodeMeta = action.countryCodeMeta?.toBuilder();
  return b.build();
}

//****************************** save-flags-list ***************************//
AppState saveUserCountryCode(AppState state, SaveUserCountryCode action) {
  final AppStateBuilder b = state.toBuilder();
  b.userCountryCode = action.userCountryCode;
  return b.build();
}

