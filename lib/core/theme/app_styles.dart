import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter/material.dart';

class AppStyle {

  static Locale? locale;
  //**************************** white - color ********************************//

  static TextStyle white16RegularTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'RegularArabic' : 'Regular', fontSize: 16, color: Colors.white);

  static TextStyle white16BoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'BoldArabic' :'Bold', fontSize: 16, color: Colors.white);

  static TextStyle white16ExtraBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraBoldArabic' :'ExtraBold', fontSize: 16, color: Colors.white);

  static TextStyle white16HeavyTextStyle =
  const TextStyle(fontFamily: 'Heavy', fontSize: 16, color: Colors.white);

  static TextStyle white16LightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'LightArabic' :'Light', fontSize: 16, color: Colors.white);

  static TextStyle white16MediumTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'MediumArabic' :'Medium', fontSize: 16, color: Colors.white);

  static TextStyle white16MediumItalicTextStyle =
  const TextStyle(fontFamily: 'MediumItalic', fontSize: 16, color: Colors.white);

  static TextStyle white16SemiBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'SemiBoldArabic' :'SemiBold', fontSize: 16, color: Colors.white);

  static TextStyle white16ThinTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ThinArabic' :'Thin', fontSize: 16, color: Colors.white);

  static TextStyle white16UltraLightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraLightArabic' :'UltraLight', fontSize: 16, color: Colors.white);


  //**************************** black - color ********************************//

  static TextStyle black16RegularTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'RegularArabic' :'Regular', fontSize: 16, color: Colors.black);

  static TextStyle black16BoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'BoldArabic' : 'Bold', fontSize: 16, color: Colors.black);

  static TextStyle black16ExtraBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraBoldArabic' : 'ExtraBold', fontSize: 16, color: Colors.black);

  static TextStyle black16HeavyTextStyle =
  const TextStyle(fontFamily: 'Heavy', fontSize: 16, color: Colors.black);

  static TextStyle black16LightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'LightArabic' : 'Light', fontSize: 16, color: Colors.black);

  static TextStyle black16MediumTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'MediumArabic' : 'Medium', fontSize: 16, color: Colors.black);

  static TextStyle black16MediumItalicTextStyle =
  const TextStyle(fontFamily: 'MediumItalic', fontSize: 16, color: Colors.black);

  static TextStyle black16SemiBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'SemiBoldArabic' : 'SemiBold', fontSize: 16, color: Colors.black);

  static TextStyle black16ThinTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ThinArabic' : 'Thin', fontSize: 16, color: Colors.black);

  static TextStyle black16UltraLightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraLightArabic' : 'UltraLight', fontSize: 16, color: Colors.black);


  //***************************** grey - color ********************************//

  static TextStyle grey16RegularTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'RegularArabic' :'Regular', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16BoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'BoldArabic' : 'Bold', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16ExtraBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraBoldArabic' : 'ExtraBold', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16HeavyTextStyle =
  const TextStyle(fontFamily: 'Heavy', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16LightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'LightArabic' : 'Light', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16MediumTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'MediumArabic' : 'Medium', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16MediumItalicTextStyle =
  const TextStyle(fontFamily: 'MediumItalic', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16SemiBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'SemiBoldArabic' : 'SemiBold', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16ThinTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ThinArabic' : 'Thin', fontSize: 16, color: AppColors.subTextColor);

  static TextStyle grey16UltraLightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraLightArabic' : 'UltraLight', fontSize: 16, color: AppColors.subTextColor);


  //**************************** theme - color *******************************//

  static TextStyle theme16RegularTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'RegularArabic' :'Regular', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16BoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'BoldArabic' : 'Bold', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16ExtraBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraBoldArabic' : 'ExtraBold', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16HeavyTextStyle =
  const TextStyle(fontFamily: 'Heavy', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16LightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'LightArabic' : 'Light', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16MediumTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'MediumArabic' :'Medium', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16MediumItalicTextStyle =
  const TextStyle(fontFamily: 'MediumItalic', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16SemiBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'SemiBoldArabic' : 'SemiBold', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16ThinTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ThinArabic' : 'Thin', fontSize: 16, color: AppColors.mainTextColor);

  static TextStyle theme16UltraLightTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'ExtraLightArabic' : 'UltraLight', fontSize: 16, color: AppColors.mainTextColor);

  //**************************** dark1 - color *******************************//

  static TextStyle dark17BoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'BoldArabic' : 'Bold', fontSize: 17, color: AppColors.dark1);

  static TextStyle dark14SemiBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'SemiBoldArabic' : 'SemiBold', fontSize: 14, color: AppColors.dark1);

  static TextStyle dark018BoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'BoldArabic' : 'Bold', fontSize: 18, color: AppColors.dark0);

  static TextStyle dark214MediumTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'MediumArabic' : 'Medium', fontSize: 14, color: AppColors.dark2);

  static TextStyle primary15SemiBoldTextStyle =
  TextStyle(fontFamily: locale?.countryCode == 'ar' ? 'SemiBoldArabic' : 'SemiBold', fontSize: 15, color: AppColors.themePrimaryColor);

}
