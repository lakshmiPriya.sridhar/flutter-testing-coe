class AppAssets {
  AppAssets._();

  static const String appLogo = 'assets/logo_icon.png';
  static const String phone = 'assets/png_icons/brand_colour/phone.png';
  static const String email = 'assets/png_icons/brand_colour/email.png';
  static const String camera = 'assets/png_icons/brand_colour/camera.png';
  static const String darkCamera = 'assets/svg_icons/dark/camera.svg';
  static const String darkGallery = 'assets/png_icons/dark/gallery.png';
  static const String whiteLogo = 'assets/color_logo_no_background.png';
  static const String whiteLogoSvg = 'assets/color_logo___no_background.svg';
  static const String blackLogo = 'assets/black_logo_no_background.png';
  static const String upload = 'assets/png_icons/grey/upload.png';
  static const String download = 'assets/png_icons/grey/download.png';
  static const String infoPng = 'assets/png_icons/grey/info.png';
  static const String dropdownArrow =
      'assets/png_icons/grey/dropdown_arrow.png';
  static const String doc =
      'assets/png_icons/dark/document.png';
  static const String notification = 'assets/png_icons/grey/bell.png';
  static const String backPng = 'assets/png_icons/dark/back.png';

  //********************* splash screen assets **********************//
  static const String pay = 'assets/splash_screen_assets/pay.svg';
  static const String save = 'assets/splash_screen_assets/save.svg';
  static const String shop = 'assets/splash_screen_assets/shop.svg';
  static const String logoWithName = 'assets/splash_screen_assets/logo_with_name.svg';
  static const String backgroundImage = 'assets/splash_screen_assets/background.png';



  //********************* svg icons brand - color folder **********************//
  static const String add = 'assets/svg_icons/brand_colour/add.svg';
  static const String back = 'assets/svg_icons/dark/back.svg';
  static const String bell = 'assets/svg_icons/dark/bell.svg';
  static const String bellGrey = 'assets/svg_icons/grey/bell.svg';
  static const String calender = 'assets/svg_icons/brand_colour/calender.svg';
  static const String cameraSvg = 'assets/svg_icons/brand_colour/camera.svg';
  static const String close = 'assets/svg_icons/brand_colour/close.svg';
  static const String customers = 'assets/svg_icons/brand_colour/customers.svg';
  static const String dashboard = 'dashboard/.svg';
  static const String document = 'assets/svg_icons/brand_colour/document.svg';
  static const String documentsCustomer =
      'assets/svg_icons/dark/douments - customers.svg';
  static const String edit = 'assets/svg_icons/dark/edit.svg';
  static const String contactUs = 'assets/png_icons/dark/contact.png';
  static const String contactUsSvg = 'assets/svg_icons/dark/contact.svg';
  static const String privacyPolicy = 'assets/svg_icons/dark/external link.svg';
  static const String emailSvg = 'assets/svg_icons/brand_colour/email.svg';
  static const String whatsApp = 'assets/svg_icons/other_assets/whatsapp.svg';
  static const String filter = 'assets/svg_icons/brand_colour/filter.svg';
  static const String help = 'assets/svg_icons/brand_colour/help.svg';
  static const String info = 'assets/svg_icons/grey/info.svg';
  static const String infoTheme = 'assets/svg_icons/brand_colour/info.svg';
  static const String logout = 'assets/svg_icons/brand_colour/logout.svg';
  static const String map = 'assets/svg_icons/brand_colour/map.svg';
  static const String minus = 'assets/svg_icons/brand_colour/minus.svg';
  static const String more = 'assets/svg_icons/brand_colour/more.svg';
  static const String newPlus = 'assets/svg_icons/brand_colour/new.svg';
  static const String paymentSvg = 'assets/svg_icons/brand_colour/payment.svg';
  static const String phoneSvg = 'assets/svg_icons/brand_colour/phone.svg';
  static const String phoneSvgDark = 'assets/svg_icons/dark/phone.svg';
  static const String resendOtp = 'assets/svg_icons/grey/resend otp.svg';
  static const String search = 'assets/svg_icons/brand_colour/search.svg';
  static const String searchGrey = 'assets/svg_icons/grey/search.svg';
  static const String setting = 'assets/svg_icons/brand_colour/setting.svg';
  static const String settlement =
      'assets/svg_icons/brand_colour/settlement.svg';
  static const String star = 'assets/svg_icons/brand_colour/star.svg';
  static const String transactionHistory =
      'assets/svg_icons/dark/transaction history.svg';
  static const String trash = 'assets/svg_icons/brand_colour/trash.svg';
  static const String uploadSvg = 'assets/svg_icons/brand_colour/upload.svg';
  static const String userConfig =
      'assets/svg_icons/brand_colour/user config.svg';
  static const String vendor = 'assets/svg_icons/dark/vendor.svg';

  //********************* other assets folder **********************//
  static const String playStore =
      'assets/png_icons/other_assets/play_store.png';
  static const String appStore = 'assets/svg_icons/other_assets/app store.svg';
  static const String violetCard =
      'assets/svg_icons/other_assets/buy_credit_illustration.svg';
  static const String lamp = 'assets/svg_icons/other_assets/ceiling-lamp.svg';
  static const String coverPicHolder =
      'assets/svg_icons/other_assets/cover pic placeholder.svg';
  static const String docs = 'assets/svg_icons/other_assets/documents.svg';
  static const String yellowFace =
      'assets/svg_icons/other_assets/face inside circle.svg';
  static const String itemsBag =
      'assets/svg_icons/other_assets/items purchased.svg';
  static const String profileIllusion =
      'assets/svg_icons/other_assets/my_profile_illustration.svg';
  static const String buyCreditIllustration =
      'assets/svg_icons/other_assets/buy_credit_illustration.svg';
  static const String notifications =
      'assets/svg_icons/other_assets/notifications.svg';
  static const String passwordReset =
      'assets/svg_icons/other_assets/password reset link sent.svg';
  static const String payment = 'assets/svg_icons/other_assets/payment.svg';
  static const String failedPayment = 'assets/svg_icons/other_assets/installment payment failed.svg';
  static const String successPopUp = 'assets/svg_icons/other_assets/Success transaction bottom sheet design.svg';
  static const String failurePopUp = 'assets/svg_icons/other_assets/Failed transaction bottom sheet design.svg';
  static const String photoCamera =
      'assets/svg_icons/other_assets/photo-camera.svg';
  static const String photoNotBlur =
      'assets/svg_icons/other_assets/blurry.svg';
  static const String qrImage = 'assets/svg_icons/other_assets/qr.svg';
  static const String refund = 'assets/svg_icons/other_assets/refund.svg';
  static const String cardWithYellowBg =
      'assets/svg_icons/other_assets/solid bg.svg';
  static const String sparkLeft =
      'assets/svg_icons/other_assets/sparkle left.svg';
  static const String sparkRight =
      'assets/svg_icons/other_assets/sparkle right.svg';
  static const String tc =
      'assets/svg_icons/other_assets/terms and conditions.svg';
  static const String underVerify =
      'assets/svg_icons/other_assets/pending account.svg';
  static const String usage = 'assets/svg_icons/other_assets/usage.svg';
  static const String appLang = 'assets/svg_icons/other_assets/language.svg';
  static const String homePageBackground = 'assets/png_icons/other_assets/home_page_background.png';
  static const String buyCreditIllustrationPng = 'assets/png_icons/other_assets/buy_credit_illustration.png';
  static const String returnAsset = 'assets/png_icons/other_assets/return.png';
  static const String badge = 'assets/svg_icons/other_assets/badge.svg';
  static const String locationPin = 'assets/svg_icons/other_assets/location-pin.svg';
  static const String money = 'assets/svg_icons/other_assets/money.svg';
  static const String smartphone = 'assets/svg_icons/other_assets/smartphone.svg';
  static const String age18 = 'assets/svg_icons/other_assets/age-18.svg';
  static const String digifiedVerificationFailed = 'assets/svg_icons/other_assets/digified-verification-failed.svg';
  static const String idCard = 'assets/svg_icons/other_assets/id.svg';
  static const String calendar = 'assets/svg_icons/other_assets/calendar.svg';
  static const String percentage = 'assets/svg_icons/other_assets/percentage.svg';
  static const String qrCode = 'assets/svg_icons/other_assets/qr-code.svg';
  static const String shoppingCart = 'assets/svg_icons/other_assets/shopping-cart.svg';
  static const String lock = 'assets/svg_icons/other_assets/lock.svg';
  static const String warning = 'assets/svg_icons/other_assets/warning.svg';

  //********************* JSON assets folder **********************//
  static const String calPurchasePower =
      'assets/JSON/Calculating Purchasing Power.json';
  static const String profileVerify = 'assets/JSON/profile verified.json';
  static const String validateNationalID =
      'assets/JSON/Validating National ID.json';
  static const String verifyProInfo =
      'assets/JSON/Verifying Profile Information.json';
  static const String newStoresOnTheWay = 'assets/JSON/new_stores_on_the_way.json';
  static const String contactUsJson = 'assets/JSON/contact_us_via_mail.json';
  static const String paymentSuccess = 'assets/JSON/payment_success.json';
  static const String paymentFailed = 'assets/JSON/payment_failed.json';
  static const String fistBump = 'assets/JSON/Gestures_Fist Bump.json';

  //********************* Btm Nav Bar assets **********************//
  static const String home = 'assets/svg_icons/brand_colour/home.svg';
  static const String homeGrey = 'assets/svg_icons/grey/home.svg';
  static const String buy = 'assets/svg_icons/brand_colour/buy.svg';
  static const String buyGrey = 'assets/svg_icons/grey/buy.svg';
  static const String purchase = 'assets/svg_icons/brand_colour/purchase.svg';
  static const String purchaseGrey = 'assets/svg_icons/grey/purchase.svg';
  static const String userProfile =
      'assets/svg_icons/brand_colour/user profile.svg';
  static const String userProfileGrey =
      'assets/svg_icons/grey/user profile.svg';

  //********************* no-data assets **********************//
  static const String noDocuments =
      'assets/svg_icons/other_assets/no_documents.svg';
  static const String noNotifications =
      'assets/svg_icons/other_assets/no_notification.svg';
  static const String noPurchases =
      'assets/svg_icons/other_assets/no_purchase_data.svg';
  static const String noTransactions =
      'assets/svg_icons/other_assets/no_transactions_data.svg';
  static const String noUsers =
      'assets/svg_icons/other_assets/no_user_customer_data.svg';
  static const String noVendors =
      'assets/svg_icons/other_assets/no_vendor_data.svg';

//********************* title assets **********************//
  static const String mobile = 'assets/svg_icons/other_assets/mobile.svg';
  static const String nationalID = 'assets/svg_icons/other_assets/national id.svg';
  static const String profile = 'assets/svg_icons/other_assets/profile.svg';
  static const String selfie = 'assets/svg_icons/other_assets/selfie.svg';
  static const String verification = 'assets/svg_icons/other_assets/verification.svg';
  // Info Graphics
  static const String nationalIDIG1 = 'assets/png_icons/other_assets/nationalIDIG1.png';
  static const String nationalIDIG2 = 'assets/png_icons/other_assets/nationalIDIG2.png';
  static const String nationalIDIG3 = 'assets/png_icons/other_assets/nationalIDIG3.png';

  static const String restrictionBG = 'assets/svg_icons/other_assets/purchase restriction alert bg.svg';
  static const String restrictionIllustration = 'assets/svg_icons/other_assets/purchase restriction illustration.svg';
  static const String hourglass = 'assets/svg_icons/other_assets/hourglass.svg';
}
