import 'package:flutter/material.dart';

class AppColors {
  static const Color buttonColor = Color(0xFF45379F);
  static const Color appIconColor = Color(0xFFFD7B02);
  static const Color bgWhiteColor = Color(0xFFFFFFFF);
  static const Color mainTextColor = Color(0xFF292751);
  static const Color subTextColor = Color(0xFF7987AA);
  static const Color textLightGreyColor = Color(0xFF7A87AB);
  static const Color secondaryTextColor = Color(0xFF7B839A);
  static const Color shadowColor = Color(0xFFE4E9F2);   //border shadow color
  static const Color themePrimaryColor = Color(0xFF4537A0);
  static const Color textBlackColor = Color(0xFF282538);
  static const Color errorTextColor = Color(0xFFD21314);
  static const Color errorBorderColor = Color(0xFFB14048);
  static const Color greenColor = Color(0xFF19AA7A);
  static const Color yellowColor = Color(0xFFFFAB16);
  static const Color yellow1Color = Color(0xFFFFF5E3);
  static const Color yellow2Color = Color(0xFFFDD388);
  static const Color blueColor = Color(0xFF3B76DB);
  static const Color dropShadow = Color(0xFF43389A);
  static const Color dark0 = Color(0xFF0B0A10);
  static const Color dark1 = Color(0xFF292751);
  static const Color dark2 = Color(0xFF7886A9);
  static const Color light2 = Color(0xFFF6F5FF);
  static const Color light1 = Color(0xFFF1F3F6);
  static const Color light0 = Color(0xFFE4E9F2);
  static const Color yellow0 = Color(0xFFED9E11);

}
