import 'dart:io';

import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

/// [TextInputFormatter] that only accepts numbers
class NumberFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.contains(RegExp(r'D'))) {
      final TextSelection newSelection = TextSelection(
          baseOffset: newValue.selection.baseOffset - 1,
          extentOffset: newValue.selection.extentOffset - 1);
      final String newText = newValue.text.replaceAll(RegExp(r'D'), '');
      return TextEditingValue(
          text: newText, selection: newSelection, composing: TextRange.empty);
    }
    return newValue;
  }
}

class Logger {
  Logger({String? tag}) : tag = tag ?? 'Logger';

  final String tag;

  static bool get isProduction => const bool.fromEnvironment('dart.vm.product');

  void d(String log) {
    if (isProduction) {
      return;
    }
    debugPrint('D/$tag : $log');
  }

  void i(String log) {
    debugPrint('I/$tag : $log');
  }

  void w(String log) {
    debugPrint(
        'W/$tag Warning ===========================================================');
    debugPrint(log);
    debugPrint(
        '==========================================================================');
  }

  void e(String log) {
    debugPrint('E/$tag ----------------------Error----------------------');
    debugPrint(log);
    debugPrint('---------------------------------------------------------');
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

class Utils {

  static Future<dynamic> cropImage(File? imageFile) async {
    var _croppedFile = await ImageCropper().cropImage(
        sourcePath: imageFile!.path,
        cropStyle: CropStyle.rectangle,
      uiSettings: [
        AndroidUiSettings(
            toolbarTitle: '',
            toolbarColor: AppColors.themePrimaryColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        IOSUiSettings(
          title: '',
        )
      ],
    );
    if(_croppedFile == null){
      return null;
    }
    else{
      return _croppedFile;
    }
  }

  static Future<Map<String, String>> getHeader(String token) async {
    return <String, String>{
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    };
  }

  static Color purchaseListColor(String status) {
    switch (status) {
      case 'outstanding_payment':
        return const Color(0XFFFFAB16);
      case 'completed':
        return const Color(0XFF19AA7A);
      default:
        return Colors.black;
    }
  }

  static String formatCreatedAtDate({required String date}) {
    try {
      final DateTime dateObj = DateTime.parse(date).toLocal();
      /*final int diffDays = DateTime.now()
          .difference(dateObj)
          .inDays;*/
      final int diffHours = DateTime.now().difference(dateObj).inHours;
      final int diffMinutes = DateTime.now().difference(dateObj).inMinutes;
      final int diffSeconds = DateTime.now().difference(dateObj).inSeconds;

      if (diffHours == 0) {
        if (diffMinutes == 0) {
          return '$diffSeconds Sec ago';
        } else if (diffMinutes >= 1) {
          for (int i = 1; i < 60; i++) {
            if (i == diffMinutes) return '$diffMinutes Min ago';
          }
        }
      } else if (diffHours == 1) {
        return '$diffHours Hour ago';
      } else if (diffHours > 1) {
        for (int i = 1; i < 24; i++) {
          if (i == diffHours) return '$diffHours Hour ago';
        }
      }
      return DateFormat("MMM dd 'at' hh:mm aaa").format(dateObj);
    } catch (e) {
      return '';
    }
  }

  static Future showFractionallySizedBottomSheet({required BuildContext context,required Widget child,required double fraction}){
    return showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        context: context,
        builder: (BuildContext context) {
          return FractionallySizedBox(
            heightFactor: fraction,
            child: child
          );
        });
  }
}
