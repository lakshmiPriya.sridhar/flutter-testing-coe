class AppMeta {
  static List<int> skeletonCount() {
    List<int> suggestions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    return suggestions;
  }

  static List<int> transactionSkeletonCount() {
    List<int> suggestions = [1, 2, 3];
    return suggestions;
  }

  static List<String> walletTypeEnum() {
    List<String> suggestions = ['type_1', 'type_2'];
    return suggestions;
  }
}
