import 'package:flutter_dotenv/flutter_dotenv.dart';

enum Status {
  forceLogout,
  getRefreshToken,
}

class AppConstants {
  AppConstants._();

  static String returnUrl = dotenv.env['RETURN_URL'].toString();
  static String lokaliseSDKToken = dotenv.env['LOKALISE_SDK_TOKEN'].toString();
  static String lokaliseProjectId = dotenv.env['LOKALISE_PROJECT_ID'].toString();
  static String privacyPolicyUrl = dotenv.env['PRIVACY_POLICY_URL'].toString();
  static String appsFlyerDevKey = dotenv.env['APPSFLYER_DEV_KEY'].toString();
  static String appsFlyerAppId = dotenv.env['APPSFLYER_APP_ID'].toString();
  static const List<String> digifiedErrorCodesList1 = ["E1203", "E5101", "E5102",
    "E5103", "E5104", "E5105", "E5106", "E5107", "E6129", "E6130", "E6128",
    "E5108", "E5109", "E5110", "E5111", "E5112", "E5113", "E5114", "E5115",
    "E5116", "E6101", "E6102","E6103", "E6104", "E6105", "E6106", "E6107",
    "E6108", "E6109", "E6110", "E6111", "E6112", "E6113", "E6114", "E6115",
    "E6116", "E6117", "E6118", "E6119", "E6120", "E6121", "E6122", "E6123",
  ];
  static const List<String> digifiedErrorCodesList2 = ["E3103", "E4102", "E4301", "E4103"];
}

enum CustomerStatus {
  waitlisted,
  approved,
  inactive,
  waiting_for_approval,
  failed_verification
}