// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_success.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ApiSuccess> _$apiSuccessSerializer = new _$ApiSuccessSerializer();

class _$ApiSuccessSerializer implements StructuredSerializer<ApiSuccess> {
  @override
  final Iterable<Type> types = const [ApiSuccess, _$ApiSuccess];
  @override
  final String wireName = 'ApiSuccess';

  @override
  Iterable<Object?> serialize(Serializers serializers, ApiSuccess object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.message;
    if (value != null) {
      result
        ..add('message')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.success;
    if (value != null) {
      result
        ..add('success')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.paymentStatus;
    if (value != null) {
      result
        ..add('payment_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.meta;
    if (value != null) {
      result
        ..add('meta')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Pagination)));
    }
    value = object.countryCodeMeta;
    if (value != null) {
      result
        ..add('country_codes')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(
                BuiltList, const [const FullType(CountryCode)])));
    }
    value = object.customer;
    if (value != null) {
      result
        ..add('customer')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(AppUser)));
    }
    value = object.token;
    if (value != null) {
      result
        ..add('token')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(AccessToken)));
    }
    value = object.uploadedFile;
    if (value != null) {
      result
        ..add('attachment')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(FileAttachment)));
    }
    value = object.attachment;
    if (value != null) {
      result
        ..add('data')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(Attachments)));
    }
    value = object.userDeviceMap;
    if (value != null) {
      result
        ..add('device')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(DeviceToken)));
    }
    return result;
  }

  @override
  ApiSuccess deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ApiSuccessBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'success':
          result.success = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'payment_status':
          result.paymentStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'meta':
          result.meta.replace(serializers.deserialize(value,
              specifiedType: const FullType(Pagination))! as Pagination);
          break;
        case 'country_codes':
          result.countryCodeMeta.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(CountryCode)]))!
              as BuiltList<Object?>);
          break;
        case 'customer':
          result.customer.replace(serializers.deserialize(value,
              specifiedType: const FullType(AppUser))! as AppUser);
          break;
        case 'token':
          result.token.replace(serializers.deserialize(value,
              specifiedType: const FullType(AccessToken))! as AccessToken);
          break;
        case 'attachment':
          result.uploadedFile.replace(serializers.deserialize(value,
                  specifiedType: const FullType(FileAttachment))!
              as FileAttachment);
          break;
        case 'data':
          result.attachment.replace(serializers.deserialize(value,
              specifiedType: const FullType(Attachments))! as Attachments);
          break;
        case 'device':
          result.userDeviceMap.replace(serializers.deserialize(value,
              specifiedType: const FullType(DeviceToken))! as DeviceToken);
          break;
      }
    }

    return result.build();
  }
}

class _$ApiSuccess extends ApiSuccess {
  @override
  final int? status;
  @override
  final String? message;
  @override
  final String? success;
  @override
  final String? paymentStatus;
  @override
  final Pagination? meta;
  @override
  final BuiltList<CountryCode>? countryCodeMeta;
  @override
  final AppUser? customer;
  @override
  final AccessToken? token;
  @override
  final FileAttachment? uploadedFile;
  @override
  final Attachments? attachment;
  @override
  final DeviceToken? userDeviceMap;

  factory _$ApiSuccess([void Function(ApiSuccessBuilder)? updates]) =>
      (new ApiSuccessBuilder()..update(updates))._build();

  _$ApiSuccess._(
      {this.status,
      this.message,
      this.success,
      this.paymentStatus,
      this.meta,
      this.countryCodeMeta,
      this.customer,
      this.token,
      this.uploadedFile,
      this.attachment,
      this.userDeviceMap})
      : super._();

  @override
  ApiSuccess rebuild(void Function(ApiSuccessBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ApiSuccessBuilder toBuilder() => new ApiSuccessBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ApiSuccess &&
        status == other.status &&
        message == other.message &&
        success == other.success &&
        paymentStatus == other.paymentStatus &&
        meta == other.meta &&
        countryCodeMeta == other.countryCodeMeta &&
        customer == other.customer &&
        token == other.token &&
        uploadedFile == other.uploadedFile &&
        attachment == other.attachment &&
        userDeviceMap == other.userDeviceMap;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, message.hashCode);
    _$hash = $jc(_$hash, success.hashCode);
    _$hash = $jc(_$hash, paymentStatus.hashCode);
    _$hash = $jc(_$hash, meta.hashCode);
    _$hash = $jc(_$hash, countryCodeMeta.hashCode);
    _$hash = $jc(_$hash, customer.hashCode);
    _$hash = $jc(_$hash, token.hashCode);
    _$hash = $jc(_$hash, uploadedFile.hashCode);
    _$hash = $jc(_$hash, attachment.hashCode);
    _$hash = $jc(_$hash, userDeviceMap.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'ApiSuccess')
          ..add('status', status)
          ..add('message', message)
          ..add('success', success)
          ..add('paymentStatus', paymentStatus)
          ..add('meta', meta)
          ..add('countryCodeMeta', countryCodeMeta)
          ..add('customer', customer)
          ..add('token', token)
          ..add('uploadedFile', uploadedFile)
          ..add('attachment', attachment)
          ..add('userDeviceMap', userDeviceMap))
        .toString();
  }
}

class ApiSuccessBuilder implements Builder<ApiSuccess, ApiSuccessBuilder> {
  _$ApiSuccess? _$v;

  int? _status;
  int? get status => _$this._status;
  set status(int? status) => _$this._status = status;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  String? _success;
  String? get success => _$this._success;
  set success(String? success) => _$this._success = success;

  String? _paymentStatus;
  String? get paymentStatus => _$this._paymentStatus;
  set paymentStatus(String? paymentStatus) =>
      _$this._paymentStatus = paymentStatus;

  PaginationBuilder? _meta;
  PaginationBuilder get meta => _$this._meta ??= new PaginationBuilder();
  set meta(PaginationBuilder? meta) => _$this._meta = meta;

  ListBuilder<CountryCode>? _countryCodeMeta;
  ListBuilder<CountryCode> get countryCodeMeta =>
      _$this._countryCodeMeta ??= new ListBuilder<CountryCode>();
  set countryCodeMeta(ListBuilder<CountryCode>? countryCodeMeta) =>
      _$this._countryCodeMeta = countryCodeMeta;

  AppUserBuilder? _customer;
  AppUserBuilder get customer => _$this._customer ??= new AppUserBuilder();
  set customer(AppUserBuilder? customer) => _$this._customer = customer;

  AccessTokenBuilder? _token;
  AccessTokenBuilder get token => _$this._token ??= new AccessTokenBuilder();
  set token(AccessTokenBuilder? token) => _$this._token = token;

  FileAttachmentBuilder? _uploadedFile;
  FileAttachmentBuilder get uploadedFile =>
      _$this._uploadedFile ??= new FileAttachmentBuilder();
  set uploadedFile(FileAttachmentBuilder? uploadedFile) =>
      _$this._uploadedFile = uploadedFile;

  AttachmentsBuilder? _attachment;
  AttachmentsBuilder get attachment =>
      _$this._attachment ??= new AttachmentsBuilder();
  set attachment(AttachmentsBuilder? attachment) =>
      _$this._attachment = attachment;

  DeviceTokenBuilder? _userDeviceMap;
  DeviceTokenBuilder get userDeviceMap =>
      _$this._userDeviceMap ??= new DeviceTokenBuilder();
  set userDeviceMap(DeviceTokenBuilder? userDeviceMap) =>
      _$this._userDeviceMap = userDeviceMap;

  ApiSuccessBuilder();

  ApiSuccessBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _status = $v.status;
      _message = $v.message;
      _success = $v.success;
      _paymentStatus = $v.paymentStatus;
      _meta = $v.meta?.toBuilder();
      _countryCodeMeta = $v.countryCodeMeta?.toBuilder();
      _customer = $v.customer?.toBuilder();
      _token = $v.token?.toBuilder();
      _uploadedFile = $v.uploadedFile?.toBuilder();
      _attachment = $v.attachment?.toBuilder();
      _userDeviceMap = $v.userDeviceMap?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ApiSuccess other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ApiSuccess;
  }

  @override
  void update(void Function(ApiSuccessBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  ApiSuccess build() => _build();

  _$ApiSuccess _build() {
    _$ApiSuccess _$result;
    try {
      _$result = _$v ??
          new _$ApiSuccess._(
              status: status,
              message: message,
              success: success,
              paymentStatus: paymentStatus,
              meta: _meta?.build(),
              countryCodeMeta: _countryCodeMeta?.build(),
              customer: _customer?.build(),
              token: _token?.build(),
              uploadedFile: _uploadedFile?.build(),
              attachment: _attachment?.build(),
              userDeviceMap: _userDeviceMap?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'meta';
        _meta?.build();
        _$failedField = 'countryCodeMeta';
        _countryCodeMeta?.build();
        _$failedField = 'customer';
        _customer?.build();
        _$failedField = 'token';
        _token?.build();
        _$failedField = 'uploadedFile';
        _uploadedFile?.build();
        _$failedField = 'attachment';
        _attachment?.build();
        _$failedField = 'userDeviceMap';
        _userDeviceMap?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'ApiSuccess', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
