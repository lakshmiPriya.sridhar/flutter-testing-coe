// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attachments.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Attachments> _$attachmentsSerializer = new _$AttachmentsSerializer();
Serializer<UrlFields> _$urlFieldsSerializer = new _$UrlFieldsSerializer();

class _$AttachmentsSerializer implements StructuredSerializer<Attachments> {
  @override
  final Iterable<Type> types = const [Attachments, _$Attachments];
  @override
  final String wireName = 'Attachments';

  @override
  Iterable<Object?> serialize(Serializers serializers, Attachments object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.url;
    if (value != null) {
      result
        ..add('url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.urlFields;
    if (value != null) {
      result
        ..add('url_fields')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(UrlFields)));
    }
    return result;
  }

  @override
  Attachments deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AttachmentsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'url_fields':
          result.urlFields.replace(serializers.deserialize(value,
              specifiedType: const FullType(UrlFields))! as UrlFields);
          break;
      }
    }

    return result.build();
  }
}

class _$UrlFieldsSerializer implements StructuredSerializer<UrlFields> {
  @override
  final Iterable<Type> types = const [UrlFields, _$UrlFields];
  @override
  final String wireName = 'UrlFields';

  @override
  Iterable<Object?> serialize(Serializers serializers, UrlFields object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.key;
    if (value != null) {
      result
        ..add('key')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.successActionStatus;
    if (value != null) {
      result
        ..add('success_action_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.policy;
    if (value != null) {
      result
        ..add('policy')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.xAmzCredential;
    if (value != null) {
      result
        ..add('x-amz-credential')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.xAmzAlgorithm;
    if (value != null) {
      result
        ..add('x-amz-algorithm')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.xAmzDate;
    if (value != null) {
      result
        ..add('x-amz-date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.xAmzSignature;
    if (value != null) {
      result
        ..add('x-amz-signature')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.cacheControl;
    if (value != null) {
      result
        ..add('Cache-Control')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  UrlFields deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UrlFieldsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'key':
          result.key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'success_action_status':
          result.successActionStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'policy':
          result.policy = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'x-amz-credential':
          result.xAmzCredential = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'x-amz-algorithm':
          result.xAmzAlgorithm = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'x-amz-date':
          result.xAmzDate = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'x-amz-signature':
          result.xAmzSignature = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'Cache-Control':
          result.cacheControl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$Attachments extends Attachments {
  @override
  final String? url;
  @override
  final UrlFields? urlFields;

  factory _$Attachments([void Function(AttachmentsBuilder)? updates]) =>
      (new AttachmentsBuilder()..update(updates))._build();

  _$Attachments._({this.url, this.urlFields}) : super._();

  @override
  Attachments rebuild(void Function(AttachmentsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AttachmentsBuilder toBuilder() => new AttachmentsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Attachments &&
        url == other.url &&
        urlFields == other.urlFields;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, url.hashCode);
    _$hash = $jc(_$hash, urlFields.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Attachments')
          ..add('url', url)
          ..add('urlFields', urlFields))
        .toString();
  }
}

class AttachmentsBuilder implements Builder<Attachments, AttachmentsBuilder> {
  _$Attachments? _$v;

  String? _url;
  String? get url => _$this._url;
  set url(String? url) => _$this._url = url;

  UrlFieldsBuilder? _urlFields;
  UrlFieldsBuilder get urlFields =>
      _$this._urlFields ??= new UrlFieldsBuilder();
  set urlFields(UrlFieldsBuilder? urlFields) => _$this._urlFields = urlFields;

  AttachmentsBuilder();

  AttachmentsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _url = $v.url;
      _urlFields = $v.urlFields?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Attachments other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Attachments;
  }

  @override
  void update(void Function(AttachmentsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Attachments build() => _build();

  _$Attachments _build() {
    _$Attachments _$result;
    try {
      _$result =
          _$v ?? new _$Attachments._(url: url, urlFields: _urlFields?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'urlFields';
        _urlFields?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'Attachments', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UrlFields extends UrlFields {
  @override
  final String? key;
  @override
  final String? successActionStatus;
  @override
  final String? policy;
  @override
  final String? xAmzCredential;
  @override
  final String? xAmzAlgorithm;
  @override
  final String? xAmzDate;
  @override
  final String? xAmzSignature;
  @override
  final String? cacheControl;

  factory _$UrlFields([void Function(UrlFieldsBuilder)? updates]) =>
      (new UrlFieldsBuilder()..update(updates))._build();

  _$UrlFields._(
      {this.key,
      this.successActionStatus,
      this.policy,
      this.xAmzCredential,
      this.xAmzAlgorithm,
      this.xAmzDate,
      this.xAmzSignature,
      this.cacheControl})
      : super._();

  @override
  UrlFields rebuild(void Function(UrlFieldsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UrlFieldsBuilder toBuilder() => new UrlFieldsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UrlFields &&
        key == other.key &&
        successActionStatus == other.successActionStatus &&
        policy == other.policy &&
        xAmzCredential == other.xAmzCredential &&
        xAmzAlgorithm == other.xAmzAlgorithm &&
        xAmzDate == other.xAmzDate &&
        xAmzSignature == other.xAmzSignature &&
        cacheControl == other.cacheControl;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, key.hashCode);
    _$hash = $jc(_$hash, successActionStatus.hashCode);
    _$hash = $jc(_$hash, policy.hashCode);
    _$hash = $jc(_$hash, xAmzCredential.hashCode);
    _$hash = $jc(_$hash, xAmzAlgorithm.hashCode);
    _$hash = $jc(_$hash, xAmzDate.hashCode);
    _$hash = $jc(_$hash, xAmzSignature.hashCode);
    _$hash = $jc(_$hash, cacheControl.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UrlFields')
          ..add('key', key)
          ..add('successActionStatus', successActionStatus)
          ..add('policy', policy)
          ..add('xAmzCredential', xAmzCredential)
          ..add('xAmzAlgorithm', xAmzAlgorithm)
          ..add('xAmzDate', xAmzDate)
          ..add('xAmzSignature', xAmzSignature)
          ..add('cacheControl', cacheControl))
        .toString();
  }
}

class UrlFieldsBuilder implements Builder<UrlFields, UrlFieldsBuilder> {
  _$UrlFields? _$v;

  String? _key;
  String? get key => _$this._key;
  set key(String? key) => _$this._key = key;

  String? _successActionStatus;
  String? get successActionStatus => _$this._successActionStatus;
  set successActionStatus(String? successActionStatus) =>
      _$this._successActionStatus = successActionStatus;

  String? _policy;
  String? get policy => _$this._policy;
  set policy(String? policy) => _$this._policy = policy;

  String? _xAmzCredential;
  String? get xAmzCredential => _$this._xAmzCredential;
  set xAmzCredential(String? xAmzCredential) =>
      _$this._xAmzCredential = xAmzCredential;

  String? _xAmzAlgorithm;
  String? get xAmzAlgorithm => _$this._xAmzAlgorithm;
  set xAmzAlgorithm(String? xAmzAlgorithm) =>
      _$this._xAmzAlgorithm = xAmzAlgorithm;

  String? _xAmzDate;
  String? get xAmzDate => _$this._xAmzDate;
  set xAmzDate(String? xAmzDate) => _$this._xAmzDate = xAmzDate;

  String? _xAmzSignature;
  String? get xAmzSignature => _$this._xAmzSignature;
  set xAmzSignature(String? xAmzSignature) =>
      _$this._xAmzSignature = xAmzSignature;

  String? _cacheControl;
  String? get cacheControl => _$this._cacheControl;
  set cacheControl(String? cacheControl) => _$this._cacheControl = cacheControl;

  UrlFieldsBuilder();

  UrlFieldsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _key = $v.key;
      _successActionStatus = $v.successActionStatus;
      _policy = $v.policy;
      _xAmzCredential = $v.xAmzCredential;
      _xAmzAlgorithm = $v.xAmzAlgorithm;
      _xAmzDate = $v.xAmzDate;
      _xAmzSignature = $v.xAmzSignature;
      _cacheControl = $v.cacheControl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UrlFields other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UrlFields;
  }

  @override
  void update(void Function(UrlFieldsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UrlFields build() => _build();

  _$UrlFields _build() {
    final _$result = _$v ??
        new _$UrlFields._(
            key: key,
            successActionStatus: successActionStatus,
            policy: policy,
            xAmzCredential: xAmzCredential,
            xAmzAlgorithm: xAmzAlgorithm,
            xAmzDate: xAmzDate,
            xAmzSignature: xAmzSignature,
            cacheControl: cacheControl);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
