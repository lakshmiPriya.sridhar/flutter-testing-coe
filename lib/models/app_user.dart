import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'app_user.g.dart';

abstract class AppUser implements Built<AppUser, AppUserBuilder> {
  factory AppUser([AppUserBuilder Function(AppUserBuilder builder) updates]) =
      _$AppUser;

  AppUser._();

  String? get login;

  @BuiltValueField(wireName: 'id')
  int? get userId;

  @BuiltValueField(wireName: 'full_name')
  String? get fullName;

  @BuiltValueField(wireName: 'email')
  String? get email;

  @BuiltValueField(wireName: 'national_id_number')
  String? get nationalIdNumber;

  @BuiltValueField(wireName: 'national_id_status')
  String? get nationalIdStatus;

  @BuiltValueField(wireName: 'deall_score')
  double? get deallScore;

  @BuiltValueField(wireName: 'max_purchase_power')
  int? get maxPurchasePower;

  @BuiltValueField(wireName: 'processing_fee')
  double? get processingFee;

  String? get status;

  @BuiltValueField(wireName: 'date_of_birth')
  String? get dateOfBirth;

  String? get address;

  @BuiltValueField(wireName: 'mobile_number')
  String? get mobileNumber;

  @BuiltValueField(wireName: 'national_id_front')
  NationalId? get nationalIdFront;

  @BuiltValueField(wireName: 'national_id_back')
  NationalId? get nationalIdBack;

  @BuiltValueField(wireName: 'profile_pic')
  NationalId? get profilePic;

  @BuiltValueField(wireName: 'terms_and_conditions_accepted_at')
  String? get termsAndConditionsAcceptedAt;

  @BuiltValueField(wireName: 'email_confirmed_at')
  String? get emailVerifiedAt;

  @BuiltValueField(wireName: 'first_name')
  String? get firstName;

  @BuiltValueField(wireName: 'last_name')
  String? get lastName;

  @BuiltValueField(wireName: 'bank_statement')
  CustomerDocs? get bankStatement;

  @BuiltValueField(wireName: 'employment_letter')
  CustomerDocs? get employmentLetter;

  @BuiltValueField(wireName: 'is_active')
  String? get isActive;

  @BuiltValueField(wireName: 'purchase_id')
  String? get purchaseID;

  @BuiltValueField(wireName: 'national_id')
  String? get nationalID;

  @BuiltValueField(wireName: 'national_id_front_url')
  String? get nationalIdFrontURL;

  @BuiltValueField(wireName: 'customer_payment_status')
  String? get customerPaymentStatus;

  @BuiltValueField(wireName: 'total_amount')
  int? get totalAmount;

  @BuiltValueField(wireName: 'minimum_amount')
  int? get minimumAmount;

  @BuiltValueField(wireName: 'maximum_amount')
  int? get maximumAmount;

  @BuiltValueField(wireName: 'no_of_installments_paid')
  int? get noOfInstallmentsPaid;

  @BuiltValueField(wireName: 'profile_status')
  String? get profileStatus;

  @BuiltValueField(wireName: 'due_date')
  String? get dueData;

  @BuiltValueField(wireName: 'customer_due_status')
  bool? get customerDueStatus;

  @BuiltValueField(wireName: 'no_of_unread_notifications')
  int? get noOfUnreadNotifications;

  String? get token;

  @BuiltValueField(wireName: 'country_code')
  CountryCode? get countryCode;

  @BuiltValueField(wireName: 'country_code_id')
  int? get countryCodeId;

  static Serializer<AppUser> get serializer => _$appUserSerializer;
}

abstract class NationalId implements Built<NationalId, NationalIdBuilder> {
  factory NationalId(
          [NationalIdBuilder Function(NationalIdBuilder builder) updates]) =
      _$NationalId;

  NationalId._();

  @BuiltValueField(wireName: 'id')
  int? get userId;

  @BuiltValueField(wireName: 'filename')
  String? get filename;

  @BuiltValueField(wireName: 's3_key')
  String? get s3Key;

  @BuiltValueField(wireName: 's3_url')
  String? get s3Url;

  static Serializer<NationalId> get serializer => _$nationalIdSerializer;
}

abstract class CustomerDocs implements Built<CustomerDocs, CustomerDocsBuilder> {
  factory CustomerDocs(
      [CustomerDocsBuilder Function(CustomerDocsBuilder builder) updates]) =
  _$CustomerDocs;

  CustomerDocs._();

  @BuiltValueField(wireName: 'id')
  int? get userId;

  @BuiltValueField(wireName: 'filename')
  String? get filename;

  @BuiltValueField(wireName: 's3_key')
  String? get s3Key;

  @BuiltValueField(wireName: 's3_url')
  String? get s3Url;

  @BuiltValueField(wireName: 'attachment_status')
  String? get status;

  static Serializer<CustomerDocs> get serializer => _$customerDocsSerializer;
}

abstract class CountryCode implements Built<CountryCode, CountryCodeBuilder> {
  factory CountryCode(
      [CountryCodeBuilder Function(CountryCodeBuilder builder) updates]) =
  _$CountryCode;

  CountryCode._();

  int? get id;

  String? get name;

  String? get flag;

  @BuiltValueField(wireName: 'isd_code')
  String? get isdCode;

  static Serializer<CountryCode> get serializer => _$countryCodeSerializer;
}
