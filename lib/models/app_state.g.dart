// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AppState extends AppState {
  @override
  final String? locale;
  @override
  final bool? needRestart;
  @override
  final GlobalKey<NavigatorState>? navigator;
  @override
  final bool? isInitializing;
  @override
  final bool? isLoading;
  @override
  final int? unreadNotificationsCount;
  @override
  final String? paginationMessage;
  @override
  final String? storesSearchPaginationMessage;
  @override
  final String? storesLovePaginationMessage;
  @override
  final String? storesTrendingPaginationMessage;
  @override
  final String? inStoresPaginationMessage;
  @override
  final String? inStoresAttachmentsPaginationMessage;
  @override
  final String? purchasesPaginationMessage;
  @override
  final String? transactionPaginationMessage;
  @override
  final String? notificationPaginationMessage;
  @override
  final String? allStoresPaginationMessage;
  @override
  final String? errorMessage;
  @override
  final Pagination? pagination;
  @override
  final String? successMessage;
  @override
  final BuiltList<CountryCode>? countryCodeMeta;
  @override
  final int? userCountryCode;
  @override
  final AppUser? currentUser;

  factory _$AppState([void Function(AppStateBuilder)? updates]) =>
      (new AppStateBuilder()..update(updates))._build();

  _$AppState._(
      {this.locale,
      this.needRestart,
      this.navigator,
      this.isInitializing,
      this.isLoading,
      this.unreadNotificationsCount,
      this.paginationMessage,
      this.storesSearchPaginationMessage,
      this.storesLovePaginationMessage,
      this.storesTrendingPaginationMessage,
      this.inStoresPaginationMessage,
      this.inStoresAttachmentsPaginationMessage,
      this.purchasesPaginationMessage,
      this.transactionPaginationMessage,
      this.notificationPaginationMessage,
      this.allStoresPaginationMessage,
      this.errorMessage,
      this.pagination,
      this.successMessage,
      this.countryCodeMeta,
      this.userCountryCode,
      this.currentUser})
      : super._();

  @override
  AppState rebuild(void Function(AppStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AppStateBuilder toBuilder() => new AppStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppState &&
        locale == other.locale &&
        needRestart == other.needRestart &&
        navigator == other.navigator &&
        isInitializing == other.isInitializing &&
        isLoading == other.isLoading &&
        unreadNotificationsCount == other.unreadNotificationsCount &&
        paginationMessage == other.paginationMessage &&
        storesSearchPaginationMessage == other.storesSearchPaginationMessage &&
        storesLovePaginationMessage == other.storesLovePaginationMessage &&
        storesTrendingPaginationMessage ==
            other.storesTrendingPaginationMessage &&
        inStoresPaginationMessage == other.inStoresPaginationMessage &&
        inStoresAttachmentsPaginationMessage ==
            other.inStoresAttachmentsPaginationMessage &&
        purchasesPaginationMessage == other.purchasesPaginationMessage &&
        transactionPaginationMessage == other.transactionPaginationMessage &&
        notificationPaginationMessage == other.notificationPaginationMessage &&
        allStoresPaginationMessage == other.allStoresPaginationMessage &&
        errorMessage == other.errorMessage &&
        pagination == other.pagination &&
        successMessage == other.successMessage &&
        countryCodeMeta == other.countryCodeMeta &&
        userCountryCode == other.userCountryCode &&
        currentUser == other.currentUser;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, locale.hashCode);
    _$hash = $jc(_$hash, needRestart.hashCode);
    _$hash = $jc(_$hash, navigator.hashCode);
    _$hash = $jc(_$hash, isInitializing.hashCode);
    _$hash = $jc(_$hash, isLoading.hashCode);
    _$hash = $jc(_$hash, unreadNotificationsCount.hashCode);
    _$hash = $jc(_$hash, paginationMessage.hashCode);
    _$hash = $jc(_$hash, storesSearchPaginationMessage.hashCode);
    _$hash = $jc(_$hash, storesLovePaginationMessage.hashCode);
    _$hash = $jc(_$hash, storesTrendingPaginationMessage.hashCode);
    _$hash = $jc(_$hash, inStoresPaginationMessage.hashCode);
    _$hash = $jc(_$hash, inStoresAttachmentsPaginationMessage.hashCode);
    _$hash = $jc(_$hash, purchasesPaginationMessage.hashCode);
    _$hash = $jc(_$hash, transactionPaginationMessage.hashCode);
    _$hash = $jc(_$hash, notificationPaginationMessage.hashCode);
    _$hash = $jc(_$hash, allStoresPaginationMessage.hashCode);
    _$hash = $jc(_$hash, errorMessage.hashCode);
    _$hash = $jc(_$hash, pagination.hashCode);
    _$hash = $jc(_$hash, successMessage.hashCode);
    _$hash = $jc(_$hash, countryCodeMeta.hashCode);
    _$hash = $jc(_$hash, userCountryCode.hashCode);
    _$hash = $jc(_$hash, currentUser.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AppState')
          ..add('locale', locale)
          ..add('needRestart', needRestart)
          ..add('navigator', navigator)
          ..add('isInitializing', isInitializing)
          ..add('isLoading', isLoading)
          ..add('unreadNotificationsCount', unreadNotificationsCount)
          ..add('paginationMessage', paginationMessage)
          ..add('storesSearchPaginationMessage', storesSearchPaginationMessage)
          ..add('storesLovePaginationMessage', storesLovePaginationMessage)
          ..add('storesTrendingPaginationMessage',
              storesTrendingPaginationMessage)
          ..add('inStoresPaginationMessage', inStoresPaginationMessage)
          ..add('inStoresAttachmentsPaginationMessage',
              inStoresAttachmentsPaginationMessage)
          ..add('purchasesPaginationMessage', purchasesPaginationMessage)
          ..add('transactionPaginationMessage', transactionPaginationMessage)
          ..add('notificationPaginationMessage', notificationPaginationMessage)
          ..add('allStoresPaginationMessage', allStoresPaginationMessage)
          ..add('errorMessage', errorMessage)
          ..add('pagination', pagination)
          ..add('successMessage', successMessage)
          ..add('countryCodeMeta', countryCodeMeta)
          ..add('userCountryCode', userCountryCode)
          ..add('currentUser', currentUser))
        .toString();
  }
}

class AppStateBuilder implements Builder<AppState, AppStateBuilder> {
  _$AppState? _$v;

  String? _locale;
  String? get locale => _$this._locale;
  set locale(String? locale) => _$this._locale = locale;

  bool? _needRestart;
  bool? get needRestart => _$this._needRestart;
  set needRestart(bool? needRestart) => _$this._needRestart = needRestart;

  GlobalKey<NavigatorState>? _navigator;
  GlobalKey<NavigatorState>? get navigator => _$this._navigator;
  set navigator(GlobalKey<NavigatorState>? navigator) =>
      _$this._navigator = navigator;

  bool? _isInitializing;
  bool? get isInitializing => _$this._isInitializing;
  set isInitializing(bool? isInitializing) =>
      _$this._isInitializing = isInitializing;

  bool? _isLoading;
  bool? get isLoading => _$this._isLoading;
  set isLoading(bool? isLoading) => _$this._isLoading = isLoading;

  int? _unreadNotificationsCount;
  int? get unreadNotificationsCount => _$this._unreadNotificationsCount;
  set unreadNotificationsCount(int? unreadNotificationsCount) =>
      _$this._unreadNotificationsCount = unreadNotificationsCount;

  String? _paginationMessage;
  String? get paginationMessage => _$this._paginationMessage;
  set paginationMessage(String? paginationMessage) =>
      _$this._paginationMessage = paginationMessage;

  String? _storesSearchPaginationMessage;
  String? get storesSearchPaginationMessage =>
      _$this._storesSearchPaginationMessage;
  set storesSearchPaginationMessage(String? storesSearchPaginationMessage) =>
      _$this._storesSearchPaginationMessage = storesSearchPaginationMessage;

  String? _storesLovePaginationMessage;
  String? get storesLovePaginationMessage =>
      _$this._storesLovePaginationMessage;
  set storesLovePaginationMessage(String? storesLovePaginationMessage) =>
      _$this._storesLovePaginationMessage = storesLovePaginationMessage;

  String? _storesTrendingPaginationMessage;
  String? get storesTrendingPaginationMessage =>
      _$this._storesTrendingPaginationMessage;
  set storesTrendingPaginationMessage(
          String? storesTrendingPaginationMessage) =>
      _$this._storesTrendingPaginationMessage = storesTrendingPaginationMessage;

  String? _inStoresPaginationMessage;
  String? get inStoresPaginationMessage => _$this._inStoresPaginationMessage;
  set inStoresPaginationMessage(String? inStoresPaginationMessage) =>
      _$this._inStoresPaginationMessage = inStoresPaginationMessage;

  String? _inStoresAttachmentsPaginationMessage;
  String? get inStoresAttachmentsPaginationMessage =>
      _$this._inStoresAttachmentsPaginationMessage;
  set inStoresAttachmentsPaginationMessage(
          String? inStoresAttachmentsPaginationMessage) =>
      _$this._inStoresAttachmentsPaginationMessage =
          inStoresAttachmentsPaginationMessage;

  String? _purchasesPaginationMessage;
  String? get purchasesPaginationMessage => _$this._purchasesPaginationMessage;
  set purchasesPaginationMessage(String? purchasesPaginationMessage) =>
      _$this._purchasesPaginationMessage = purchasesPaginationMessage;

  String? _transactionPaginationMessage;
  String? get transactionPaginationMessage =>
      _$this._transactionPaginationMessage;
  set transactionPaginationMessage(String? transactionPaginationMessage) =>
      _$this._transactionPaginationMessage = transactionPaginationMessage;

  String? _notificationPaginationMessage;
  String? get notificationPaginationMessage =>
      _$this._notificationPaginationMessage;
  set notificationPaginationMessage(String? notificationPaginationMessage) =>
      _$this._notificationPaginationMessage = notificationPaginationMessage;

  String? _allStoresPaginationMessage;
  String? get allStoresPaginationMessage => _$this._allStoresPaginationMessage;
  set allStoresPaginationMessage(String? allStoresPaginationMessage) =>
      _$this._allStoresPaginationMessage = allStoresPaginationMessage;

  String? _errorMessage;
  String? get errorMessage => _$this._errorMessage;
  set errorMessage(String? errorMessage) => _$this._errorMessage = errorMessage;

  PaginationBuilder? _pagination;
  PaginationBuilder get pagination =>
      _$this._pagination ??= new PaginationBuilder();
  set pagination(PaginationBuilder? pagination) =>
      _$this._pagination = pagination;

  String? _successMessage;
  String? get successMessage => _$this._successMessage;
  set successMessage(String? successMessage) =>
      _$this._successMessage = successMessage;

  ListBuilder<CountryCode>? _countryCodeMeta;
  ListBuilder<CountryCode> get countryCodeMeta =>
      _$this._countryCodeMeta ??= new ListBuilder<CountryCode>();
  set countryCodeMeta(ListBuilder<CountryCode>? countryCodeMeta) =>
      _$this._countryCodeMeta = countryCodeMeta;

  int? _userCountryCode;
  int? get userCountryCode => _$this._userCountryCode;
  set userCountryCode(int? userCountryCode) =>
      _$this._userCountryCode = userCountryCode;

  AppUserBuilder? _currentUser;
  AppUserBuilder get currentUser =>
      _$this._currentUser ??= new AppUserBuilder();
  set currentUser(AppUserBuilder? currentUser) =>
      _$this._currentUser = currentUser;

  AppStateBuilder();

  AppStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _locale = $v.locale;
      _needRestart = $v.needRestart;
      _navigator = $v.navigator;
      _isInitializing = $v.isInitializing;
      _isLoading = $v.isLoading;
      _unreadNotificationsCount = $v.unreadNotificationsCount;
      _paginationMessage = $v.paginationMessage;
      _storesSearchPaginationMessage = $v.storesSearchPaginationMessage;
      _storesLovePaginationMessage = $v.storesLovePaginationMessage;
      _storesTrendingPaginationMessage = $v.storesTrendingPaginationMessage;
      _inStoresPaginationMessage = $v.inStoresPaginationMessage;
      _inStoresAttachmentsPaginationMessage =
          $v.inStoresAttachmentsPaginationMessage;
      _purchasesPaginationMessage = $v.purchasesPaginationMessage;
      _transactionPaginationMessage = $v.transactionPaginationMessage;
      _notificationPaginationMessage = $v.notificationPaginationMessage;
      _allStoresPaginationMessage = $v.allStoresPaginationMessage;
      _errorMessage = $v.errorMessage;
      _pagination = $v.pagination?.toBuilder();
      _successMessage = $v.successMessage;
      _countryCodeMeta = $v.countryCodeMeta?.toBuilder();
      _userCountryCode = $v.userCountryCode;
      _currentUser = $v.currentUser?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AppState;
  }

  @override
  void update(void Function(AppStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AppState build() => _build();

  _$AppState _build() {
    _$AppState _$result;
    try {
      _$result = _$v ??
          new _$AppState._(
              locale: locale,
              needRestart: needRestart,
              navigator: navigator,
              isInitializing: isInitializing,
              isLoading: isLoading,
              unreadNotificationsCount: unreadNotificationsCount,
              paginationMessage: paginationMessage,
              storesSearchPaginationMessage: storesSearchPaginationMessage,
              storesLovePaginationMessage: storesLovePaginationMessage,
              storesTrendingPaginationMessage: storesTrendingPaginationMessage,
              inStoresPaginationMessage: inStoresPaginationMessage,
              inStoresAttachmentsPaginationMessage:
                  inStoresAttachmentsPaginationMessage,
              purchasesPaginationMessage: purchasesPaginationMessage,
              transactionPaginationMessage: transactionPaginationMessage,
              notificationPaginationMessage: notificationPaginationMessage,
              allStoresPaginationMessage: allStoresPaginationMessage,
              errorMessage: errorMessage,
              pagination: _pagination?.build(),
              successMessage: successMessage,
              countryCodeMeta: _countryCodeMeta?.build(),
              userCountryCode: userCountryCode,
              currentUser: _currentUser?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'pagination';
        _pagination?.build();

        _$failedField = 'countryCodeMeta';
        _countryCodeMeta?.build();

        _$failedField = 'currentUser';
        _currentUser?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'AppState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
