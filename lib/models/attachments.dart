import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'attachments.g.dart';

abstract class Attachments implements Built<Attachments, AttachmentsBuilder> {
  factory Attachments([AttachmentsBuilder Function(AttachmentsBuilder builder) updates]) = _$Attachments;

  Attachments._();

  @BuiltValueField(wireName: 'url')
  String? get url;

  @BuiltValueField(wireName: 'url_fields')
  UrlFields? get urlFields;

  static Serializer<Attachments> get serializer => _$attachmentsSerializer;
}


abstract class UrlFields implements Built<UrlFields, UrlFieldsBuilder> {
  factory UrlFields([UrlFieldsBuilder Function(UrlFieldsBuilder builder) updates]) = _$UrlFields;

  UrlFields._();

  @BuiltValueField(wireName: 'key')
  String? get key;

  @BuiltValueField(wireName: 'success_action_status')
  String? get successActionStatus;

  @BuiltValueField(wireName: 'policy')
  String? get policy;

  @BuiltValueField(wireName: 'x-amz-credential')
  String? get xAmzCredential;

  @BuiltValueField(wireName: 'x-amz-algorithm')
  String? get xAmzAlgorithm;

  @BuiltValueField(wireName: 'x-amz-date')
  String? get xAmzDate;

  @BuiltValueField(wireName: 'x-amz-signature')
  String? get xAmzSignature;

  @BuiltValueField(wireName: 'Cache-Control')
  String? get cacheControl;

  static Serializer<UrlFields> get serializer => _$urlFieldsSerializer;
}