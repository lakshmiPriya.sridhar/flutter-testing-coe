// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_token.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DeviceToken> _$deviceTokenSerializer = new _$DeviceTokenSerializer();

class _$DeviceTokenSerializer implements StructuredSerializer<DeviceToken> {
  @override
  final Iterable<Type> types = const [DeviceToken, _$DeviceToken];
  @override
  final String wireName = 'DeviceToken';

  @override
  Iterable<Object?> serialize(Serializers serializers, DeviceToken object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.platform;
    if (value != null) {
      result
        ..add('platform')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(AccessToken)));
    }
    return result;
  }

  @override
  DeviceToken deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DeviceTokenBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'platform':
          result.platform.replace(serializers.deserialize(value,
              specifiedType: const FullType(AccessToken))! as AccessToken);
          break;
      }
    }

    return result.build();
  }
}

class _$DeviceToken extends DeviceToken {
  @override
  final int? id;
  @override
  final AccessToken? platform;

  factory _$DeviceToken([void Function(DeviceTokenBuilder)? updates]) =>
      (new DeviceTokenBuilder()..update(updates))._build();

  _$DeviceToken._({this.id, this.platform}) : super._();

  @override
  DeviceToken rebuild(void Function(DeviceTokenBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DeviceTokenBuilder toBuilder() => new DeviceTokenBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DeviceToken && id == other.id && platform == other.platform;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, platform.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'DeviceToken')
          ..add('id', id)
          ..add('platform', platform))
        .toString();
  }
}

class DeviceTokenBuilder implements Builder<DeviceToken, DeviceTokenBuilder> {
  _$DeviceToken? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  AccessTokenBuilder? _platform;
  AccessTokenBuilder get platform =>
      _$this._platform ??= new AccessTokenBuilder();
  set platform(AccessTokenBuilder? platform) => _$this._platform = platform;

  DeviceTokenBuilder();

  DeviceTokenBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _platform = $v.platform?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DeviceToken other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$DeviceToken;
  }

  @override
  void update(void Function(DeviceTokenBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  DeviceToken build() => _build();

  _$DeviceToken _build() {
    _$DeviceToken _$result;
    try {
      _$result =
          _$v ?? new _$DeviceToken._(id: id, platform: _platform?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'platform';
        _platform?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'DeviceToken', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
