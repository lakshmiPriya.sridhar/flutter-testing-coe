import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:flutter/material.dart' hide Builder;
import 'package:flutter_test_coe/models/models.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder> {
  factory AppState(
      [AppStateBuilder Function(AppStateBuilder builder) updates]) = _$AppState;

  AppState._();

  static AppState initState() {
    return AppState((AppStateBuilder b) {
      return b
        ..navigator = GlobalKey<NavigatorState>()
        ..isInitializing = true
        ..isLoading = false
        ..needRestart = true
        ..locale = 'en'
      ;
    });
  }

  String? get locale;

  bool? get needRestart;

  GlobalKey<NavigatorState>? get navigator;

  bool? get isInitializing;

  bool? get isLoading;

  int? get unreadNotificationsCount;

  String? get paginationMessage;

  String? get storesSearchPaginationMessage;

  String? get storesLovePaginationMessage;

  String? get storesTrendingPaginationMessage;

  String? get inStoresPaginationMessage;

  String? get inStoresAttachmentsPaginationMessage;

  String? get purchasesPaginationMessage;

  String? get transactionPaginationMessage;

  String? get notificationPaginationMessage;

  String? get allStoresPaginationMessage;

  String? get errorMessage;

  Pagination? get pagination;

  String? get successMessage;

//******************************** metas **************************************//

  BuiltList<CountryCode>? get countryCodeMeta;

  int? get userCountryCode;

//******************************** auth-module ********************************//
  AppUser? get currentUser;

}
