import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'pagination.g.dart';

abstract class Pagination implements Built<Pagination, PaginationBuilder> {
  factory Pagination([PaginationBuilder Function(PaginationBuilder builder) updates]) =
      _$Pagination;

  Pagination._();

  @BuiltValueField(wireName: 'total_pages')
  int? get totalPages;

  @BuiltValueField(wireName: 'total_count')
  int? get totalCount;

  @BuiltValueField(wireName: 'next_page')
  int? get nextPage;

  static Serializer<Pagination> get serializer => _$paginationSerializer;
}
