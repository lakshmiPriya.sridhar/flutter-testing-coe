// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(AccessToken.serializer)
      ..add(ApiError.serializer)
      ..add(ApiSuccess.serializer)
      ..add(AppUser.serializer)
      ..add(Attachments.serializer)
      ..add(CountryCode.serializer)
      ..add(CustomerDocs.serializer)
      ..add(DeviceToken.serializer)
      ..add(FileAttachment.serializer)
      ..add(NationalId.serializer)
      ..add(Pagination.serializer)
      ..add(UrlFields.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(CountryCode)]),
          () => new ListBuilder<CountryCode>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>()))
    .build();

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
