// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AppUser> _$appUserSerializer = new _$AppUserSerializer();
Serializer<NationalId> _$nationalIdSerializer = new _$NationalIdSerializer();
Serializer<CustomerDocs> _$customerDocsSerializer =
    new _$CustomerDocsSerializer();
Serializer<CountryCode> _$countryCodeSerializer = new _$CountryCodeSerializer();

class _$AppUserSerializer implements StructuredSerializer<AppUser> {
  @override
  final Iterable<Type> types = const [AppUser, _$AppUser];
  @override
  final String wireName = 'AppUser';

  @override
  Iterable<Object?> serialize(Serializers serializers, AppUser object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.login;
    if (value != null) {
      result
        ..add('login')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.userId;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.fullName;
    if (value != null) {
      result
        ..add('full_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.email;
    if (value != null) {
      result
        ..add('email')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.nationalIdNumber;
    if (value != null) {
      result
        ..add('national_id_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.nationalIdStatus;
    if (value != null) {
      result
        ..add('national_id_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.deallScore;
    if (value != null) {
      result
        ..add('deall_score')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.maxPurchasePower;
    if (value != null) {
      result
        ..add('max_purchase_power')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.processingFee;
    if (value != null) {
      result
        ..add('processing_fee')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(double)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.dateOfBirth;
    if (value != null) {
      result
        ..add('date_of_birth')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.address;
    if (value != null) {
      result
        ..add('address')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.mobileNumber;
    if (value != null) {
      result
        ..add('mobile_number')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.nationalIdFront;
    if (value != null) {
      result
        ..add('national_id_front')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(NationalId)));
    }
    value = object.nationalIdBack;
    if (value != null) {
      result
        ..add('national_id_back')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(NationalId)));
    }
    value = object.profilePic;
    if (value != null) {
      result
        ..add('profile_pic')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(NationalId)));
    }
    value = object.termsAndConditionsAcceptedAt;
    if (value != null) {
      result
        ..add('terms_and_conditions_accepted_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.emailVerifiedAt;
    if (value != null) {
      result
        ..add('email_confirmed_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.firstName;
    if (value != null) {
      result
        ..add('first_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.lastName;
    if (value != null) {
      result
        ..add('last_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.bankStatement;
    if (value != null) {
      result
        ..add('bank_statement')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(CustomerDocs)));
    }
    value = object.employmentLetter;
    if (value != null) {
      result
        ..add('employment_letter')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(CustomerDocs)));
    }
    value = object.isActive;
    if (value != null) {
      result
        ..add('is_active')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.purchaseID;
    if (value != null) {
      result
        ..add('purchase_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.nationalID;
    if (value != null) {
      result
        ..add('national_id')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.nationalIdFrontURL;
    if (value != null) {
      result
        ..add('national_id_front_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.customerPaymentStatus;
    if (value != null) {
      result
        ..add('customer_payment_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.totalAmount;
    if (value != null) {
      result
        ..add('total_amount')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.minimumAmount;
    if (value != null) {
      result
        ..add('minimum_amount')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.maximumAmount;
    if (value != null) {
      result
        ..add('maximum_amount')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.noOfInstallmentsPaid;
    if (value != null) {
      result
        ..add('no_of_installments_paid')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.profileStatus;
    if (value != null) {
      result
        ..add('profile_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.dueData;
    if (value != null) {
      result
        ..add('due_date')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.customerDueStatus;
    if (value != null) {
      result
        ..add('customer_due_status')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(bool)));
    }
    value = object.noOfUnreadNotifications;
    if (value != null) {
      result
        ..add('no_of_unread_notifications')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.token;
    if (value != null) {
      result
        ..add('token')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.countryCode;
    if (value != null) {
      result
        ..add('country_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(CountryCode)));
    }
    value = object.countryCodeId;
    if (value != null) {
      result
        ..add('country_code_id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  AppUser deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AppUserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'login':
          result.login = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'full_name':
          result.fullName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'national_id_number':
          result.nationalIdNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'national_id_status':
          result.nationalIdStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'deall_score':
          result.deallScore = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'max_purchase_power':
          result.maxPurchasePower = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'processing_fee':
          result.processingFee = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double?;
          break;
        case 'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'date_of_birth':
          result.dateOfBirth = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'address':
          result.address = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'mobile_number':
          result.mobileNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'national_id_front':
          result.nationalIdFront.replace(serializers.deserialize(value,
              specifiedType: const FullType(NationalId))! as NationalId);
          break;
        case 'national_id_back':
          result.nationalIdBack.replace(serializers.deserialize(value,
              specifiedType: const FullType(NationalId))! as NationalId);
          break;
        case 'profile_pic':
          result.profilePic.replace(serializers.deserialize(value,
              specifiedType: const FullType(NationalId))! as NationalId);
          break;
        case 'terms_and_conditions_accepted_at':
          result.termsAndConditionsAcceptedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'email_confirmed_at':
          result.emailVerifiedAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'first_name':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'last_name':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'bank_statement':
          result.bankStatement.replace(serializers.deserialize(value,
              specifiedType: const FullType(CustomerDocs))! as CustomerDocs);
          break;
        case 'employment_letter':
          result.employmentLetter.replace(serializers.deserialize(value,
              specifiedType: const FullType(CustomerDocs))! as CustomerDocs);
          break;
        case 'is_active':
          result.isActive = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'purchase_id':
          result.purchaseID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'national_id':
          result.nationalID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'national_id_front_url':
          result.nationalIdFrontURL = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'customer_payment_status':
          result.customerPaymentStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'total_amount':
          result.totalAmount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'minimum_amount':
          result.minimumAmount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'maximum_amount':
          result.maximumAmount = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'no_of_installments_paid':
          result.noOfInstallmentsPaid = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'profile_status':
          result.profileStatus = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'due_date':
          result.dueData = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'customer_due_status':
          result.customerDueStatus = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool?;
          break;
        case 'no_of_unread_notifications':
          result.noOfUnreadNotifications = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'token':
          result.token = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'country_code':
          result.countryCode.replace(serializers.deserialize(value,
              specifiedType: const FullType(CountryCode))! as CountryCode);
          break;
        case 'country_code_id':
          result.countryCodeId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
      }
    }

    return result.build();
  }
}

class _$NationalIdSerializer implements StructuredSerializer<NationalId> {
  @override
  final Iterable<Type> types = const [NationalId, _$NationalId];
  @override
  final String wireName = 'NationalId';

  @override
  Iterable<Object?> serialize(Serializers serializers, NationalId object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.userId;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.filename;
    if (value != null) {
      result
        ..add('filename')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.s3Key;
    if (value != null) {
      result
        ..add('s3_key')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.s3Url;
    if (value != null) {
      result
        ..add('s3_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  NationalId deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NationalIdBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'filename':
          result.filename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 's3_key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 's3_url':
          result.s3Url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$CustomerDocsSerializer implements StructuredSerializer<CustomerDocs> {
  @override
  final Iterable<Type> types = const [CustomerDocs, _$CustomerDocs];
  @override
  final String wireName = 'CustomerDocs';

  @override
  Iterable<Object?> serialize(Serializers serializers, CustomerDocs object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.userId;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.filename;
    if (value != null) {
      result
        ..add('filename')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.s3Key;
    if (value != null) {
      result
        ..add('s3_key')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.s3Url;
    if (value != null) {
      result
        ..add('s3_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.status;
    if (value != null) {
      result
        ..add('attachment_status')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CustomerDocs deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CustomerDocsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'filename':
          result.filename = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 's3_key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 's3_url':
          result.s3Url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'attachment_status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$CountryCodeSerializer implements StructuredSerializer<CountryCode> {
  @override
  final Iterable<Type> types = const [CountryCode, _$CountryCode];
  @override
  final String wireName = 'CountryCode';

  @override
  Iterable<Object?> serialize(Serializers serializers, CountryCode object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.name;
    if (value != null) {
      result
        ..add('name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.flag;
    if (value != null) {
      result
        ..add('flag')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.isdCode;
    if (value != null) {
      result
        ..add('isd_code')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CountryCode deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new CountryCodeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'flag':
          result.flag = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'isd_code':
          result.isdCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$AppUser extends AppUser {
  @override
  final String? login;
  @override
  final int? userId;
  @override
  final String? fullName;
  @override
  final String? email;
  @override
  final String? nationalIdNumber;
  @override
  final String? nationalIdStatus;
  @override
  final double? deallScore;
  @override
  final int? maxPurchasePower;
  @override
  final double? processingFee;
  @override
  final String? status;
  @override
  final String? dateOfBirth;
  @override
  final String? address;
  @override
  final String? mobileNumber;
  @override
  final NationalId? nationalIdFront;
  @override
  final NationalId? nationalIdBack;
  @override
  final NationalId? profilePic;
  @override
  final String? termsAndConditionsAcceptedAt;
  @override
  final String? emailVerifiedAt;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final CustomerDocs? bankStatement;
  @override
  final CustomerDocs? employmentLetter;
  @override
  final String? isActive;
  @override
  final String? purchaseID;
  @override
  final String? nationalID;
  @override
  final String? nationalIdFrontURL;
  @override
  final String? customerPaymentStatus;
  @override
  final int? totalAmount;
  @override
  final int? minimumAmount;
  @override
  final int? maximumAmount;
  @override
  final int? noOfInstallmentsPaid;
  @override
  final String? profileStatus;
  @override
  final String? dueData;
  @override
  final bool? customerDueStatus;
  @override
  final int? noOfUnreadNotifications;
  @override
  final String? token;
  @override
  final CountryCode? countryCode;
  @override
  final int? countryCodeId;

  factory _$AppUser([void Function(AppUserBuilder)? updates]) =>
      (new AppUserBuilder()..update(updates))._build();

  _$AppUser._(
      {this.login,
      this.userId,
      this.fullName,
      this.email,
      this.nationalIdNumber,
      this.nationalIdStatus,
      this.deallScore,
      this.maxPurchasePower,
      this.processingFee,
      this.status,
      this.dateOfBirth,
      this.address,
      this.mobileNumber,
      this.nationalIdFront,
      this.nationalIdBack,
      this.profilePic,
      this.termsAndConditionsAcceptedAt,
      this.emailVerifiedAt,
      this.firstName,
      this.lastName,
      this.bankStatement,
      this.employmentLetter,
      this.isActive,
      this.purchaseID,
      this.nationalID,
      this.nationalIdFrontURL,
      this.customerPaymentStatus,
      this.totalAmount,
      this.minimumAmount,
      this.maximumAmount,
      this.noOfInstallmentsPaid,
      this.profileStatus,
      this.dueData,
      this.customerDueStatus,
      this.noOfUnreadNotifications,
      this.token,
      this.countryCode,
      this.countryCodeId})
      : super._();

  @override
  AppUser rebuild(void Function(AppUserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AppUserBuilder toBuilder() => new AppUserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppUser &&
        login == other.login &&
        userId == other.userId &&
        fullName == other.fullName &&
        email == other.email &&
        nationalIdNumber == other.nationalIdNumber &&
        nationalIdStatus == other.nationalIdStatus &&
        deallScore == other.deallScore &&
        maxPurchasePower == other.maxPurchasePower &&
        processingFee == other.processingFee &&
        status == other.status &&
        dateOfBirth == other.dateOfBirth &&
        address == other.address &&
        mobileNumber == other.mobileNumber &&
        nationalIdFront == other.nationalIdFront &&
        nationalIdBack == other.nationalIdBack &&
        profilePic == other.profilePic &&
        termsAndConditionsAcceptedAt == other.termsAndConditionsAcceptedAt &&
        emailVerifiedAt == other.emailVerifiedAt &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        bankStatement == other.bankStatement &&
        employmentLetter == other.employmentLetter &&
        isActive == other.isActive &&
        purchaseID == other.purchaseID &&
        nationalID == other.nationalID &&
        nationalIdFrontURL == other.nationalIdFrontURL &&
        customerPaymentStatus == other.customerPaymentStatus &&
        totalAmount == other.totalAmount &&
        minimumAmount == other.minimumAmount &&
        maximumAmount == other.maximumAmount &&
        noOfInstallmentsPaid == other.noOfInstallmentsPaid &&
        profileStatus == other.profileStatus &&
        dueData == other.dueData &&
        customerDueStatus == other.customerDueStatus &&
        noOfUnreadNotifications == other.noOfUnreadNotifications &&
        token == other.token &&
        countryCode == other.countryCode &&
        countryCodeId == other.countryCodeId;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, login.hashCode);
    _$hash = $jc(_$hash, userId.hashCode);
    _$hash = $jc(_$hash, fullName.hashCode);
    _$hash = $jc(_$hash, email.hashCode);
    _$hash = $jc(_$hash, nationalIdNumber.hashCode);
    _$hash = $jc(_$hash, nationalIdStatus.hashCode);
    _$hash = $jc(_$hash, deallScore.hashCode);
    _$hash = $jc(_$hash, maxPurchasePower.hashCode);
    _$hash = $jc(_$hash, processingFee.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jc(_$hash, dateOfBirth.hashCode);
    _$hash = $jc(_$hash, address.hashCode);
    _$hash = $jc(_$hash, mobileNumber.hashCode);
    _$hash = $jc(_$hash, nationalIdFront.hashCode);
    _$hash = $jc(_$hash, nationalIdBack.hashCode);
    _$hash = $jc(_$hash, profilePic.hashCode);
    _$hash = $jc(_$hash, termsAndConditionsAcceptedAt.hashCode);
    _$hash = $jc(_$hash, emailVerifiedAt.hashCode);
    _$hash = $jc(_$hash, firstName.hashCode);
    _$hash = $jc(_$hash, lastName.hashCode);
    _$hash = $jc(_$hash, bankStatement.hashCode);
    _$hash = $jc(_$hash, employmentLetter.hashCode);
    _$hash = $jc(_$hash, isActive.hashCode);
    _$hash = $jc(_$hash, purchaseID.hashCode);
    _$hash = $jc(_$hash, nationalID.hashCode);
    _$hash = $jc(_$hash, nationalIdFrontURL.hashCode);
    _$hash = $jc(_$hash, customerPaymentStatus.hashCode);
    _$hash = $jc(_$hash, totalAmount.hashCode);
    _$hash = $jc(_$hash, minimumAmount.hashCode);
    _$hash = $jc(_$hash, maximumAmount.hashCode);
    _$hash = $jc(_$hash, noOfInstallmentsPaid.hashCode);
    _$hash = $jc(_$hash, profileStatus.hashCode);
    _$hash = $jc(_$hash, dueData.hashCode);
    _$hash = $jc(_$hash, customerDueStatus.hashCode);
    _$hash = $jc(_$hash, noOfUnreadNotifications.hashCode);
    _$hash = $jc(_$hash, token.hashCode);
    _$hash = $jc(_$hash, countryCode.hashCode);
    _$hash = $jc(_$hash, countryCodeId.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AppUser')
          ..add('login', login)
          ..add('userId', userId)
          ..add('fullName', fullName)
          ..add('email', email)
          ..add('nationalIdNumber', nationalIdNumber)
          ..add('nationalIdStatus', nationalIdStatus)
          ..add('deallScore', deallScore)
          ..add('maxPurchasePower', maxPurchasePower)
          ..add('processingFee', processingFee)
          ..add('status', status)
          ..add('dateOfBirth', dateOfBirth)
          ..add('address', address)
          ..add('mobileNumber', mobileNumber)
          ..add('nationalIdFront', nationalIdFront)
          ..add('nationalIdBack', nationalIdBack)
          ..add('profilePic', profilePic)
          ..add('termsAndConditionsAcceptedAt', termsAndConditionsAcceptedAt)
          ..add('emailVerifiedAt', emailVerifiedAt)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('bankStatement', bankStatement)
          ..add('employmentLetter', employmentLetter)
          ..add('isActive', isActive)
          ..add('purchaseID', purchaseID)
          ..add('nationalID', nationalID)
          ..add('nationalIdFrontURL', nationalIdFrontURL)
          ..add('customerPaymentStatus', customerPaymentStatus)
          ..add('totalAmount', totalAmount)
          ..add('minimumAmount', minimumAmount)
          ..add('maximumAmount', maximumAmount)
          ..add('noOfInstallmentsPaid', noOfInstallmentsPaid)
          ..add('profileStatus', profileStatus)
          ..add('dueData', dueData)
          ..add('customerDueStatus', customerDueStatus)
          ..add('noOfUnreadNotifications', noOfUnreadNotifications)
          ..add('token', token)
          ..add('countryCode', countryCode)
          ..add('countryCodeId', countryCodeId))
        .toString();
  }
}

class AppUserBuilder implements Builder<AppUser, AppUserBuilder> {
  _$AppUser? _$v;

  String? _login;
  String? get login => _$this._login;
  set login(String? login) => _$this._login = login;

  int? _userId;
  int? get userId => _$this._userId;
  set userId(int? userId) => _$this._userId = userId;

  String? _fullName;
  String? get fullName => _$this._fullName;
  set fullName(String? fullName) => _$this._fullName = fullName;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _nationalIdNumber;
  String? get nationalIdNumber => _$this._nationalIdNumber;
  set nationalIdNumber(String? nationalIdNumber) =>
      _$this._nationalIdNumber = nationalIdNumber;

  String? _nationalIdStatus;
  String? get nationalIdStatus => _$this._nationalIdStatus;
  set nationalIdStatus(String? nationalIdStatus) =>
      _$this._nationalIdStatus = nationalIdStatus;

  double? _deallScore;
  double? get deallScore => _$this._deallScore;
  set deallScore(double? deallScore) => _$this._deallScore = deallScore;

  int? _maxPurchasePower;
  int? get maxPurchasePower => _$this._maxPurchasePower;
  set maxPurchasePower(int? maxPurchasePower) =>
      _$this._maxPurchasePower = maxPurchasePower;

  double? _processingFee;
  double? get processingFee => _$this._processingFee;
  set processingFee(double? processingFee) =>
      _$this._processingFee = processingFee;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  String? _dateOfBirth;
  String? get dateOfBirth => _$this._dateOfBirth;
  set dateOfBirth(String? dateOfBirth) => _$this._dateOfBirth = dateOfBirth;

  String? _address;
  String? get address => _$this._address;
  set address(String? address) => _$this._address = address;

  String? _mobileNumber;
  String? get mobileNumber => _$this._mobileNumber;
  set mobileNumber(String? mobileNumber) => _$this._mobileNumber = mobileNumber;

  NationalIdBuilder? _nationalIdFront;
  NationalIdBuilder get nationalIdFront =>
      _$this._nationalIdFront ??= new NationalIdBuilder();
  set nationalIdFront(NationalIdBuilder? nationalIdFront) =>
      _$this._nationalIdFront = nationalIdFront;

  NationalIdBuilder? _nationalIdBack;
  NationalIdBuilder get nationalIdBack =>
      _$this._nationalIdBack ??= new NationalIdBuilder();
  set nationalIdBack(NationalIdBuilder? nationalIdBack) =>
      _$this._nationalIdBack = nationalIdBack;

  NationalIdBuilder? _profilePic;
  NationalIdBuilder get profilePic =>
      _$this._profilePic ??= new NationalIdBuilder();
  set profilePic(NationalIdBuilder? profilePic) =>
      _$this._profilePic = profilePic;

  String? _termsAndConditionsAcceptedAt;
  String? get termsAndConditionsAcceptedAt =>
      _$this._termsAndConditionsAcceptedAt;
  set termsAndConditionsAcceptedAt(String? termsAndConditionsAcceptedAt) =>
      _$this._termsAndConditionsAcceptedAt = termsAndConditionsAcceptedAt;

  String? _emailVerifiedAt;
  String? get emailVerifiedAt => _$this._emailVerifiedAt;
  set emailVerifiedAt(String? emailVerifiedAt) =>
      _$this._emailVerifiedAt = emailVerifiedAt;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  CustomerDocsBuilder? _bankStatement;
  CustomerDocsBuilder get bankStatement =>
      _$this._bankStatement ??= new CustomerDocsBuilder();
  set bankStatement(CustomerDocsBuilder? bankStatement) =>
      _$this._bankStatement = bankStatement;

  CustomerDocsBuilder? _employmentLetter;
  CustomerDocsBuilder get employmentLetter =>
      _$this._employmentLetter ??= new CustomerDocsBuilder();
  set employmentLetter(CustomerDocsBuilder? employmentLetter) =>
      _$this._employmentLetter = employmentLetter;

  String? _isActive;
  String? get isActive => _$this._isActive;
  set isActive(String? isActive) => _$this._isActive = isActive;

  String? _purchaseID;
  String? get purchaseID => _$this._purchaseID;
  set purchaseID(String? purchaseID) => _$this._purchaseID = purchaseID;

  String? _nationalID;
  String? get nationalID => _$this._nationalID;
  set nationalID(String? nationalID) => _$this._nationalID = nationalID;

  String? _nationalIdFrontURL;
  String? get nationalIdFrontURL => _$this._nationalIdFrontURL;
  set nationalIdFrontURL(String? nationalIdFrontURL) =>
      _$this._nationalIdFrontURL = nationalIdFrontURL;

  String? _customerPaymentStatus;
  String? get customerPaymentStatus => _$this._customerPaymentStatus;
  set customerPaymentStatus(String? customerPaymentStatus) =>
      _$this._customerPaymentStatus = customerPaymentStatus;

  int? _totalAmount;
  int? get totalAmount => _$this._totalAmount;
  set totalAmount(int? totalAmount) => _$this._totalAmount = totalAmount;

  int? _minimumAmount;
  int? get minimumAmount => _$this._minimumAmount;
  set minimumAmount(int? minimumAmount) =>
      _$this._minimumAmount = minimumAmount;

  int? _maximumAmount;
  int? get maximumAmount => _$this._maximumAmount;
  set maximumAmount(int? maximumAmount) =>
      _$this._maximumAmount = maximumAmount;

  int? _noOfInstallmentsPaid;
  int? get noOfInstallmentsPaid => _$this._noOfInstallmentsPaid;
  set noOfInstallmentsPaid(int? noOfInstallmentsPaid) =>
      _$this._noOfInstallmentsPaid = noOfInstallmentsPaid;

  String? _profileStatus;
  String? get profileStatus => _$this._profileStatus;
  set profileStatus(String? profileStatus) =>
      _$this._profileStatus = profileStatus;

  String? _dueData;
  String? get dueData => _$this._dueData;
  set dueData(String? dueData) => _$this._dueData = dueData;

  bool? _customerDueStatus;
  bool? get customerDueStatus => _$this._customerDueStatus;
  set customerDueStatus(bool? customerDueStatus) =>
      _$this._customerDueStatus = customerDueStatus;

  int? _noOfUnreadNotifications;
  int? get noOfUnreadNotifications => _$this._noOfUnreadNotifications;
  set noOfUnreadNotifications(int? noOfUnreadNotifications) =>
      _$this._noOfUnreadNotifications = noOfUnreadNotifications;

  String? _token;
  String? get token => _$this._token;
  set token(String? token) => _$this._token = token;

  CountryCodeBuilder? _countryCode;
  CountryCodeBuilder get countryCode =>
      _$this._countryCode ??= new CountryCodeBuilder();
  set countryCode(CountryCodeBuilder? countryCode) =>
      _$this._countryCode = countryCode;

  int? _countryCodeId;
  int? get countryCodeId => _$this._countryCodeId;
  set countryCodeId(int? countryCodeId) =>
      _$this._countryCodeId = countryCodeId;

  AppUserBuilder();

  AppUserBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _login = $v.login;
      _userId = $v.userId;
      _fullName = $v.fullName;
      _email = $v.email;
      _nationalIdNumber = $v.nationalIdNumber;
      _nationalIdStatus = $v.nationalIdStatus;
      _deallScore = $v.deallScore;
      _maxPurchasePower = $v.maxPurchasePower;
      _processingFee = $v.processingFee;
      _status = $v.status;
      _dateOfBirth = $v.dateOfBirth;
      _address = $v.address;
      _mobileNumber = $v.mobileNumber;
      _nationalIdFront = $v.nationalIdFront?.toBuilder();
      _nationalIdBack = $v.nationalIdBack?.toBuilder();
      _profilePic = $v.profilePic?.toBuilder();
      _termsAndConditionsAcceptedAt = $v.termsAndConditionsAcceptedAt;
      _emailVerifiedAt = $v.emailVerifiedAt;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _bankStatement = $v.bankStatement?.toBuilder();
      _employmentLetter = $v.employmentLetter?.toBuilder();
      _isActive = $v.isActive;
      _purchaseID = $v.purchaseID;
      _nationalID = $v.nationalID;
      _nationalIdFrontURL = $v.nationalIdFrontURL;
      _customerPaymentStatus = $v.customerPaymentStatus;
      _totalAmount = $v.totalAmount;
      _minimumAmount = $v.minimumAmount;
      _maximumAmount = $v.maximumAmount;
      _noOfInstallmentsPaid = $v.noOfInstallmentsPaid;
      _profileStatus = $v.profileStatus;
      _dueData = $v.dueData;
      _customerDueStatus = $v.customerDueStatus;
      _noOfUnreadNotifications = $v.noOfUnreadNotifications;
      _token = $v.token;
      _countryCode = $v.countryCode?.toBuilder();
      _countryCodeId = $v.countryCodeId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppUser other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AppUser;
  }

  @override
  void update(void Function(AppUserBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AppUser build() => _build();

  _$AppUser _build() {
    _$AppUser _$result;
    try {
      _$result = _$v ??
          new _$AppUser._(
              login: login,
              userId: userId,
              fullName: fullName,
              email: email,
              nationalIdNumber: nationalIdNumber,
              nationalIdStatus: nationalIdStatus,
              deallScore: deallScore,
              maxPurchasePower: maxPurchasePower,
              processingFee: processingFee,
              status: status,
              dateOfBirth: dateOfBirth,
              address: address,
              mobileNumber: mobileNumber,
              nationalIdFront: _nationalIdFront?.build(),
              nationalIdBack: _nationalIdBack?.build(),
              profilePic: _profilePic?.build(),
              termsAndConditionsAcceptedAt: termsAndConditionsAcceptedAt,
              emailVerifiedAt: emailVerifiedAt,
              firstName: firstName,
              lastName: lastName,
              bankStatement: _bankStatement?.build(),
              employmentLetter: _employmentLetter?.build(),
              isActive: isActive,
              purchaseID: purchaseID,
              nationalID: nationalID,
              nationalIdFrontURL: nationalIdFrontURL,
              customerPaymentStatus: customerPaymentStatus,
              totalAmount: totalAmount,
              minimumAmount: minimumAmount,
              maximumAmount: maximumAmount,
              noOfInstallmentsPaid: noOfInstallmentsPaid,
              profileStatus: profileStatus,
              dueData: dueData,
              customerDueStatus: customerDueStatus,
              noOfUnreadNotifications: noOfUnreadNotifications,
              token: token,
              countryCode: _countryCode?.build(),
              countryCodeId: countryCodeId);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'nationalIdFront';
        _nationalIdFront?.build();
        _$failedField = 'nationalIdBack';
        _nationalIdBack?.build();
        _$failedField = 'profilePic';
        _profilePic?.build();

        _$failedField = 'bankStatement';
        _bankStatement?.build();
        _$failedField = 'employmentLetter';
        _employmentLetter?.build();

        _$failedField = 'countryCode';
        _countryCode?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'AppUser', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$NationalId extends NationalId {
  @override
  final int? userId;
  @override
  final String? filename;
  @override
  final String? s3Key;
  @override
  final String? s3Url;

  factory _$NationalId([void Function(NationalIdBuilder)? updates]) =>
      (new NationalIdBuilder()..update(updates))._build();

  _$NationalId._({this.userId, this.filename, this.s3Key, this.s3Url})
      : super._();

  @override
  NationalId rebuild(void Function(NationalIdBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NationalIdBuilder toBuilder() => new NationalIdBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NationalId &&
        userId == other.userId &&
        filename == other.filename &&
        s3Key == other.s3Key &&
        s3Url == other.s3Url;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, userId.hashCode);
    _$hash = $jc(_$hash, filename.hashCode);
    _$hash = $jc(_$hash, s3Key.hashCode);
    _$hash = $jc(_$hash, s3Url.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'NationalId')
          ..add('userId', userId)
          ..add('filename', filename)
          ..add('s3Key', s3Key)
          ..add('s3Url', s3Url))
        .toString();
  }
}

class NationalIdBuilder implements Builder<NationalId, NationalIdBuilder> {
  _$NationalId? _$v;

  int? _userId;
  int? get userId => _$this._userId;
  set userId(int? userId) => _$this._userId = userId;

  String? _filename;
  String? get filename => _$this._filename;
  set filename(String? filename) => _$this._filename = filename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _s3Url;
  String? get s3Url => _$this._s3Url;
  set s3Url(String? s3Url) => _$this._s3Url = s3Url;

  NationalIdBuilder();

  NationalIdBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _userId = $v.userId;
      _filename = $v.filename;
      _s3Key = $v.s3Key;
      _s3Url = $v.s3Url;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NationalId other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$NationalId;
  }

  @override
  void update(void Function(NationalIdBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  NationalId build() => _build();

  _$NationalId _build() {
    final _$result = _$v ??
        new _$NationalId._(
            userId: userId, filename: filename, s3Key: s3Key, s3Url: s3Url);
    replace(_$result);
    return _$result;
  }
}

class _$CustomerDocs extends CustomerDocs {
  @override
  final int? userId;
  @override
  final String? filename;
  @override
  final String? s3Key;
  @override
  final String? s3Url;
  @override
  final String? status;

  factory _$CustomerDocs([void Function(CustomerDocsBuilder)? updates]) =>
      (new CustomerDocsBuilder()..update(updates))._build();

  _$CustomerDocs._(
      {this.userId, this.filename, this.s3Key, this.s3Url, this.status})
      : super._();

  @override
  CustomerDocs rebuild(void Function(CustomerDocsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CustomerDocsBuilder toBuilder() => new CustomerDocsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CustomerDocs &&
        userId == other.userId &&
        filename == other.filename &&
        s3Key == other.s3Key &&
        s3Url == other.s3Url &&
        status == other.status;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, userId.hashCode);
    _$hash = $jc(_$hash, filename.hashCode);
    _$hash = $jc(_$hash, s3Key.hashCode);
    _$hash = $jc(_$hash, s3Url.hashCode);
    _$hash = $jc(_$hash, status.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CustomerDocs')
          ..add('userId', userId)
          ..add('filename', filename)
          ..add('s3Key', s3Key)
          ..add('s3Url', s3Url)
          ..add('status', status))
        .toString();
  }
}

class CustomerDocsBuilder
    implements Builder<CustomerDocs, CustomerDocsBuilder> {
  _$CustomerDocs? _$v;

  int? _userId;
  int? get userId => _$this._userId;
  set userId(int? userId) => _$this._userId = userId;

  String? _filename;
  String? get filename => _$this._filename;
  set filename(String? filename) => _$this._filename = filename;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _s3Url;
  String? get s3Url => _$this._s3Url;
  set s3Url(String? s3Url) => _$this._s3Url = s3Url;

  String? _status;
  String? get status => _$this._status;
  set status(String? status) => _$this._status = status;

  CustomerDocsBuilder();

  CustomerDocsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _userId = $v.userId;
      _filename = $v.filename;
      _s3Key = $v.s3Key;
      _s3Url = $v.s3Url;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CustomerDocs other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CustomerDocs;
  }

  @override
  void update(void Function(CustomerDocsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CustomerDocs build() => _build();

  _$CustomerDocs _build() {
    final _$result = _$v ??
        new _$CustomerDocs._(
            userId: userId,
            filename: filename,
            s3Key: s3Key,
            s3Url: s3Url,
            status: status);
    replace(_$result);
    return _$result;
  }
}

class _$CountryCode extends CountryCode {
  @override
  final int? id;
  @override
  final String? name;
  @override
  final String? flag;
  @override
  final String? isdCode;

  factory _$CountryCode([void Function(CountryCodeBuilder)? updates]) =>
      (new CountryCodeBuilder()..update(updates))._build();

  _$CountryCode._({this.id, this.name, this.flag, this.isdCode}) : super._();

  @override
  CountryCode rebuild(void Function(CountryCodeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CountryCodeBuilder toBuilder() => new CountryCodeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CountryCode &&
        id == other.id &&
        name == other.name &&
        flag == other.flag &&
        isdCode == other.isdCode;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, name.hashCode);
    _$hash = $jc(_$hash, flag.hashCode);
    _$hash = $jc(_$hash, isdCode.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CountryCode')
          ..add('id', id)
          ..add('name', name)
          ..add('flag', flag)
          ..add('isdCode', isdCode))
        .toString();
  }
}

class CountryCodeBuilder implements Builder<CountryCode, CountryCodeBuilder> {
  _$CountryCode? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _flag;
  String? get flag => _$this._flag;
  set flag(String? flag) => _$this._flag = flag;

  String? _isdCode;
  String? get isdCode => _$this._isdCode;
  set isdCode(String? isdCode) => _$this._isdCode = isdCode;

  CountryCodeBuilder();

  CountryCodeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _name = $v.name;
      _flag = $v.flag;
      _isdCode = $v.isdCode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CountryCode other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CountryCode;
  }

  @override
  void update(void Function(CountryCodeBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CountryCode build() => _build();

  _$CountryCode _build() {
    final _$result = _$v ??
        new _$CountryCode._(id: id, name: name, flag: flag, isdCode: isdCode);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
