import 'dart:io';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'file_attachment.g.dart';

abstract class FileAttachment
    implements Built<FileAttachment, FileAttachmentBuilder> {
  factory FileAttachment(
      [FileAttachmentBuilder Function(FileAttachmentBuilder builder)
          updates]) = _$FileAttachment;

  FileAttachment._();

  int? get id;

  @BuiltValueField(wireName: 's3_key')
  String? get s3Key;

  @BuiltValueField(wireName: 'file_name')
  String? get fileName;

  File? get attachmentFile;

  @BuiltValueField(wireName: 'file_type')
  String? get fileType;

  @BuiltValueField(wireName: 'attachment_url')
  String? get attachmentUrl;

  static Serializer<FileAttachment> get serializer =>
      _$fileAttachmentSerializer;
}
