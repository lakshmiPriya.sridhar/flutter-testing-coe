import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_test_coe/models/models.dart';

part 'device_token.g.dart';

abstract class DeviceToken implements Built<DeviceToken, DeviceTokenBuilder> {
  factory DeviceToken(
          [DeviceTokenBuilder Function(DeviceTokenBuilder builder) updates]) =
      _$DeviceToken;

  DeviceToken._();

  int? get id;

  @BuiltValueField(wireName: 'platform')
  AccessToken? get platform;

  static Serializer<DeviceToken> get serializer => _$deviceTokenSerializer;
}
