// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'file_attachment.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<FileAttachment> _$fileAttachmentSerializer =
    new _$FileAttachmentSerializer();

class _$FileAttachmentSerializer
    implements StructuredSerializer<FileAttachment> {
  @override
  final Iterable<Type> types = const [FileAttachment, _$FileAttachment];
  @override
  final String wireName = 'FileAttachment';

  @override
  Iterable<Object?> serialize(Serializers serializers, FileAttachment object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.id;
    if (value != null) {
      result
        ..add('id')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.s3Key;
    if (value != null) {
      result
        ..add('s3_key')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.fileName;
    if (value != null) {
      result
        ..add('file_name')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.attachmentFile;
    if (value != null) {
      result
        ..add('attachmentFile')
        ..add(
            serializers.serialize(value, specifiedType: const FullType(File)));
    }
    value = object.fileType;
    if (value != null) {
      result
        ..add('file_type')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.attachmentUrl;
    if (value != null) {
      result
        ..add('attachment_url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  FileAttachment deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new FileAttachmentBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current! as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int?;
          break;
        case 's3_key':
          result.s3Key = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'file_name':
          result.fileName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'attachmentFile':
          result.attachmentFile = serializers.deserialize(value,
              specifiedType: const FullType(File)) as File?;
          break;
        case 'file_type':
          result.fileType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
        case 'attachment_url':
          result.attachmentUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String?;
          break;
      }
    }

    return result.build();
  }
}

class _$FileAttachment extends FileAttachment {
  @override
  final int? id;
  @override
  final String? s3Key;
  @override
  final String? fileName;
  @override
  final File? attachmentFile;
  @override
  final String? fileType;
  @override
  final String? attachmentUrl;

  factory _$FileAttachment([void Function(FileAttachmentBuilder)? updates]) =>
      (new FileAttachmentBuilder()..update(updates))._build();

  _$FileAttachment._(
      {this.id,
      this.s3Key,
      this.fileName,
      this.attachmentFile,
      this.fileType,
      this.attachmentUrl})
      : super._();

  @override
  FileAttachment rebuild(void Function(FileAttachmentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FileAttachmentBuilder toBuilder() =>
      new FileAttachmentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FileAttachment &&
        id == other.id &&
        s3Key == other.s3Key &&
        fileName == other.fileName &&
        attachmentFile == other.attachmentFile &&
        fileType == other.fileType &&
        attachmentUrl == other.attachmentUrl;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, id.hashCode);
    _$hash = $jc(_$hash, s3Key.hashCode);
    _$hash = $jc(_$hash, fileName.hashCode);
    _$hash = $jc(_$hash, attachmentFile.hashCode);
    _$hash = $jc(_$hash, fileType.hashCode);
    _$hash = $jc(_$hash, attachmentUrl.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'FileAttachment')
          ..add('id', id)
          ..add('s3Key', s3Key)
          ..add('fileName', fileName)
          ..add('attachmentFile', attachmentFile)
          ..add('fileType', fileType)
          ..add('attachmentUrl', attachmentUrl))
        .toString();
  }
}

class FileAttachmentBuilder
    implements Builder<FileAttachment, FileAttachmentBuilder> {
  _$FileAttachment? _$v;

  int? _id;
  int? get id => _$this._id;
  set id(int? id) => _$this._id = id;

  String? _s3Key;
  String? get s3Key => _$this._s3Key;
  set s3Key(String? s3Key) => _$this._s3Key = s3Key;

  String? _fileName;
  String? get fileName => _$this._fileName;
  set fileName(String? fileName) => _$this._fileName = fileName;

  File? _attachmentFile;
  File? get attachmentFile => _$this._attachmentFile;
  set attachmentFile(File? attachmentFile) =>
      _$this._attachmentFile = attachmentFile;

  String? _fileType;
  String? get fileType => _$this._fileType;
  set fileType(String? fileType) => _$this._fileType = fileType;

  String? _attachmentUrl;
  String? get attachmentUrl => _$this._attachmentUrl;
  set attachmentUrl(String? attachmentUrl) =>
      _$this._attachmentUrl = attachmentUrl;

  FileAttachmentBuilder();

  FileAttachmentBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _s3Key = $v.s3Key;
      _fileName = $v.fileName;
      _attachmentFile = $v.attachmentFile;
      _fileType = $v.fileType;
      _attachmentUrl = $v.attachmentUrl;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FileAttachment other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$FileAttachment;
  }

  @override
  void update(void Function(FileAttachmentBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  FileAttachment build() => _build();

  _$FileAttachment _build() {
    final _$result = _$v ??
        new _$FileAttachment._(
            id: id,
            s3Key: s3Key,
            fileName: fileName,
            attachmentFile: attachmentFile,
            fileType: fileType,
            attachmentUrl: attachmentUrl);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
