import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter_test_coe/models/models.dart';

part 'api_success.g.dart';

abstract class ApiSuccess implements Built<ApiSuccess, ApiSuccessBuilder> {
  factory ApiSuccess(
          [ApiSuccessBuilder Function(ApiSuccessBuilder builder) updates]) =
      _$ApiSuccess;

  ApiSuccess._();

  int? get status;

  String? get message;

  String? get success;

  @BuiltValueField(wireName: 'payment_status')
  String? get paymentStatus;

//***************************** pagination ***********************************//
  Pagination? get meta;

  @BuiltValueField(wireName: 'country_codes')
  BuiltList<CountryCode>? get countryCodeMeta;

//******************************** auth-module ********************************//
  @BuiltValueField(wireName: 'customer')
  AppUser? get customer;

  AccessToken? get token;

//****************************** file-upload **********************************//

  @BuiltValueField(wireName: 'attachment')
  FileAttachment? get uploadedFile;

  @BuiltValueField(wireName: 'data')
  Attachments? get attachment;

  @BuiltValueField(wireName: 'device')
  DeviceToken? get userDeviceMap;

  static Serializer<ApiSuccess> get serializer => _$apiSuccessSerializer;
}
