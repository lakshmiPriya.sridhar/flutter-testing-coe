import 'dart:async';
import 'dart:io';
// import 'package:firebase_analytics/firebase_analytics.dart';
// import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_test_coe/actions/auth/auth_action.dart';
import 'package:flutter_test_coe/data/api/api_routes.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test_coe/data/app_repository.dart';
import 'package:flutter_test_coe/data/preference_client.dart';
import 'package:flutter_test_coe/middleware/middleware.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:flutter_test_coe/reducers/reducers.dart';
import 'package:flutter_test_coe/theme.dart';
import 'package:flutter_test_coe/views/init_page.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // await Firebase.initializeApp();
  debugPrint('----------------background message: ${message.notification!.body}-------------');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();

  await dotenv.load(fileName: '.env');
  final SharedPreferences prefs = await SharedPreferences.getInstance();

  final AppRepository repository = AppRepository(
      httpClient: http.Client(),
      preferencesClient: PreferencesClient(prefs: prefs),
      config: ApiRoutes.apiConfig);

  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
  ]);

  ///flutter in-app webview
  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);

    var swAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_BASIC_USAGE);
    var swInterceptAvailable = await AndroidWebViewFeature.isFeatureSupported(
        AndroidWebViewFeature.SERVICE_WORKER_SHOULD_INTERCEPT_REQUEST);

    if (swAvailable && swInterceptAvailable) {
      AndroidServiceWorkerController serviceWorkerController =
      AndroidServiceWorkerController.instance();

      await serviceWorkerController
          .setServiceWorkerClient(AndroidServiceWorkerClient(
        shouldInterceptRequest: (request) async {
          return null;
        },
      ));
    }
  }

  runApp(
    MyApp(
      repository: repository,
      prefs: prefs,
    ),
  );
}

class MyApp extends StatefulWidget {
  MyApp({Key? key, AppRepository? repository, required this.prefs})
      : store = Store<AppState>(
          reducer,
          middleware: middleware(repository!),
          initialState: AppState.initState(),
        ),
        super(key: key);

  final Store<AppState> store;
  final SharedPreferences? prefs;

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late Store<AppState> store;

  // FirebaseAnalytics analytics = FirebaseAnalytics.instance;

  @override
  void initState() {
    super.initState();
    store = widget.store;
    _init();
  }

  void _init() async {
    Future<void>.delayed(const Duration(seconds: 5), () {
      store.dispatch(CheckForUserInPrefs());
    });
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        navigatorKey: store.state.navigator,
        title: 'MyApp',
        theme: themeData,
        home: const InitPage(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
