import 'dart:convert';

import 'package:flutter_test_coe/models/models.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesClient {
  PreferencesClient({required this.prefs});

  final SharedPreferences prefs;

//******************************** user-details *******************************//
  AppUser? getUser() {
    final String? userString = prefs.getString('appUser');
    if (userString == null) {
      return null;
    }
    final dynamic user = json.decode(userString);
    return serializers.deserializeWith(AppUser.serializer, user);
  }

  void saveUser({AppUser? appUser}) {
    if (appUser == null) {
      prefs.setString('appUser', '');
      return;
    }
    final dynamic user = serializers.serializeWith(AppUser.serializer, appUser);
    final String userString = json.encode(user);
    prefs.setString('appUser', userString);
  }

//******************************** user-access-token **************************//
  String? getUserAccessToken() {
    final String? userAccessToken = prefs.getString('token');
    return userAccessToken;
  }

  void setUserAccessToken({String? token}) {
    prefs.setString('token', token ?? '');
  }

  //******************************** user-refresh-token **************************//
  String? getUserRefreshToken() {
    final String? userAccessToken = prefs.getString('refreshToken');
    return userAccessToken;
  }

  void setUserRefreshToken({String? token}) {
    prefs.setString('refreshToken', token ?? '');
  }

 //******************************** set-app-lang **************************//
  void setAppLanguage(String? locale) {
    prefs.setString('locale', locale ?? 'en');
  }

  //******************************** get-app-lang **************************//
  String? getAppLanguage() {
    return prefs.getString('locale');
  }
}
