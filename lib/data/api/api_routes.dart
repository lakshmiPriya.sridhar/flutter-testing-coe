import 'package:flutter_test_coe/data/api/api_client.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class ApiRoutes {
  static const ApiConfig developConfig = ApiConfig(
    scheme: 'https',
    host: 'deall-api-new.herokuapp.com',
    scope: scope,
  );

  static const ApiConfig stagingConfig = ApiConfig(
    scheme: 'https',
    host: 'api.staging.deall.app',
//    port: 443,
    scope: scope,
  );

  static const ApiConfig prodConfig = ApiConfig(
    scheme: 'https',
    host: 'api.deall.app',
    port: 443,
    scope: scope,
  );

  static ApiConfig apiConfig = ApiConfig(
    scheme: 'https',
    host: dotenv.env['BASE_URL'].toString(),
    port: 443,
    scope: scope,
  );

  static const ApiConfig testConfig = ApiConfig(
    scheme: 'http',
    host: "localhost.test",
    port: 443,
    scope: scope,
  );


  //Scope
  static const String debugScope = '';
  static const String scope = '/api/v1';
}
