import 'dart:async';
import 'dart:io';
import 'package:built_collection/built_collection.dart';
import 'package:flutter_test_coe/data/api/api_client.dart';
import 'package:flutter_test_coe/data/services/api_service.dart';
import 'package:flutter_test_coe/models/models.dart';

class AuthService extends ApiService {
  AuthService({required ApiClient client}) : super(client: client);

//********************************* request-mobile-otp ************************//
  Future<String?> requestMobileOtp({Map<String, dynamic>? objToApi}) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.POST,
        path: '/user_management/customer/auth/login',
        body: objToApi);
    if (res.isSuccess) {
      return res.data?.success;
    } else {
      throw res.error;
    }
  }

//********************************* verify-mobile-otp *************************//
  Future<Map<String, dynamic>> verifyMobileOtp(
      {Map<String, dynamic>? objToApi}) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.POST,
        path: '/user_management/customer/auth/verify_login_otp',
        body: objToApi);
    if (res.isSuccess) {
      return {'customer': res.data!.customer!, 'token': res.data!.token!};
    } else {
      throw res.error;
    }
  }

//********************************* get-user **********************************//
  Future<AppUser?> getUserDetails(
    Map<String, String>? headersToApi,
  ) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.GET,
        headers: headersToApi,
        path: '/customer_management/customers/me');
    if (res.isSuccess) {
      return res.data?.customer;
    } else if (res.isUnAuthorizedRequest) {
      throw true;
    } else {
      throw res.error;
    }
  }

//***************************** log-out ***************************************//
  Future<bool> logOut({Map<String, String>? headersToApi}) async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.DELETE,
        path: '/user_management/customer/auth/logout',
        headers: headersToApi);
    if (res.isSuccess) {
      return true;
    } else if (res.isUnAuthorizedRequest) {
      throw true;
    } else {
      throw res.error;
    }
  }

  //*************************** send-devise-token-to-backend ******************//
  Future<ApiSuccess?> sendDeviceID(
      {required Map<String, String> headersToApi, required String deviceId}) async {
    final Map<String, dynamic> objToApi = <String, dynamic>{
      'device': <String, dynamic>{
        'device_id': deviceId,
        'platform': Platform.isIOS ? 'ios' : 'android'
      }
    };
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
        method: Method.POST,
        path: '/user_management/customer/device',
        body: objToApi,
        headers: headersToApi);
    if (res.isSuccess) {
      return res.data;
    } else {
      throw res.error;
    }
  }

  //********************************* country-code-meta *****************************//
  Future<BuiltList<CountryCode>?> getCountryCodeMeta() async {
    final ApiResponse<ApiSuccess> res = await client!.callJsonApi<ApiSuccess>(
      method: Method.GET,
      path: '/meta/country_codes',
    );
    if (res.isSuccess) {
      return res.data?.countryCodeMeta;
    } else if (res.isUnAuthorizedRequest) {
      throw true;
    } else {
      throw res.error;
    }
  }
}

