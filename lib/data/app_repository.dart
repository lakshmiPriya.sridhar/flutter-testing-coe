import 'dart:async';

import 'package:flutter_test_coe/data/services/auth/auth_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/data/api/api_client.dart';
import 'package:flutter_test_coe/data/app_repository_provider.dart';
import 'package:flutter_test_coe/data/preference_client.dart';
import 'package:flutter_test_coe/data/services/api_service.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:http/http.dart' as http;

class AppRepository {
  AppRepository({required this.httpClient, required this.preferencesClient, required this.config})
      : assert(preferencesClient != null && config != null) {
    apiClient = ApiClient(httpClient, config: config!);
    services = <ApiService>[
      AuthService(client: apiClient!),
    ];
  }

  final PreferencesClient? preferencesClient;
  final ApiConfig? config;
  ApiClient? apiClient;
  List<ApiService>? services;
  final http.Client httpClient;

  static AppRepository of(BuildContext context) {
    final AppRepositoryProvider? provider =
        context.dependOnInheritedWidgetOfExactType<AppRepositoryProvider>();
    if (provider == null) {
      throw 'AppRepositoryProvider not found';
    }

    return provider.repository;
  }

  ApiService getService<T>() {
    return services!.firstWhere((ApiService s) => s is T);
  }

  Future<AppUser?> getUserFromPrefs() async {
    return preferencesClient!.getUser();
  }

  Future<void> setUserPrefs({AppUser? appUser}) async {
    preferencesClient!.saveUser(appUser: appUser);
  }

  //***************************** user-access-token ****************************//
  Future<void> setUserAccessToken({String? accessToken}) async {
    preferencesClient!.setUserAccessToken(token: accessToken);
  }

  Future<String?> getUserAccessToken() async {
    return preferencesClient!.getUserAccessToken();
  }

  //***************************** user-refresh-token ****************************//
  Future<void> setUserRefreshToken({String? refreshToken}) async {
    preferencesClient!.setUserRefreshToken(token: refreshToken);
  }

  Future<String?> getUserRefreshToken() async {
    return preferencesClient!.getUserRefreshToken();
  }

  //***************************** app-language ****************************//
  Future<void> setAppLanguage({String? locale}) async {
    preferencesClient!.setAppLanguage(locale);
  }

  Future<String?> getAppLanguage() async {
    return preferencesClient!.getAppLanguage();
  }
}
