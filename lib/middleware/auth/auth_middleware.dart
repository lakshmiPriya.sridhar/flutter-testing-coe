import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:flutter_test_coe/core/utils/utils.dart';
import 'package:flutter_test_coe/core/values/app_constants.dart';
import 'package:flutter_test_coe/global_widgets/dart_helper.dart';
import 'package:flutter_test_coe/global_widgets/toast_helper.dart';
import 'package:flutter_test_coe/views/auth/login_page.dart';
import 'package:flutter_test_coe/views/auth/otp_verification_page.dart';
import 'package:flutter_test_coe/views/home/home_page.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/actions/actions.dart';
import 'package:flutter_test_coe/data/app_repository.dart';
import 'package:flutter_test_coe/data/services/auth/auth_service.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:redux/redux.dart';
import 'firebase_helper.dart';

final bool isTest = Platform.environment.containsKey('FLUTTER_TEST');

class AuthMiddleware {
  AuthMiddleware({required this.repository})
      : authService = repository.getService<AuthService>() as AuthService;
  final AppRepository repository;
  String registrationToken = '';
  final AuthService authService;
  int notificationCount = 0;

  List<Middleware<AppState>> createAuthMiddleware() {
    return <Middleware<AppState>>[
      TypedMiddleware<AppState, CheckForUserInPrefs>(checkForUserInPrefs),
      TypedMiddleware<AppState, GetUserDetails>(getUserDetails),
      TypedMiddleware<AppState, RequestMobileOtp>(requestMobileOtp),
      TypedMiddleware<AppState, VerifyMobileOtp>(verifyMobileOtp),
      TypedMiddleware<AppState, LoginWithRefreshToken>(loginWithRefreshToken),
      TypedMiddleware<AppState, LogOutUser>(logOutUser),
      TypedMiddleware<AppState, ForceLogOutUser>(forceLogOutUser),
      TypedMiddleware<AppState, SetUpFireBaseListener>(setUpFireBaseListener),
      TypedMiddleware<AppState, GetCountryCodeMeta>(getCountryCodeMeta),
      TypedMiddleware<AppState, ChangeAppLanguage>(changeAppLanguage),
    ];
  }

  //********************************* checkForUserInPrefs ***********************//
  void checkForUserInPrefs(Store<AppState> store, CheckForUserInPrefs action,
      NextDispatcher next) async {
    try {
      store.dispatch(SetInitializer(false));
      store.dispatch(GetCountryCodeMeta());
      final AppUser? user = await repository.getUserFromPrefs();
      store.dispatch(SetInitializer(false));
      if (user != null && user.userId != null &&
          store.state.needRestart == true) {
        store.dispatch(SaveUser(userDetails: user));
        store.dispatch(GetUserDetails(callbackFunc: (AppUser user) {
          if (user.status == 'approved') {
            ///other api calls
            if (isTest == false) {
              store.dispatch(SetUpFireBaseListener());
            }
          }
        }));
      } else {
        store.dispatch(SaveUser(userDetails: null));
      }
    } catch (e) {
      store.dispatch(SetInitializer(false));
      return;
    }
    next(action);
  }

  //********************************* change-app-language **********************************//
  void changeAppLanguage(Store<AppState> store, ChangeAppLanguage action,
      NextDispatcher next) async {
    try {
      await repository.setAppLanguage(locale: action.locale);
      store.dispatch(SaveAppLanguage(locale: action.locale));
    } catch (e) {
      debugPrint('---- change app language catch block: ${e.toString()}');
    }
    next(action);
  }

//********************************* get-user **********************************//
  void getUserDetails(Store<AppState> store, GetUserDetails action,
      NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      final String? token = await repository.getUserAccessToken();
      final Map<String, String> headersToApi = await Utils.getHeader(token!);
      final AppUser? response = await authService.getUserDetails(headersToApi);

      if (response != null) {
        store.dispatch(SaveUser(userDetails: response));
        store.dispatch(
            SaveUserCountryCode(userCountryCode: response.countryCodeId));

        if (action.callbackFunc != null) {
          action.callbackFunc!(response);
        }
        repository.setUserPrefs(appUser: response);
      }
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      // store.dispatch(ForceLogOutUser(error: e));
      ToastHelper().getErrorFlushBar(
          e.errorMessage.toString(), store.state.navigator!.currentContext!);
      return;
    } catch (e) {
      if (e == true) {
        store.dispatch(ForceLogOutUser(
            status: Status.getRefreshToken.toString(), callbackFunc: (value) {
          store.dispatch(GetUserDetails(callbackFunc: action.callbackFunc));
        }));
      }
      else {
        ToastHelper().getErrorFlushBar(
            e.toString(), store.state.navigator!.currentContext!);
      }
      store.dispatch(SetLoader(false));
      debugPrint('---- get user details catch block: ${e.toString()}');
    }
    next(action);
  }

//********************************* request-mobile-otp ************************//
  void requestMobileOtp(Store<AppState> store, RequestMobileOtp action,
      NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      final String? response =
      await authService.requestMobileOtp(objToApi: action.bodyToApi);
      String? mobileNum;
      switch (action.country) {
        case '+91':
          mobileNum = action.mobileNo!.substring(0, 2) +
              "*** " +
              action.mobileNo!.substring(5, 10);
          break;
        case '+20':
          mobileNum = action.mobileNo!.substring(0, 2) +
              "*** " +
              (action.mobileNo?.length == 10
                  ? action.mobileNo!.substring(6, 10)
                  : action.mobileNo!.substring(7, 11));
          break;
        case "+972":
          mobileNum = action.mobileNo!.substring(0, 3) +
              "*** " +
              (action.mobileNo?.length == 9
                  ? action.mobileNo!.substring(5, 9)
                  : action.mobileNo!.substring(6, 10));
          break;

        case "+1":
          mobileNum = action.mobileNo!.substring(0, 3) +
              "*** " +
              action.mobileNo!.substring(6, 10);
          break;
        default:
          mobileNum = action.mobileNo!.substring(0, 2) +
              "***" + (action.mobileNo?.length == 10
              ? action.mobileNo!.substring(6, 10)
              : action.mobileNo!.substring(7, 11));
          break;
      }
      store.dispatch(SetLoader(false));
      store.state.navigator!.currentState!.push(DartHelper.pushMethod(
          OtpVerification(
              actualMobileNo: action.mobileNo,
              mobileNo: mobileNum)));
      ToastHelper().getIntermittentToast(
          response!, store.state.navigator!.currentContext!,
          fromMiddleware: true);
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      // store.dispatch(ForceLogOutUser(error: e));
      ToastHelper().getErrorFlushBar(
          e.errorMessage!, store.state.navigator!.currentContext!);
      return;
    } catch (e) {
      store.dispatch(SetLoader(false));
      store.dispatch(ForceLogOutUser(status: Status.forceLogout.toString()));
      debugPrint(
          '============ request-mobile-otp catch block ==========\n${e
              .toString()}');
    }
    next(action);
  }

//********************************* verify-mobile-otp *************************//
  void verifyMobileOtp(Store<AppState> store, VerifyMobileOtp action,
      NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      final Map<String, dynamic> response =
      await authService.verifyMobileOtp(objToApi: action.bodyToApi);
      final AppUser? user = response['customer'];
      final AccessToken? token = response['token'];
      repository.setUserPrefs(appUser: user);
      repository.setUserAccessToken(accessToken: token!.accessToken);
      repository.setUserRefreshToken(refreshToken: token.refreshToken);
      if (user!.status == CustomerStatus.approved.name) {
        store.dispatch(SaveUser(userDetails: user));
        store.dispatch(SaveNeedRestart(needRestart: true));
        store.dispatch(CheckForUserInPrefs(needRestart: true));

        // setUpFireBase();
        // registrationToken = (await getFCMToken())!;
        final String? token = await repository.getUserAccessToken();
        final Map<String, String> headersToApi = await Utils.getHeader(token!);
        final ApiSuccess? _response = await authService.sendDeviceID(
          deviceId: registrationToken,
          headersToApi: headersToApi,
        );

        store.state.navigator!.currentState!.pushAndRemoveUntil(
            DartHelper.pushMethod(const HomePage()),
                (Route<dynamic> route) => false);
      }
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      // store.dispatch(ForceLogOutUser(error: e));
      ToastHelper().getErrorFlushBar(
          e.errorMessage!, store.state.navigator!.currentContext!);
      return;
    } catch (e) {
      if (e == true) {
        store.dispatch(ForceLogOutUser(
            status: Status.getRefreshToken.toString(), callbackFunc: (value) {
          store.dispatch(VerifyMobileOtp(bodyToApi: action.bodyToApi));
        }));
      }
      else {
        ToastHelper().getErrorFlushBar(
            e.toString(), store.state.navigator!.currentContext!);
      }
      store.dispatch(SetLoader(false));
      debugPrint(
          '============ verify-mobile-otp catch block ========== ${e
              .toString()}');
    }
    next(action);
  }

//********************************* log-in-with-refresh-token *************************************//
  void loginWithRefreshToken(Store<AppState> store,
      LoginWithRefreshToken action, NextDispatcher next) async {
    try {
      final String? refreshToken = await repository.getUserRefreshToken();
      debugPrint(
          '////////refreshToken from loginWithRefreshToken: $refreshToken');
      store.dispatch(SetLoader(true));
      final Map<String, dynamic>? objToApi = {
        "customer": {
          "grant_type": "refresh_token",
          "refresh_token": refreshToken
        }
      };
      final Map<String, dynamic> response =
      await authService.verifyMobileOtp(objToApi: objToApi);
      final AppUser? user = response['customer'];
      final AccessToken? token = response['token'];
      repository.setUserAccessToken(accessToken: token!.accessToken);
      repository.setUserRefreshToken(refreshToken: token.refreshToken);
      store.dispatch(SetLoader(false));

      if (action.callbackFunc != null) {
        action.callbackFunc!('');
      }
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      ToastHelper().getErrorFlushBar(
          e.errorMessage!, store.state.navigator!.currentContext!);
      store.dispatch(ForceLogOutUser(status: Status.forceLogout.toString()));
      debugPrint(
          '============ login with refresh token error block ========== ${e
              .toString()}');
      return;
    } catch (e) {
      store.dispatch(SetLoader(false));
      store.dispatch(ForceLogOutUser(status: Status.forceLogout.toString()));
      debugPrint(
          '============ login with refresh token catch block ========== ${e
              .toString()}');
    }
    next(action);
  }

  //********************************* category-meta *****************************//
  void getCountryCodeMeta(Store<AppState> store, GetCountryCodeMeta action,
      NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      final BuiltList<CountryCode>? countryCodeMeta =
      await authService.getCountryCodeMeta();

      store.dispatch(SaveCountryCodeMeta(countryCodeMeta: countryCodeMeta));
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      ToastHelper().getErrorFlushBar(
          e.errorMessage!, store.state.navigator!.currentContext!);
      debugPrint(
          '============ get country code meta error block ========== ${e
              .toString()}');
      return;
    } catch (e) {
      store.dispatch(SetLoader(false));
      ToastHelper().getErrorFlushBar(
          e.toString(), store.state.navigator!.currentContext!);
      debugPrint(
          '============ get country code meta catch block ========== ${e
              .toString()}');
      store.dispatch(ForceLogOutUser(status: Status.forceLogout.toString()));
    }
    next(action);
  }

//************************************ logout user ****************************//
  void logOutUser(Store<AppState> store, LogOutUser action,
      NextDispatcher next) async {
    try {
      store.dispatch(SetLoader(true));
      final String? token = await repository.getUserAccessToken();
      final Map<String, String> headersToApi = await Utils.getHeader(token!);
      await authService.logOut(headersToApi: headersToApi);
      repository.setUserPrefs(appUser: null);
      repository.setUserAccessToken(accessToken: '');
      repository.setUserRefreshToken(refreshToken: '');
      store.dispatch(SetLoader(false));
    } on ApiError catch (e) {
      store.dispatch(SetLoader(false));
      store.dispatch(ForceLogOutUser(status: Status.forceLogout.toString()));
    } catch (e) {
      store.dispatch(SetLoader(false));
      store.dispatch(ForceLogOutUser(status: Status.forceLogout.toString()));
    }
    next(action);
  }

//************************************ force logout user *************************//
  void forceLogOutUser(Store<AppState> store, ForceLogOutUser action,
      NextDispatcher next) async {
    try {
      if (action.status == Status.getRefreshToken.toString()) {
        store.dispatch(
            LoginWithRefreshToken(callbackFunc: action.callbackFunc));
      } else {
        repository.setUserPrefs(appUser: null);
        repository.setUserAccessToken(accessToken: '');
        repository.setUserRefreshToken(refreshToken: '');
        store.state.navigator?.currentState?.pushAndRemoveUntil(
            DartHelper.pushMethod(const LoginPage()), (route) => false);
      }
    } catch (e) {
      debugPrint('force-logout catch block ${e.toString()}');
    }
    next(action);
  }

  //************************** notification-listener *************************//
  void setUpFireBaseListener(Store<AppState> store,
      SetUpFireBaseListener action, NextDispatcher next) async {
    next(action);

    try {
      ///handle notifications received when app is in killed state
      RemoteMessage? initialMessage = await FirebaseMessaging.instance
          .getInitialMessage();

      if (initialMessage != null) {
        ///first go to the redirection screen, and then, call the APIs in the checkForUserInPrefs
        fcmRouting(initialMessage, store);
        store.dispatch(SaveNeedRestart(needRestart: true));
        store.dispatch(CheckForUserInPrefs(needRestart: true));
        store.dispatch(SaveNeedRestart(needRestart: false));
      }

      ///handle foreground messages
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        print('front${message.data}');
        // store.dispatch(GetNotificationList(pageNo: 1));
        switch (message.data['notification_type']) {
          case 'installment_remainder':
            // store.dispatch(GetPaymentDue());
            break;

          case 'document_status':
            store.dispatch(GetUserDetails());
            break;
        }
      });

      ///handle background messages
      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        debugPrint(
            '-----------------background message received-------------------');
        // store.dispatch(GetNotificationList(pageNo: 1));
        fcmRouting(message, store);
      });
    } catch (err) {
      debugPrint(err.toString());
      return;
    }
  }
}

