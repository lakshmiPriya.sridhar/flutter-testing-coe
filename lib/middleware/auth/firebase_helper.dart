import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/actions/actions.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:redux/redux.dart';

final bool isTest = Platform.environment.containsKey('FLUTTER_TEST');

FirebaseMessaging messaging = FirebaseMessaging.instance;

void setUpFireBase() => isTest == false ? _setUpFireBase() : {};

void _setUpFireBase() async {
  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );
}

getFCMToken() => isTest == false ? _getFCMToken() : "";

Future<String?> _getFCMToken() async {
  String? registrationToken = '';
  try {
    await messaging.getToken().then((String? token) {
      debugPrint('--------->>>\n$token\n<<<<<---------');
      registrationToken = token;
    });
    return registrationToken;
  } catch (e) {
    debugPrint(
        '======================= error in getting the token ===============');
  }
}

void fcmRouting(RemoteMessage message, Store<AppState> store) {

  // store.dispatch(UpdateNotificationReadStatus(notificationID : int.parse(message.data['notification_id'])));

  Future<void>.delayed(const Duration(microseconds: 10), () {
    print('back${message.data}');
    switch (message.data['notification_type']) {
      case 'installment_remainder':
        // store.state.navigator?.currentState?.push(DartHelper.pushMethod(const BuyTabPage()));
        break;

      case 'document_status':
        store.dispatch(GetUserDetails());
        // store.state.navigator?.currentState?.push(DartHelper.pushMethod(const ProfileDocuments()));
        break;

      case 'return_status':
        // store.dispatch(GetPurchasesByID(int.parse(message.data['customer_purchase']['id'])));
        break;

      case 'purchase_creation_status':
        if(message.data['purchase_status'] == 'success'){
          // store.dispatch(GetPurchasesList(1));
          // store.dispatch(GetCustomerTransactions(1));
          // store.dispatch(GetPaymentDue());
          // store.dispatch(GetPurchasesByID(int.parse(message.data['customer_purchase_id'])));
          // store.state.navigator?.currentState?.push(DartHelper.pushMethod(const PurchaseDetails()));
        }else{
          // store.state.navigator?.currentState?.pushAndRemoveUntil(
          //     MaterialPageRoute(
          //         builder: (context) =>
          //         const HomePage(index: 1)),
          //         (route) => false);
          // purchaseFailedBottomSheet(store.state.navigator?.currentContext, message.data['amount'].toString());
        }
        break;
    }

  });
  return;
}
