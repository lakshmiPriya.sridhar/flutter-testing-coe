// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth_connector.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AuthViewModel extends AuthViewModel {
  @override
  final bool? restart;
  @override
  final NeedRestartAction needRestart;
  @override
  final bool? isInitializing;
  @override
  final bool? isLoading;
  @override
  final AppUser currentUser;
  @override
  final String? paginationMessage;
  @override
  final String? storesSearchPaginationMessage;
  @override
  final String? storesLovePaginationMessage;
  @override
  final String? storesTrendingPaginationMessage;
  @override
  final String? inStoresPaginationMessage;
  @override
  final String? inStoresAttachmentsPaginationMessage;
  @override
  final String? purchasesPaginationMessage;
  @override
  final String? transactionPaginationMessage;
  @override
  final GetUserDetailsAction getUserDetails;
  @override
  final RequestMobileOtpAction requestMobileOtpAction;
  @override
  final VerifyMobileOtpAction verifyMobileOtpAction;
  @override
  final LogOutAction logOut;
  @override
  final ForceLogOutUserAction forceLogOutUser;
  @override
  final CheckForUserInPrefsAction checkForUserPrefs;
  @override
  final BuiltList<CountryCode>? countryCodeMeta;
  @override
  final SaveUserCountryCodeAction saveUserCountryCode;
  @override
  final int? userCountryCode;
  @override
  final ChangeLanguageAction changeLanguage;
  @override
  final bool? purchaseEnabled;

  factory _$AuthViewModel([void Function(AuthViewModelBuilder)? updates]) =>
      (new AuthViewModelBuilder()..update(updates))._build();

  _$AuthViewModel._(
      {this.restart,
      required this.needRestart,
      this.isInitializing,
      this.isLoading,
      required this.currentUser,
      this.paginationMessage,
      this.storesSearchPaginationMessage,
      this.storesLovePaginationMessage,
      this.storesTrendingPaginationMessage,
      this.inStoresPaginationMessage,
      this.inStoresAttachmentsPaginationMessage,
      this.purchasesPaginationMessage,
      this.transactionPaginationMessage,
      required this.getUserDetails,
      required this.requestMobileOtpAction,
      required this.verifyMobileOtpAction,
      required this.logOut,
      required this.forceLogOutUser,
      required this.checkForUserPrefs,
      this.countryCodeMeta,
      required this.saveUserCountryCode,
      this.userCountryCode,
      required this.changeLanguage,
      this.purchaseEnabled})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        needRestart, r'AuthViewModel', 'needRestart');
    BuiltValueNullFieldError.checkNotNull(
        currentUser, r'AuthViewModel', 'currentUser');
    BuiltValueNullFieldError.checkNotNull(
        getUserDetails, r'AuthViewModel', 'getUserDetails');
    BuiltValueNullFieldError.checkNotNull(
        requestMobileOtpAction, r'AuthViewModel', 'requestMobileOtpAction');
    BuiltValueNullFieldError.checkNotNull(
        verifyMobileOtpAction, r'AuthViewModel', 'verifyMobileOtpAction');
    BuiltValueNullFieldError.checkNotNull(logOut, r'AuthViewModel', 'logOut');
    BuiltValueNullFieldError.checkNotNull(
        forceLogOutUser, r'AuthViewModel', 'forceLogOutUser');
    BuiltValueNullFieldError.checkNotNull(
        checkForUserPrefs, r'AuthViewModel', 'checkForUserPrefs');
    BuiltValueNullFieldError.checkNotNull(
        saveUserCountryCode, r'AuthViewModel', 'saveUserCountryCode');
    BuiltValueNullFieldError.checkNotNull(
        changeLanguage, r'AuthViewModel', 'changeLanguage');
  }

  @override
  AuthViewModel rebuild(void Function(AuthViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthViewModelBuilder toBuilder() => new AuthViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    final dynamic _$dynamicOther = other;
    return other is AuthViewModel &&
        restart == other.restart &&
        needRestart == _$dynamicOther.needRestart &&
        isInitializing == other.isInitializing &&
        isLoading == other.isLoading &&
        currentUser == other.currentUser &&
        paginationMessage == other.paginationMessage &&
        storesSearchPaginationMessage == other.storesSearchPaginationMessage &&
        storesLovePaginationMessage == other.storesLovePaginationMessage &&
        storesTrendingPaginationMessage ==
            other.storesTrendingPaginationMessage &&
        inStoresPaginationMessage == other.inStoresPaginationMessage &&
        inStoresAttachmentsPaginationMessage ==
            other.inStoresAttachmentsPaginationMessage &&
        purchasesPaginationMessage == other.purchasesPaginationMessage &&
        transactionPaginationMessage == other.transactionPaginationMessage &&
        getUserDetails == _$dynamicOther.getUserDetails &&
        requestMobileOtpAction == _$dynamicOther.requestMobileOtpAction &&
        verifyMobileOtpAction == _$dynamicOther.verifyMobileOtpAction &&
        logOut == _$dynamicOther.logOut &&
        forceLogOutUser == _$dynamicOther.forceLogOutUser &&
        checkForUserPrefs == _$dynamicOther.checkForUserPrefs &&
        countryCodeMeta == other.countryCodeMeta &&
        saveUserCountryCode == _$dynamicOther.saveUserCountryCode &&
        userCountryCode == other.userCountryCode &&
        changeLanguage == _$dynamicOther.changeLanguage &&
        purchaseEnabled == other.purchaseEnabled;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, restart.hashCode);
    _$hash = $jc(_$hash, needRestart.hashCode);
    _$hash = $jc(_$hash, isInitializing.hashCode);
    _$hash = $jc(_$hash, isLoading.hashCode);
    _$hash = $jc(_$hash, currentUser.hashCode);
    _$hash = $jc(_$hash, paginationMessage.hashCode);
    _$hash = $jc(_$hash, storesSearchPaginationMessage.hashCode);
    _$hash = $jc(_$hash, storesLovePaginationMessage.hashCode);
    _$hash = $jc(_$hash, storesTrendingPaginationMessage.hashCode);
    _$hash = $jc(_$hash, inStoresPaginationMessage.hashCode);
    _$hash = $jc(_$hash, inStoresAttachmentsPaginationMessage.hashCode);
    _$hash = $jc(_$hash, purchasesPaginationMessage.hashCode);
    _$hash = $jc(_$hash, transactionPaginationMessage.hashCode);
    _$hash = $jc(_$hash, getUserDetails.hashCode);
    _$hash = $jc(_$hash, requestMobileOtpAction.hashCode);
    _$hash = $jc(_$hash, verifyMobileOtpAction.hashCode);
    _$hash = $jc(_$hash, logOut.hashCode);
    _$hash = $jc(_$hash, forceLogOutUser.hashCode);
    _$hash = $jc(_$hash, checkForUserPrefs.hashCode);
    _$hash = $jc(_$hash, countryCodeMeta.hashCode);
    _$hash = $jc(_$hash, saveUserCountryCode.hashCode);
    _$hash = $jc(_$hash, userCountryCode.hashCode);
    _$hash = $jc(_$hash, changeLanguage.hashCode);
    _$hash = $jc(_$hash, purchaseEnabled.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'AuthViewModel')
          ..add('restart', restart)
          ..add('needRestart', needRestart)
          ..add('isInitializing', isInitializing)
          ..add('isLoading', isLoading)
          ..add('currentUser', currentUser)
          ..add('paginationMessage', paginationMessage)
          ..add('storesSearchPaginationMessage', storesSearchPaginationMessage)
          ..add('storesLovePaginationMessage', storesLovePaginationMessage)
          ..add('storesTrendingPaginationMessage',
              storesTrendingPaginationMessage)
          ..add('inStoresPaginationMessage', inStoresPaginationMessage)
          ..add('inStoresAttachmentsPaginationMessage',
              inStoresAttachmentsPaginationMessage)
          ..add('purchasesPaginationMessage', purchasesPaginationMessage)
          ..add('transactionPaginationMessage', transactionPaginationMessage)
          ..add('getUserDetails', getUserDetails)
          ..add('requestMobileOtpAction', requestMobileOtpAction)
          ..add('verifyMobileOtpAction', verifyMobileOtpAction)
          ..add('logOut', logOut)
          ..add('forceLogOutUser', forceLogOutUser)
          ..add('checkForUserPrefs', checkForUserPrefs)
          ..add('countryCodeMeta', countryCodeMeta)
          ..add('saveUserCountryCode', saveUserCountryCode)
          ..add('userCountryCode', userCountryCode)
          ..add('changeLanguage', changeLanguage)
          ..add('purchaseEnabled', purchaseEnabled))
        .toString();
  }
}

class AuthViewModelBuilder
    implements Builder<AuthViewModel, AuthViewModelBuilder> {
  _$AuthViewModel? _$v;

  bool? _restart;
  bool? get restart => _$this._restart;
  set restart(bool? restart) => _$this._restart = restart;

  NeedRestartAction? _needRestart;
  NeedRestartAction? get needRestart => _$this._needRestart;
  set needRestart(NeedRestartAction? needRestart) =>
      _$this._needRestart = needRestart;

  bool? _isInitializing;
  bool? get isInitializing => _$this._isInitializing;
  set isInitializing(bool? isInitializing) =>
      _$this._isInitializing = isInitializing;

  bool? _isLoading;
  bool? get isLoading => _$this._isLoading;
  set isLoading(bool? isLoading) => _$this._isLoading = isLoading;

  AppUserBuilder? _currentUser;
  AppUserBuilder get currentUser =>
      _$this._currentUser ??= new AppUserBuilder();
  set currentUser(AppUserBuilder? currentUser) =>
      _$this._currentUser = currentUser;

  String? _paginationMessage;
  String? get paginationMessage => _$this._paginationMessage;
  set paginationMessage(String? paginationMessage) =>
      _$this._paginationMessage = paginationMessage;

  String? _storesSearchPaginationMessage;
  String? get storesSearchPaginationMessage =>
      _$this._storesSearchPaginationMessage;
  set storesSearchPaginationMessage(String? storesSearchPaginationMessage) =>
      _$this._storesSearchPaginationMessage = storesSearchPaginationMessage;

  String? _storesLovePaginationMessage;
  String? get storesLovePaginationMessage =>
      _$this._storesLovePaginationMessage;
  set storesLovePaginationMessage(String? storesLovePaginationMessage) =>
      _$this._storesLovePaginationMessage = storesLovePaginationMessage;

  String? _storesTrendingPaginationMessage;
  String? get storesTrendingPaginationMessage =>
      _$this._storesTrendingPaginationMessage;
  set storesTrendingPaginationMessage(
          String? storesTrendingPaginationMessage) =>
      _$this._storesTrendingPaginationMessage = storesTrendingPaginationMessage;

  String? _inStoresPaginationMessage;
  String? get inStoresPaginationMessage => _$this._inStoresPaginationMessage;
  set inStoresPaginationMessage(String? inStoresPaginationMessage) =>
      _$this._inStoresPaginationMessage = inStoresPaginationMessage;

  String? _inStoresAttachmentsPaginationMessage;
  String? get inStoresAttachmentsPaginationMessage =>
      _$this._inStoresAttachmentsPaginationMessage;
  set inStoresAttachmentsPaginationMessage(
          String? inStoresAttachmentsPaginationMessage) =>
      _$this._inStoresAttachmentsPaginationMessage =
          inStoresAttachmentsPaginationMessage;

  String? _purchasesPaginationMessage;
  String? get purchasesPaginationMessage => _$this._purchasesPaginationMessage;
  set purchasesPaginationMessage(String? purchasesPaginationMessage) =>
      _$this._purchasesPaginationMessage = purchasesPaginationMessage;

  String? _transactionPaginationMessage;
  String? get transactionPaginationMessage =>
      _$this._transactionPaginationMessage;
  set transactionPaginationMessage(String? transactionPaginationMessage) =>
      _$this._transactionPaginationMessage = transactionPaginationMessage;

  GetUserDetailsAction? _getUserDetails;
  GetUserDetailsAction? get getUserDetails => _$this._getUserDetails;
  set getUserDetails(GetUserDetailsAction? getUserDetails) =>
      _$this._getUserDetails = getUserDetails;

  RequestMobileOtpAction? _requestMobileOtpAction;
  RequestMobileOtpAction? get requestMobileOtpAction =>
      _$this._requestMobileOtpAction;
  set requestMobileOtpAction(RequestMobileOtpAction? requestMobileOtpAction) =>
      _$this._requestMobileOtpAction = requestMobileOtpAction;

  VerifyMobileOtpAction? _verifyMobileOtpAction;
  VerifyMobileOtpAction? get verifyMobileOtpAction =>
      _$this._verifyMobileOtpAction;
  set verifyMobileOtpAction(VerifyMobileOtpAction? verifyMobileOtpAction) =>
      _$this._verifyMobileOtpAction = verifyMobileOtpAction;

  LogOutAction? _logOut;
  LogOutAction? get logOut => _$this._logOut;
  set logOut(LogOutAction? logOut) => _$this._logOut = logOut;

  ForceLogOutUserAction? _forceLogOutUser;
  ForceLogOutUserAction? get forceLogOutUser => _$this._forceLogOutUser;
  set forceLogOutUser(ForceLogOutUserAction? forceLogOutUser) =>
      _$this._forceLogOutUser = forceLogOutUser;

  CheckForUserInPrefsAction? _checkForUserPrefs;
  CheckForUserInPrefsAction? get checkForUserPrefs => _$this._checkForUserPrefs;
  set checkForUserPrefs(CheckForUserInPrefsAction? checkForUserPrefs) =>
      _$this._checkForUserPrefs = checkForUserPrefs;

  ListBuilder<CountryCode>? _countryCodeMeta;
  ListBuilder<CountryCode> get countryCodeMeta =>
      _$this._countryCodeMeta ??= new ListBuilder<CountryCode>();
  set countryCodeMeta(ListBuilder<CountryCode>? countryCodeMeta) =>
      _$this._countryCodeMeta = countryCodeMeta;

  SaveUserCountryCodeAction? _saveUserCountryCode;
  SaveUserCountryCodeAction? get saveUserCountryCode =>
      _$this._saveUserCountryCode;
  set saveUserCountryCode(SaveUserCountryCodeAction? saveUserCountryCode) =>
      _$this._saveUserCountryCode = saveUserCountryCode;

  int? _userCountryCode;
  int? get userCountryCode => _$this._userCountryCode;
  set userCountryCode(int? userCountryCode) =>
      _$this._userCountryCode = userCountryCode;

  ChangeLanguageAction? _changeLanguage;
  ChangeLanguageAction? get changeLanguage => _$this._changeLanguage;
  set changeLanguage(ChangeLanguageAction? changeLanguage) =>
      _$this._changeLanguage = changeLanguage;

  bool? _purchaseEnabled;
  bool? get purchaseEnabled => _$this._purchaseEnabled;
  set purchaseEnabled(bool? purchaseEnabled) =>
      _$this._purchaseEnabled = purchaseEnabled;

  AuthViewModelBuilder();

  AuthViewModelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _restart = $v.restart;
      _needRestart = $v.needRestart;
      _isInitializing = $v.isInitializing;
      _isLoading = $v.isLoading;
      _currentUser = $v.currentUser.toBuilder();
      _paginationMessage = $v.paginationMessage;
      _storesSearchPaginationMessage = $v.storesSearchPaginationMessage;
      _storesLovePaginationMessage = $v.storesLovePaginationMessage;
      _storesTrendingPaginationMessage = $v.storesTrendingPaginationMessage;
      _inStoresPaginationMessage = $v.inStoresPaginationMessage;
      _inStoresAttachmentsPaginationMessage =
          $v.inStoresAttachmentsPaginationMessage;
      _purchasesPaginationMessage = $v.purchasesPaginationMessage;
      _transactionPaginationMessage = $v.transactionPaginationMessage;
      _getUserDetails = $v.getUserDetails;
      _requestMobileOtpAction = $v.requestMobileOtpAction;
      _verifyMobileOtpAction = $v.verifyMobileOtpAction;
      _logOut = $v.logOut;
      _forceLogOutUser = $v.forceLogOutUser;
      _checkForUserPrefs = $v.checkForUserPrefs;
      _countryCodeMeta = $v.countryCodeMeta?.toBuilder();
      _saveUserCountryCode = $v.saveUserCountryCode;
      _userCountryCode = $v.userCountryCode;
      _changeLanguage = $v.changeLanguage;
      _purchaseEnabled = $v.purchaseEnabled;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AuthViewModel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AuthViewModel;
  }

  @override
  void update(void Function(AuthViewModelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  AuthViewModel build() => _build();

  _$AuthViewModel _build() {
    _$AuthViewModel _$result;
    try {
      _$result = _$v ??
          new _$AuthViewModel._(
              restart: restart,
              needRestart: BuiltValueNullFieldError.checkNotNull(
                  needRestart, r'AuthViewModel', 'needRestart'),
              isInitializing: isInitializing,
              isLoading: isLoading,
              currentUser: currentUser.build(),
              paginationMessage: paginationMessage,
              storesSearchPaginationMessage: storesSearchPaginationMessage,
              storesLovePaginationMessage: storesLovePaginationMessage,
              storesTrendingPaginationMessage: storesTrendingPaginationMessage,
              inStoresPaginationMessage: inStoresPaginationMessage,
              inStoresAttachmentsPaginationMessage:
                  inStoresAttachmentsPaginationMessage,
              purchasesPaginationMessage: purchasesPaginationMessage,
              transactionPaginationMessage: transactionPaginationMessage,
              getUserDetails: BuiltValueNullFieldError.checkNotNull(
                  getUserDetails, r'AuthViewModel', 'getUserDetails'),
              requestMobileOtpAction: BuiltValueNullFieldError.checkNotNull(
                  requestMobileOtpAction, r'AuthViewModel', 'requestMobileOtpAction'),
              verifyMobileOtpAction: BuiltValueNullFieldError.checkNotNull(
                  verifyMobileOtpAction, r'AuthViewModel', 'verifyMobileOtpAction'),
              logOut: BuiltValueNullFieldError.checkNotNull(
                  logOut, r'AuthViewModel', 'logOut'),
              forceLogOutUser: BuiltValueNullFieldError.checkNotNull(
                  forceLogOutUser, r'AuthViewModel', 'forceLogOutUser'),
              checkForUserPrefs: BuiltValueNullFieldError.checkNotNull(
                  checkForUserPrefs, r'AuthViewModel', 'checkForUserPrefs'),
              countryCodeMeta: _countryCodeMeta?.build(),
              saveUserCountryCode:
                  BuiltValueNullFieldError.checkNotNull(saveUserCountryCode, r'AuthViewModel', 'saveUserCountryCode'),
              userCountryCode: userCountryCode,
              changeLanguage: BuiltValueNullFieldError.checkNotNull(changeLanguage, r'AuthViewModel', 'changeLanguage'),
              purchaseEnabled: purchaseEnabled);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'currentUser';
        currentUser.build();

        _$failedField = 'countryCodeMeta';
        _countryCodeMeta?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'AuthViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
