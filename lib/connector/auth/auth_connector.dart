import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:flutter_test_coe/actions/actions.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:built_value/built_value.dart';
import 'package:flutter/material.dart' hide Builder;
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

part 'auth_connector.g.dart';

//********************************* get-user **********************************//
typedef GetUserDetailsAction = void Function();
//********************************* request-mobile-otp ************************//
typedef RequestMobileOtpAction = void Function(
    String? mobileNo, Map<String, dynamic>? bodyToApi, String? country);
//********************************* verify-mobile-otp *************************//
typedef VerifyMobileOtpAction = void Function(Map<String, dynamic>? bodyToApi);
//******************************** log-out ************************************//
typedef LogOutAction = void Function();
//******************************** force-log-out ******************************//
typedef ForceLogOutUserAction = void Function(String? status);

typedef CheckForUserInPrefsAction = void Function(bool? needRestart);

typedef NeedRestartAction = void Function(bool? needRestart);

typedef SaveUserCountryCodeAction = void Function(int? countryCodeId);

typedef ChangeLanguageAction = void Function(String? locale);

abstract class AuthViewModel
    implements Built<AuthViewModel, AuthViewModelBuilder> {
  factory AuthViewModel(
      [AuthViewModelBuilder Function(AuthViewModelBuilder builder)
          updates]) = _$AuthViewModel;

  AuthViewModel._();

  factory AuthViewModel.fromStore(Store<AppState> store) {
    return AuthViewModel((AuthViewModelBuilder b) {
      return b
        ..restart = store.state.needRestart
        ..needRestart = (bool? needRestart) {
          store.dispatch(SaveNeedRestart(needRestart: needRestart));
        }
        ..isInitializing = store.state.isInitializing
        ..isLoading = store.state.isLoading
        ..currentUser = store.state.currentUser?.toBuilder()
        ..paginationMessage = store.state.paginationMessage
        ..storesSearchPaginationMessage =
            store.state.storesSearchPaginationMessage
        ..storesLovePaginationMessage = store.state.storesLovePaginationMessage
        ..storesTrendingPaginationMessage =
            store.state.storesTrendingPaginationMessage
        ..inStoresPaginationMessage = store.state.inStoresPaginationMessage
        ..inStoresAttachmentsPaginationMessage =
            store.state.inStoresAttachmentsPaginationMessage
        ..purchasesPaginationMessage = store.state.purchasesPaginationMessage
        ..transactionPaginationMessage =
            store.state.transactionPaginationMessage
        ..countryCodeMeta = store.state.countryCodeMeta?.toBuilder()
        ..userCountryCode = store.state.userCountryCode
//********************************* get-user **********************************//
        ..getUserDetails = () async {
          await store.dispatch(GetUserDetails());
        }
//********************************* request-mobile-otp ************************//
        ..requestMobileOtpAction =
            (String? mobileNo, Map<String, dynamic>? bodyToApi, String? country) {
          store.dispatch(
              RequestMobileOtp(mobileNo: mobileNo, bodyToApi: bodyToApi,
                  country: country));
        }
//********************************* verify-mobile-otp *************************//
        ..verifyMobileOtpAction = (Map<String, dynamic>? bodyToApi) {
          store.dispatch(VerifyMobileOtp(bodyToApi: bodyToApi));
        }
//***************************** log-out ***************************************//
        ..logOut = () {
          store.dispatch(LogOutUser());
        }
//*********************** force-log-out ***************************************//
        ..forceLogOutUser = (String? status) {
          store.dispatch(ForceLogOutUser(status: status));
        }
        ..checkForUserPrefs = (bool? needRestart) {
          store.dispatch(CheckForUserInPrefs(needRestart: needRestart));
        }
        ..saveUserCountryCode = (int? countryCodeId) {
          store.dispatch(SaveUserCountryCode(userCountryCode: countryCodeId));
        }
        ..changeLanguage = (String? locale) {
          store.dispatch(ChangeAppLanguage(locale: locale));
        }
        ;
    });
  }

  bool? get restart;

  NeedRestartAction get needRestart;

  bool? get isInitializing;

  bool? get isLoading;

  AppUser get currentUser;

  String? get paginationMessage;

  String? get storesSearchPaginationMessage;

  String? get storesLovePaginationMessage;

  String? get storesTrendingPaginationMessage;

  String? get inStoresPaginationMessage;

  String? get inStoresAttachmentsPaginationMessage;

  String? get purchasesPaginationMessage;

  String? get transactionPaginationMessage;

  GetUserDetailsAction get getUserDetails;

  RequestMobileOtpAction get requestMobileOtpAction;

  VerifyMobileOtpAction get verifyMobileOtpAction;

  LogOutAction get logOut;

  ForceLogOutUserAction get forceLogOutUser;

  CheckForUserInPrefsAction get checkForUserPrefs;

  BuiltList<CountryCode>? get countryCodeMeta;

  SaveUserCountryCodeAction get saveUserCountryCode;

  int? get userCountryCode;

  ChangeLanguageAction get changeLanguage;

  bool? get purchaseEnabled;
}

class AuthConnector extends StatelessWidget {
  const AuthConnector({Key? key, required this.builder}) : super(key: key);

  final ViewModelBuilder<AuthViewModel> builder;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AuthViewModel>(
      builder: builder,
      converter: (Store<AppState> store) => AuthViewModel.fromStore(store),
    );
  }
}
