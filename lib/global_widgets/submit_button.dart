import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter/material.dart';

class SubmitButtonTheme extends StatelessWidget {
  final String? buttonText;
  final Function()? onTap;
  final Function()? onDoubleTap;
  final Color? textColor;
  final Color? buttonColor;
  final Color? borderColor;
  final double? horipad;
  final double? vertiPad;
  const SubmitButtonTheme({Key? key, this.buttonText, required this.onTap, this.textColor, required this.buttonColor, this.borderColor, this.horipad, this.vertiPad, this.onDoubleTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
        decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(
        Radius.circular(16.0),
        )),
      padding: EdgeInsets.symmetric(horizontal: horipad ?? 20, vertical: vertiPad ?? 20),
      child: InkWell(
        onTap: onTap,
        onDoubleTap: onDoubleTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(16.0),
            ),
            color: buttonColor ?? AppColors.themePrimaryColor,
          ),
          padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
          alignment: Alignment.center,
          child: Text(buttonText ?? "", style: AppStyle.white16SemiBoldTextStyle.copyWith(color: textColor)),
        ),
      ),
    );
  }
}

