import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';


class CommonPinCodeFields extends StatelessWidget {
  final TextEditingController controller;
  const CommonPinCodeFields({Key? key, required this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PinCodeTextField(
      appContext: context,
      mainAxisAlignment:
      MainAxisAlignment.spaceBetween,
      controller: controller,
      enabled: true,
      length: 4,
      obscureText: false,
      cursorWidth: 1,
      cursorColor: AppColors.themePrimaryColor,
      textStyle: AppStyle.grey16RegularTextStyle
          .copyWith(
          color: AppColors.textBlackColor),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      textInputAction: TextInputAction.done,
      animationType: AnimationType.fade,
      pinTheme: PinTheme(
        selectedFillColor: AppColors.bgWhiteColor,
        activeColor: AppColors.greenColor,
        inactiveColor: AppColors.shadowColor,
        disabledColor: AppColors.shadowColor,
        selectedColor: AppColors.themePrimaryColor,
        shape: PinCodeFieldShape.box,
        borderRadius: BorderRadius.circular(16),
        fieldHeight: 64,
        fieldWidth: 64,
        activeFillColor: Colors.white,
      ),
      animationDuration: const Duration(milliseconds: 300),
      onChanged: (value) {},
      boxShadows: const [
        BoxShadow(
            color: AppColors.shadowColor,
            blurRadius: 60.0,
            offset: Offset(0, 1))
      ],
    );
  }
}
