import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CommonTextField extends StatelessWidget {
  final TextEditingController? controller;
  final bool? autoFocus;
  final String? hintText;
  final String? errorText;
  final bool? isEmail;
  final String? testKey;
  final int? maxLine;
  final bool? iban;
  final bool? cardValidity;
  final String? labelText;
  final Function? onChanged;
  final bool? alphaOnly;
  final int? maxLength;
  const CommonTextField(
      {Key? key,
      this.controller,
      this.autoFocus,
      this.hintText,
      this.errorText,
      this.maxLine = 1,
      this.isEmail,
      this.testKey,
      this.iban,
      this.cardValidity = false,
      this.labelText,
      this.onChanged,
      this.alphaOnly = false,
      this.maxLength})
      : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
        decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: AppColors.dropShadow.withOpacity(0.10),
                  blurRadius: 40.0,
                  offset: const Offset(0, 10))
            ],
            // border: Border.all(color: AppColors.shadowColor, width: 0.5),
            borderRadius:
            const BorderRadius.all(Radius.circular(16.0)),
            color: Colors.transparent),
        child: TextFormField(
          controller: controller,
          key: Key(testKey ?? ""),
          autofocus: autoFocus!,
          textAlign: TextAlign.start,
          maxLines: maxLine,
          decoration: iban == true ? InputDecoration(
            labelText: labelText ?? "",
            labelStyle: AppStyle.black16MediumTextStyle.copyWith(
                fontSize: 14,
                color: AppColors.textLightGreyColor.withOpacity(0.5)),
            filled: true,
            hintText: hintText,
            hintStyle: AppStyle.grey16MediumTextStyle
                .copyWith(color: AppColors.textLightGreyColor.withOpacity(0.5)),
            fillColor: Colors.white,
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 25, vertical: 18),
            isDense: true,
            prefixIcon: Container(
              width: 55,
              padding: const EdgeInsets.fromLTRB(20,12,10,0),
              child:  Text(
                'LE',
                style: AppStyle
                    .black16MediumTextStyle
                    .copyWith(fontSize: 18),
              )
            )
          ) : InputDecoration(
              filled: true,
              hintText: hintText,
              labelText: labelText ?? "",
              labelStyle: AppStyle.black16MediumTextStyle.copyWith(
                      fontSize: 14,
                      color: AppColors.textLightGreyColor.withOpacity(0.5)),
              hintStyle: AppStyle.grey16MediumTextStyle
              .copyWith(color: AppColors.textLightGreyColor.withOpacity(0.5)),
              fillColor: Colors.white,
              contentPadding:
              const EdgeInsets.symmetric(horizontal: 25, vertical: 18),
              isDense: true,
          ),
          keyboardType: cardValidity == true ? TextInputType.number : TextInputType.text,
          inputFormatters: alphaOnly == true ?
          [FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z ]')),
            LengthLimitingTextInputFormatter(maxLength)]
              : [LengthLimitingTextInputFormatter(maxLength)],
          textInputAction: TextInputAction.done,
          style: AppStyle.black16RegularTextStyle.copyWith(fontSize: 18),
          cursorColor: AppColors.themePrimaryColor,
          cursorWidth: 1.0,
          onChanged: (String val) => onChanged != null ? onChanged!() : {},
          validator: (String? value) {
            if (isEmail == true) {
              if (value!.isEmpty) {
                return 'Error!  Email cannot be empty';
              } else if (!EmailValidator.validate(value.trim())) {
                return errorText;
              } else {
                return null;
              }
            } else {
              if (value!.isEmpty) {
                return errorText;
              } else {
                return null;
              }
            }
          },
        ));
  }
}
