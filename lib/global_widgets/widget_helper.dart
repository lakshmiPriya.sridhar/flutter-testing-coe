import 'dart:io';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

import 'dart_helper.dart';

PermissionStatus? permissions;

SizedBox getSpace(double height, double width) {
  return SizedBox(height: height, width: width);
}

SizedBox emptyBox() {
  return const SizedBox(height: 0.0, width: 0.0);
}

Widget forwardArrow() {
  return Container(
    padding: const EdgeInsets.all(5),
    alignment: Alignment.center,
    decoration: const BoxDecoration(
      color: AppColors.shadowColor,
      borderRadius: BorderRadius.all(
        Radius.circular(100.0),
      ),
    ),
    child: const Icon(
      Icons.arrow_forward_ios_sharp,
      color: AppColors.themePrimaryColor,
      size: 12.0,
    ),
  );
}

Widget backIcon(BuildContext context, {bool? givePadding = true}) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      getSpace(10, 0),
      InkWell(
        onTap: () => Navigator.pop(context),
        child: Container(
          padding: givePadding == true ? const EdgeInsets.all(15) : null,
          child: const Icon(
            Icons.arrow_back_ios_rounded,
            color: AppColors.dark1,
            size: 25.0,
          ),
        ),
      ),
    ],
  );
}

Widget globalLoader(bool isLoading) {
  return isLoading
      ? Positioned(
          top: 0.0,
          bottom: 0.0,
          left: 0.0,
          right: 0.0,
          child: Container(
            color: Colors.white.withOpacity(0.001),
          ))
      : Container();
}

void checkForCameraPermission(
    BuildContext context, String pickType, ValueSetter<File> imageFile) async {
  Future _openImagePicker(ImageSource source, BuildContext context) async {
    try {
      final _file = await ImagePicker().pickImage(source: source);
      if (_file != null) {
        imageFile(File(_file.path));
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  if (pickType == 'CAMERA') {
    if (await Permission.camera.isGranted) {
      _openImagePicker(ImageSource.camera, context);
    } else if (await Permission.camera.request().isGranted) {
      _openImagePicker(ImageSource.camera, context);
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context2) => Platform.isIOS
              ? CupertinoAlertDialog(
                  title: const Text('Deall Would Like to Access the Camera'),
                  content: const Text(
                      'This app needs camera access to take pictures for uploading user profile photo'),
                  actions: <Widget>[
                      CupertinoDialogAction(
                          child: const Text('Deny'),
                          onPressed: () => Navigator.of(context2).pop()),
                      CupertinoDialogAction(
                          child: const Text('Settings'),
                          onPressed: () async {
                            await openAppSettings();
                            Navigator.of(context2).pop();
                          })
                    ])
              : AlertDialog(
                  title: const Text('Camera Permission'),
                  content: const Text(
                      'This app needs camera access to take pictures for uploading user profile photo'),
                  actions: <Widget>[
                      TextButton(
                          child: const Text('Deny'),
                          onPressed: () => Navigator.of(context2).pop()),
                      TextButton(
                          child: const Text('Settings'),
                          onPressed: () async {
                            await openAppSettings();
                            Navigator.of(context2).pop();
                          })
                    ]));
    }
  } else if(pickType == 'GALLERY') {
    if(Platform.isAndroid) {
      if (await Permission.storage.isGranted) {
        _openImagePicker(ImageSource.gallery, context);
      } else if (await Permission.storage.request().isGranted) {
        _openImagePicker(ImageSource.gallery, context);
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context2) => AlertDialog(
                title: const Text('Gallery Permission'),
                content: const Text(
                    'This app needs gallery access to take pictures for upload user profile photo'),
                actions: <Widget>[
                  TextButton(
                      child: const Text('Deny'),
                      onPressed: () => Navigator.of(context2).pop()),
                  TextButton(
                      child: const Text('Settings'),
                      onPressed: () async {
                        await openAppSettings();
                        Navigator.of(context2).pop();
                      })
                ]));
      }
    }
    else if(Platform.isIOS){
      if (await Permission.photos.isGranted) {
        _openImagePicker(ImageSource.gallery, context);
      } else if (await Permission.photos.request().isGranted) {
        _openImagePicker(ImageSource.gallery, context);
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context2) => CupertinoAlertDialog(
                title: const Text('Deall Would Like to Access the Gallery'),
                content: const Text(
                    'This app needs gallery access to take pictures for uploading user profile photo'),
                actions: <Widget>[
                  CupertinoDialogAction(
                      child: const Text('Deny'),
                      onPressed: () => Navigator.of(context2).pop()),
                  CupertinoDialogAction(
                      child: const Text('Settings'),
                      onPressed: () async {
                        await openAppSettings();
                        Navigator.of(context2).pop();
                      })
                ]));
      }
    }
  }
}

Widget emptyLoadingWidget(double height, double width) {
  return Builder(
    builder: (context) {

      return SizedBox(
          height: height,
          width: width,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: const [
                Text('Fetching Data'),
                SizedBox(height: 10.0),
                CupertinoActivityIndicator(color: Colors.grey)
              ]));
    }
  );
}

void checkForStoragePermission(BuildContext context, bool isMultipleImage,
    ValueSetter<List<File>> imageFile) async {
  Future _openFilePicker(BuildContext context) async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
          allowMultiple: isMultipleImage,
          type: FileType.custom,
          allowedExtensions: [
            'jpg',
            'jpeg',
            'png',
            'pdf',
          ]);
      if (!DartHelper.isNullOrEmptyLists(result?.files)) {
        imageFile(result!.paths.map((path) => File(path!)).toList());
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  if (await Permission.storage.isGranted) {
    _openFilePicker(context);
  } else if (await Permission.storage.request().isGranted) {
    _openFilePicker(context);
  } else {
    showDialog(
        context: context,
        builder: (BuildContext context2) => Platform.isIOS
            ? CupertinoAlertDialog(
                title: const Text('Deall Would Like to Access the storage'),
                content: const Text(
                    'This app needs storage access to upload documents'),
                actions: <Widget>[
                    CupertinoDialogAction(
                        child: const Text('Deny'),
                        onPressed: () => Navigator.of(context2).pop()),
                    CupertinoDialogAction(
                        child: const Text('Settings'),
                        onPressed: () async {
                          await openAppSettings();
                          Navigator.of(context2).pop();
                        })
                  ])
            : AlertDialog(
                title: const Text('Storage Permission'),
                content: const Text(
                    'This app needs gallery access to take pictures for upload user profile photo'),
                actions: <Widget>[
                    TextButton(
                        child: const Text('Deny'),
                        onPressed: () => Navigator.of(context2).pop()),
                    TextButton(
                        child: const Text('Settings'),
                        onPressed: () async {
                          await openAppSettings();
                          Navigator.of(context2).pop();
                        })
                  ]));
  }
}

Widget docContainer(String status, String fileName, [bool? approvalStatus = true]) {

  String? fileStatus;
  Color? statusColor;
  switch(status) {
    case 'pending_verification':
      fileStatus = 'Pending Approval';
      statusColor = AppColors.yellowColor;
      break;
    case 'approved' :
      fileStatus = 'Approved';
      statusColor = AppColors.greenColor;
      break;
    case 'rejected':
      fileStatus = 'Rejected';
      statusColor = AppColors.errorTextColor;
      break;
  }

  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
    height: 118,
    decoration: BoxDecoration(
        color: const Color(0xFFFFFFFF),
        boxShadow: [
          BoxShadow(
              color: AppColors.dropShadow.withOpacity(0.10),
              blurRadius: 40,
              offset: const Offset(0, 10))
        ],
        borderRadius: const BorderRadius.all(Radius.circular(16)),
        border: Border.all(color: AppColors.shadowColor, width: 0.5)),
    child: Row(
      children: [
        ///doc image
        Container(
          height: 60,
          width: 60,
          decoration: const BoxDecoration(
              color: Color(0xFFF1F3F6),
              borderRadius: BorderRadius.all(Radius.circular(12))),
          child: Center(
              child: Image.asset(
                AppAssets.doc,
                height: 23,
              )),
        ),
        getSpace(0, 20),

        ///if approval status needs to be displayed
        approvalStatus == true ?
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  fileName,
                  style: AppStyle.black16SemiBoldTextStyle,
                ),
                Text(
                  fileStatus ?? 'Pending Approval',
                  style: AppStyle.black16SemiBoldTextStyle.copyWith(
                      fontSize: 14,
                      color: statusColor ?? AppColors.yellowColor),
                )
              ],
            ),
          ),
        ) : Expanded(
          child: Text(
            fileName,
            style: AppStyle.black16SemiBoldTextStyle,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    ),
  );
}

/// Creates the [KeyboardActionsConfig] to hook up the fields
/// and their focus nodes to our [FormKeyboardActions].
KeyboardActionsConfig keyboardActionsConfig(BuildContext context, FocusNode focusNode) {
  return KeyboardActionsConfig(
    keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
    keyboardBarColor: Colors.grey[200],
    nextFocus: true,
    actions: [
      KeyboardActionsItem(
        focusNode: focusNode,
        displayArrows: false,
        displayDoneButton: true,
      ),
    ],
  );
}

Widget infoGraphicTile({required String asset, required String text}){
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Image.asset(asset),
      ),
      getSpace(20,0),
      Text(text, style: AppStyle.grey16MediumTextStyle,)
    ],);}

Widget storeCoverPicPlaceholder(BuildContext context) {
  return Container(
    color: AppColors.shadowColor.withOpacity(0.6),
    padding: const EdgeInsets.all(20),
    child: SvgPicture.asset(
      AppAssets.coverPicHolder,
      width: MediaQuery.of(context).size.width,
      fit: BoxFit.contain
    ),
  );
}

Widget storeLogoPlaceholder(double padding) {
  return Padding(
    padding: EdgeInsets.all(padding),
    child: SvgPicture.asset(AppAssets.vendor,
        height: 10, width: 10, fit: BoxFit.contain));
}

Widget retakeButton({required Function() onTap, EdgeInsets? margin}){
  return Builder(
    builder: (context) {
      return InkWell(
          onTap: onTap,
          child: Container(
            margin: margin ?? const EdgeInsets.symmetric(vertical: 30, horizontal: 40),
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
                color: AppColors.bgWhiteColor,
                borderRadius: BorderRadius.circular(12),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: AppColors.dropShadow.withOpacity(0.10),
                    blurRadius: 40.0,
                    offset: const Offset(0, 10))
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  AppAssets.cameraSvg,
                  height: 20,
                ),
                getSpace(0, 8),
                Text('Retake', style: AppStyle.primary15SemiBoldTextStyle,),
              ],
            ),
          ));
    }
  );
}

Widget restrictionTile({required String text}){
  return Container(
    margin: const EdgeInsets.symmetric(horizontal: 20),
    decoration: BoxDecoration(
        color: AppColors.yellow1Color,
        borderRadius: BorderRadius.circular(16),
        border: Border.all(color: AppColors.yellow2Color)
    ),
    child: Stack(
      children: [
        SvgPicture.asset(AppAssets.restrictionBG),
        Padding(
          padding: const EdgeInsets.all(15),
          child: Row(
            children: [
              SvgPicture.asset(AppAssets.warning),
              getSpace(0,15),
              Flexible(child: Text(text, style: AppStyle.dark14SemiBoldTextStyle,)),
            ],
          ),
        ),
      ],
    ),
  );
}