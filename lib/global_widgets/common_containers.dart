import 'dart:io';

import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/global_widgets/dart_helper.dart';
import 'package:flutter_test_coe/global_widgets/submit_button.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter_svg/flutter_svg.dart';


Widget appLogoContainer(BuildContext context, {bool fromOnboarding = false, bool enableContactUs = false,
  Function? onTap, Widget? optionalWidget}) {
  return Container(
    padding: const EdgeInsets.only(top: 35, left: 20, right: 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        optionalWidget ?? Image.asset(
          AppAssets.appLogo,
          height: 40,
          width: 40,
        ),
        Row(
          children: [
            enableContactUs ?
            InkWell(
              // onTap:  () => showModalBottomSheet(
              //     backgroundColor: Colors.transparent,
              //     context: context,
              //     isScrollControlled: true,
              //     builder: (context) => contactUsBottomSheet(context, locale)),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Image.asset(
                  AppAssets.contactUs,
                  fit: BoxFit.contain,
                  height: 25,
                  width: 25,
                  color: const Color(0xFF7B839A),
                ),

                // SvgPicture.asset(
                //   AppAssets.contactUs,
                //   fit: BoxFit.contain,
                //   height: 25,
                //   width: 25,
                //   color: const Color(0xFF7B839A),
                // ),
              ),
            ):emptyBox(),
            InkWell(
              onTap: () {
                // showModalBottomSheet(
                //     backgroundColor: Colors.transparent,
                //     context: context,
                //     isScrollControlled: true,
                //     builder: (BuildContext context) {
                //       return ChangeLanguage(
                //           needRestart: false,
                //           fromOnboarding: fromOnboarding,
                //           context: context,
                //           onTap: onTap);
                //     });
              },
              child: Container(
                  alignment: Alignment.center,
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        AppAssets.appLang,
                        fit: BoxFit.contain,
                        height: 19.5,
                        width: 19.5,
                        color: const Color(0xFF7B839A),
                      ),
                      getSpace(0, 2),
                      Text('EN',
                          style: AppStyle.grey16MediumTextStyle),
                      getSpace(0, 2),
                      Image.asset(
                        AppAssets.dropdownArrow,
                        height: 8,
                        width: 8,
                      )
                    ],
                  )),
            ),
          ],
        )
      ],
    ),
  );
}

Widget mainAndSubTextContainer({String? mainText, String? subText, String? iCon, bool? needHelpIcon, Function()? helpOnTap}) {
  return Container(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              DartHelper.isNullOrEmpty(iCon) ? emptyBox():
              Row(
                children: [
                  SvgPicture.asset(iCon!,
                      fit: BoxFit.contain,
                      height: 28),
                  getSpace(0,10),
                ],
              ),
              mainText != null
                  ? Text(mainText,
                  style: AppStyle.theme16BoldTextStyle.copyWith(fontSize: 22)):emptyBox(),
              (needHelpIcon ?? false) ? Row(
                children: [
                  getSpace(0,5),
                  InkWell(
                    onTap: helpOnTap,
                    child: SvgPicture.asset(
                      AppAssets.help,
                      height: 22,
                      width: 22,
                      color: AppColors.secondaryTextColor,
                    ),
                  ),
                ],
              ):emptyBox(),
            ],
          ),
          const SizedBox(height: 10),
          subText != null
              ? Text(subText, style: AppStyle.grey16MediumTextStyle)
              : Container(),
        ],
      ));
}

Widget mainAndSubTextWithValueContainer({String? mainText, String? subText, String? value, String? iCon}) {
  return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              DartHelper.isNullOrEmpty(iCon) ? emptyBox():
              Row(
                children: [
                  SvgPicture.asset(iCon!,
                      fit: BoxFit.contain,
                      height: 28),
                  getSpace(0,10),
                ],
              ),
              mainText != null
                  ? Text(mainText,
                  style: AppStyle.theme16BoldTextStyle.copyWith(fontSize: 22))
                  :emptyBox(),
            ],
          ),
          const SizedBox(height: 10),
          RichText(text: TextSpan(children: [
            TextSpan(text: subText ?? "", style: AppStyle.grey16MediumTextStyle),
            WidgetSpan(child: Directionality(
              textDirection: TextDirection.ltr,
              child: Text(value ?? "", style: AppStyle.grey16MediumTextStyle),
            ))
          ]))
        ],
      ));
}

Widget subAndMainTextContainer({String? mainText, String? subText}) {
  return Container(
      padding: const EdgeInsets.only(left: 20, bottom: 20, top: 30, right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          subText != null
              ? Text(subText, style: AppStyle.grey16MediumTextStyle)
              : Container(),
          const SizedBox(height: 15),
          mainText != null
              ? Text(mainText,
                  style: AppStyle.theme16BoldTextStyle.copyWith(fontSize: 24,
                      color: AppColors.dark1))
              : Container(),
        ],
      ));
}

Widget infoTextContainer(
    {String? text, String? icon, double? horiPad, double? vertiPad}) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: horiPad ?? 0, vertical: vertiPad ?? 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          DartHelper.isNullOrEmpty(icon) ? emptyBox() :
          SvgPicture.asset(icon!,
              fit: BoxFit.contain, width: 15.0, height: 15.0),
          const SizedBox(width: 15),
          text != null
              ? Expanded(
                  child: Text(text,
                      style: AppStyle.grey16MediumTextStyle
                          .copyWith(fontSize: 13)))
              : Container(),
        ],
      ));
}

Widget imageTextContainer(
    {String? text,
    String? image,
    double? horiPad,
    double? vertiPad,
    double? size,
    bool? align = false,
    Color? imgColor,
    bool needDecoration = false,
    bool? isPng = false, bool? arrow = false}) {
  return Container(
      padding: EdgeInsets.symmetric(horizontal: horiPad ?? 0, vertical: vertiPad ?? 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment:
            align == true ? MainAxisAlignment.center : MainAxisAlignment.start,
        children: [
          image != null ? Container(
            alignment: Alignment.center,
            height: size,
            width: size,
            decoration: needDecoration ?
            const BoxDecoration(
              color: AppColors.light2,
              shape: BoxShape.circle
            ):null,
            child: Padding(
              padding: EdgeInsets.all(needDecoration ? 5:0),
              child: isPng == true
                  ? Image.asset(
                image,
                width: size!,
                height: size,
                color: imgColor,
                fit: BoxFit.contain,
              )
                  : SvgPicture.asset(
                image,
                fit: BoxFit.contain,
                width: size!,
                height: size,
                color: imgColor,
              ),
            ),
          ) : Container(),
          const SizedBox(width: 20),
          text != null
              ? Expanded(
                  child: Text(text, style: AppStyle.theme16MediumTextStyle.copyWith(height: 1.8)))
              : Container(),

          arrow == true ? const Icon(
            Icons.arrow_forward_ios_rounded,
            size: 12,
            color: Color(0xFF292751)) : Container()
        ],
      ));
}

Widget nationalIdContainer(
    {double? verticalPad, File? image, String? imageURL}) {
  return Container(
    height: 220,
    margin: EdgeInsets.symmetric(horizontal: 20, vertical: verticalPad!),
    padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
    decoration: const BoxDecoration(
      boxShadow: <BoxShadow>[
        BoxShadow(
            color: AppColors.shadowColor,
            blurRadius: 5.0,
            spreadRadius: 2,
            offset: Offset(0.2, 0.2))
      ],
      borderRadius: BorderRadius.all(
        Radius.circular(16.0),
      ),
      color: Colors.white,
    ),
    child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: imageURL != null
            ? Image.network(
                imageURL,
                height: 210,
                width: double.infinity,
                fit: BoxFit.fill,
              )
            : Image.file(
                image!,
                height: 210,
                width: double.infinity,
                fit: BoxFit.fitWidth,
              )),
  );
}

Widget profileListTabItems(
    {String? text,
    String? image,
    double? horiPad,
    double? vertiPad,
    double? size,
    bool? align = false,
    Function()? onTap}) {
  return InkWell(
    onTap: onTap,
    child: Container(
      padding: EdgeInsets.symmetric(horizontal: horiPad!, vertical: vertiPad!),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              SvgPicture.asset(image!,
                  fit: BoxFit.contain, width: size!, height: size),
              const SizedBox(width: 20),
              text != null
                  ? Text(text, style: AppStyle.theme16SemiBoldTextStyle)
                  : Container(),
            ],
          ),
          const Icon(
            Icons.arrow_forward_ios_outlined,
            size: 16,
            color: AppColors.subTextColor,
          )
        ],
      ),
    ),
  );
}

Widget radioInActive() {
  return Container(
      height: 22,
      width: 22,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.subTextColor),
        borderRadius: const BorderRadius.all(
          Radius.circular(100.0),
        ),
        color: AppColors.shadowColor,
      ));
}

Widget radioActive() {
  return Container(
      height: 22,
      width: 22,
      padding: const EdgeInsets.all(2),
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.subTextColor),
        borderRadius: const BorderRadius.all(
          Radius.circular(100.0),
        ),
      ),
      child: Container(
          height: 22,
          width: 22,
          decoration: BoxDecoration(
            border: Border.all(color: AppColors.subTextColor),
            borderRadius: const BorderRadius.all(
              Radius.circular(100.0),
            ),
            color: AppColors.themePrimaryColor,
          )));
}

Widget sheetStand({Widget? child}) {
  return Stack(
    children: [
      Container(
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: AppColors.dropShadow.withOpacity(0.10),
                  blurRadius: 40.0,
                  offset: const Offset(0, 10))
            ],
            border: Border.all(color: AppColors.shadowColor, width: 0.5),
            borderRadius:
            const BorderRadius.all(Radius.circular(16.0)),
            color: Colors.white),
        child: child,
      ),
      Container(
        height: 20,
        margin: const EdgeInsets.symmetric(vertical: 20),
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.shadowColor, width: 5),
          borderRadius: const BorderRadius.all(
            Radius.circular(16.0),
          ),
          color: AppColors.subTextColor.withOpacity(0.5),
        ),
      ),
      Positioned(
        top: 10,
        left: 0,
        right: 0,
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          height: 10,
          color: Colors.white,
        ),
      )
    ],
  );
}

Widget noDataHandler(
    {String? asset,
    String? text,
    bool? hasButton,
    Function()? onTap,
    String? buttonText}) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 180),
    child: Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(asset!, width: 90, height: 90, fit: BoxFit.cover),
          getSpace(15, 0),
          Text(text!, style: AppStyle.grey16MediumTextStyle),
          getSpace(25, 0),
          hasButton == true
              ? SubmitButtonTheme(
                  horipad: 55,
                  onTap: onTap,
                  buttonColor: AppColors.buttonColor,
                  textColor: Colors.white,
                  buttonText: buttonText)
              : Container()
        ],
      ),
    ),
  );
}


languageChangingWidget({required BuildContext context, Function? onTap}){
  return InkWell(
    onTap: () {
      // showModalBottomSheet(
      //     backgroundColor: Colors.transparent,
      //     context: context,
      //     isScrollControlled: true,
      //     builder: (BuildContext context) {
      //       return ChangeLanguage(
      //           needRestart: false,
      //           fromOnboarding: true,
      //           context: context,
      //           onTap: onTap);
      //     });
    },
    child: Container(
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              AppAssets.appLang,
              fit: BoxFit.contain,
              height: 19.5,
              width: 19.5,
              color: AppColors.shadowColor,
            ),
            getSpace(0, 2),
            Text('EN',
                style: AppStyle.white16MediumTextStyle),
            getSpace(0, 2),
            Image.asset(
              AppAssets.dropdownArrow,
              color: AppColors.shadowColor,
              height: 16,
              width: 8,
            )
          ],
        )),
  );
}

Widget step1Indicator() {
  return Row(
    children: [
      stepperText('1/3'),
      getSpace(0, 7),
      stepperDot(),
      getSpace(0, 7),
      stepperDot()
    ],
  );
}

Widget step2Indicator() {
  return Row(
    children: [
      stepperDot(),
      getSpace(0, 7),
      stepperText('2/3'),
      getSpace(0, 7),
      stepperDot()
    ],
  );
}

Widget step3Indicator() {
  return Row(
    children: [
      stepperDot(),
      getSpace(0, 7),
      stepperDot(),
      getSpace(0, 7),
      stepperText('3/3')
    ],
  );
}

Widget stepperText(String text) {
  return Container(
    height: 16,
    padding: const EdgeInsets.symmetric(horizontal: 6),
    decoration: const BoxDecoration(
        color: AppColors.dark1,
        borderRadius: BorderRadius.all(Radius.circular(8))),
    child: Text(
      text,
      style: AppStyle.black16SemiBoldTextStyle
          .copyWith(fontSize: 13, color: Colors.white),
    ),
  );
}

Widget stepperDot() {
  return Container(
    height: 6,
    width: 6,
    decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        color: AppColors.light0),
  );
}