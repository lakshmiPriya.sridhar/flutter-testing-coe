import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CommonNumericTextField extends StatelessWidget {
  final TextEditingController? controller;
  final bool? autoFocus;
  final String? hintText;
  final String? errorText;
  final int? length;
  final String? labelText;
  const CommonNumericTextField(
      {Key? key,
        this.controller,
        this.autoFocus,
        this.hintText,
        this.errorText,
        this.length,
        this.labelText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: AppColors.dropShadow.withOpacity(0.10),
                  blurRadius: 40.0,
                  offset: const Offset(0, 10))
            ],
            border: Border.all(color: AppColors.shadowColor, width: 0.5),
            borderRadius:
            const BorderRadius.all(Radius.circular(16.0)),
            color: Colors.transparent),
        child: TextFormField(
          controller: controller,
          autofocus: autoFocus!,
          textAlign: TextAlign.start,
          decoration: InputDecoration(
            filled: true,
            hintText: hintText,
            hintStyle: AppStyle.grey16MediumTextStyle.copyWith(
                color: AppColors.textLightGreyColor.withOpacity(0.5)),
            labelText: labelText ?? "",
            labelStyle: AppStyle.black16MediumTextStyle.copyWith(
                fontSize: 14,
                color: AppColors.textLightGreyColor.withOpacity(0.5)),
            fillColor: Colors.white,
            contentPadding:
            const EdgeInsets.symmetric(horizontal: 25, vertical: 18),
            isDense: true,
          ),
          keyboardType: TextInputType.number,
          textInputAction: TextInputAction.done,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          style: AppStyle.black16RegularTextStyle.copyWith(fontSize: 18),
          cursorColor: AppColors.themePrimaryColor,
          cursorWidth: 1.0,
          onChanged: (String val) {},
          validator: (String? value) {
              if (value!.isEmpty) {
                return errorText;
              } else if(length != null && value.length != length) {
                return 'Error! Invalid input';
              } else{
                return null;
              }
          },
        ));
  }
}
