import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';
import 'package:flutter_test_coe/views/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class BtmNavBar extends StatefulWidget {
  const BtmNavBar({Key? key}) : super(key: key);

  @override
  State<BtmNavBar> createState() => _BtmNavBarState();
}

class _BtmNavBarState extends State<BtmNavBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      width: MediaQuery.of(context).size.width,
      height: 65,
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
              color: AppColors.shadowColor,
              blurRadius: 40.0)
        ],
        color: Colors.white,
      ),
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              getTabIcon(
                onTap: () {
                  Navigator.of(context).popUntil((route) => route.isFirst);
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(
                          builder: (context) => const HomePage(index: 0,)));
                  },
                  index: 0,
                  icon: AppAssets.homeGrey,
                  text: 'home'),
              getTabIcon(
                  onTap: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                            builder: (context) => const HomePage(index: 1,)));
                  },
                  index: 1,
                  icon: AppAssets.buyGrey,
                  text: 'buy'),
              getTabIcon(
                  onTap: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                            builder: (context) => const HomePage(index: 2,)));
                  },
                  index: 2,
                  icon: AppAssets.purchaseGrey,
                  text: 'purchase'),
              getTabIcon(
                  onTap: () {
                    Navigator.of(context).popUntil((route) => route.isFirst);
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                            builder: (context) => const HomePage(index: 3,)));
                  },
                  index: 3,
                  icon: AppAssets.userProfile,
                  text: 'profile'),
            ],
          ),
          Positioned(
            top: 0, right: 0,
              child: Container(
            height: 2, width: 105,
            color: AppColors.mainTextColor,
          ))
        ],
      ),
    );
  }

  Widget getTabIcon(
      {String? icon, String? text, int? index, Color? color, Function()? onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30),
          color: color,
          height: double.infinity,
          alignment: Alignment.center,
          child: Tab(
              iconMargin: const EdgeInsets.all(0.0),
              child: Column(
                children: [
                  SvgPicture.asset(
                    icon!,
                    fit: BoxFit.contain,
                    width: 20.0,
                    height: 20.0,
                  ),
                  getSpace(5, 0),
                  Text(text!, style: AppStyle.black16BoldTextStyle.copyWith(fontSize: 13, color: text == "profile" ? AppColors.themePrimaryColor : AppColors.subTextColor),),
                ],
              )
          )),
    );
  }
}
