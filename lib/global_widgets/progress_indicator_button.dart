import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter/material.dart';

class ProgressIndicatorButton extends StatelessWidget {
  final Color? buttonColor;
  final Color? borderColor;
  final double? horipad;
  final double? vertiPad;
  const ProgressIndicatorButton({Key? key, required this.buttonColor, this.borderColor, this.horipad, this.vertiPad}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Container(
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16.0),
          )),
      padding: EdgeInsets.symmetric(horizontal: horipad ?? 20, vertical: vertiPad ?? 20),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(
            Radius.circular(16.0),
          ),
          color: buttonColor ?? AppColors.themePrimaryColor,
        ),
        padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 0),
        alignment: Alignment.center,
        child: const CircularProgressIndicator(
          strokeWidth: 2,
          backgroundColor: Colors.grey,
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
      ),
    );
  }
}

