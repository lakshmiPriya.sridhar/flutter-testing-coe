String capFirstLetter(String text) {
  if (text == "") {
    return "";
  }
  return text[0].toUpperCase() + text.substring(1);
}

String capFirstLetterOfEach(String text) {
  if (text == "") {
    return "";
  }
  return text.split(" ").map((str) {
    if (str.trim().isNotEmpty) {
      return str.trim().substring(0, 1).toUpperCase() + str.trim().substring(1);
    }
  }).join(" ");
}

String removeDash(String text) {
  if (text == "") {
    return "";
  }
  return text.split("_").map((str) {
    if (str.trim().isNotEmpty) {
      return str.trim().substring(0, 1).toUpperCase() + str.trim().substring(1);
    }
  }).join(" ");
}
