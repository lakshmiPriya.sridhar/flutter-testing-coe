library theme;

import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter/material.dart';

final ThemeData themeData = ThemeData(
    errorColor: AppColors.errorTextColor,
    backgroundColor: Colors.white,
    primaryColor: AppColors.themePrimaryColor,
    scaffoldBackgroundColor: AppColors.bgWhiteColor,
    accentColor: AppColors.themePrimaryColor,
    buttonTheme: const ButtonThemeData(
      buttonColor: AppColors.themePrimaryColor,
      textTheme: ButtonTextTheme.primary,
    ),
    sliderTheme: const SliderThemeData(
        showValueIndicator: ShowValueIndicator.always
    ),
    inputDecorationTheme: InputDecorationTheme(
        filled: true,
        fillColor: AppColors.shadowColor,
        hintStyle: AppStyle.black16RegularTextStyle,
        errorStyle: AppStyle.black16RegularTextStyle.copyWith(fontSize: 14, color: AppColors.errorTextColor),
        contentPadding: const EdgeInsets.symmetric(horizontal: 5, vertical: 22.5),
        isDense: true,
        border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(16)),
            borderSide:
            BorderSide(width: 0.9, color: AppColors.shadowColor.withOpacity(0.5))),
        errorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            borderSide: BorderSide(
                width: 0.9, color: AppColors.errorBorderColor)),
        enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(16)),
            borderSide:
            BorderSide(width: 0.9, color: AppColors.shadowColor.withOpacity(0.5))),
        focusedBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            borderSide:
            BorderSide(width: 0.9, color: AppColors.themePrimaryColor)),
        focusedErrorBorder: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(16)),
            borderSide:
            BorderSide(width: 0.9, color: AppColors.errorBorderColor,),),
    ),
    primarySwatch: MaterialColor(0XFF363A7C, color),
    iconTheme: const IconThemeData(color: AppColors.shadowColor, size: 24.0));
Map<int, Color> color = {
  50: const Color.fromRGBO(136, 14, 79, .1),
  100: const Color.fromRGBO(136, 14, 79, .2),
  200: const Color.fromRGBO(136, 14, 79, .3),
  300: const Color.fromRGBO(136, 14, 79, .4),
  400: const Color.fromRGBO(136, 14, 79, .5),
  500: const Color.fromRGBO(136, 14, 79, .6),
  600: const Color.fromRGBO(136, 14, 79, .7),
  700: const Color.fromRGBO(136, 14, 79, .8),
  800: const Color.fromRGBO(136, 14, 79, .9),
  900: const Color.fromRGBO(136, 14, 79, 1),
};