import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';

class BuyTabPage extends StatefulWidget {
  const BuyTabPage({Key? key}) : super(key: key);

  @override
  State<BuyTabPage> createState() => _BuyTabPageState();
}

class _BuyTabPageState extends State<BuyTabPage> {
  File? imageFileCamera;
  File? imageFileGallery;
  File? file;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
                child: imageFileCamera == null
                    ? TextButton(
                        key: const Key('upload_image_camera'),
                        onPressed: () async {
                          checkForCameraPermission(context, 'CAMERA',
                              (File value) async {
                            setState(() {
                              imageFileCamera = value;
                            });
                          });
                        },
                        child: const Text('Upload Image from Camera'),
                      )
                    : Image.file(imageFileCamera!)),
            getSpace(30, 0),
            Container(
                child: imageFileGallery == null
                    ? TextButton(
                        key: const Key('upload_image_gallery'),
                        onPressed: () async {
                          checkForCameraPermission(context, 'GALLERY',
                              (File value) async {
                            setState(() {
                              imageFileGallery = value;
                            });
                          });
                        },
                        child: const Text('Upload Image from Gallery'),
                      )
                    : Image.file(imageFileGallery!)),
            getSpace(30, 0),
            Container(
              child: file == null
                  ? TextButton(
                      key: const Key('upload_file'),
                      onPressed: () async {
                        checkForStoragePermission(context, false, (value) async {
                          setState(() {
                            file = value[0];
                          });
                        });
                      },
                      child: const Text('Upload file from Storage'))
                  : Image.file(file!),
            )
          ],
        ),
      ),
    );
  }
}
