import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter_test_coe/global_widgets/text_helper.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/connector/auth/auth_connector.dart';
import 'package:flutter_test_coe/views/auth/login_page.dart';
import 'package:getwidget/getwidget.dart';

class HomeTabPage extends StatefulWidget {
  const HomeTabPage({Key? key}) : super(key: key);

  @override
  State<HomeTabPage> createState() => _HomeTabPageState();
}

class _HomeTabPageState extends State<HomeTabPage> {
  String itemsString = "";

  @override
  Widget build(BuildContext context) {

    return AuthConnector(
        builder: (BuildContext authContext, AuthViewModel authModel) {
              return SafeArea(
                child: Scaffold(
                  backgroundColor: Colors.white,
                  body: ListView(
                    key: const Key('scroll_view'),
                    children: [
                      Stack(
                      children: [
                        Container(
                            height: 300,
                            color: Colors.white),
                        Container(
                            height: 230,
                            decoration: const BoxDecoration(
                                image: DecorationImage(
                                    scale: 1,
                                    fit: BoxFit.cover,
                                    image: AssetImage(
                                        AppAssets.homePageBackground))),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20, vertical: 30),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      child: Text(
                                          "Hello! ${capFirstLetterOfEach(authModel.currentUser.firstName ?? "")}",
                                          style: AppStyle.theme16BoldTextStyle
                                              .copyWith(fontSize: 24)),
                                    ),
                                    GFIconBadge(
                                      padding: const EdgeInsets.all(0),
                                        child: GFIconButton(
                                          onPressed: () {
                                          },
                                          icon: Image.asset(
                                              AppAssets.notification,
                                              height: 25,
                                              width: 25),
                                          type: GFButtonType.transparent,
                                        ),
                                        position: GFBadgePosition.topEnd(
                                            top: 3, end: 4),
                                        counterChild: Container()
                                        ),
                                  ],
                                ),
                                getSpace(15, 0),
                              ],
                            )),
                      ],
                    ),
                      Container(height: 1000, color: AppColors.yellowColor,),
                      TextButton(
                          key: const Key('logout_button'),
                          onPressed: () {
                          authModel.logOut();
                          Navigator.pushAndRemoveUntil(context,
                              MaterialPageRoute(builder: (BuildContext context) => const LoginPage()),
                                  (Route<dynamic> route) => false
                          );
                        }, child: Text('Logout'))
                    ],
                  ),
                ),
              );

    });
  }
}
