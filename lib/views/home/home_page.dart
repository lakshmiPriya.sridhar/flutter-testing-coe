import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';
import 'package:flutter_test_coe/views/home/buy_tab/buy_tab_page.dart';
import 'package:flutter_test_coe/views/home/home_tab/home_tab_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/connector/auth/auth_connector.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  final int? index;

  const HomePage({Key? key, this.index}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;
  int? _selectedIndex;

  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 4);
    _tabController?.index = widget.index ?? 0;
    _selectedIndex = widget.index ?? 0;
    super.initState();
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AuthConnector(
        builder: (BuildContext authContext, AuthViewModel authModel) {
          return SafeArea(
            child: Scaffold(
                backgroundColor: Colors.white,
                bottomNavigationBar: _builtBtmNavBar(
                    authModel: authModel),
                body: TabBarView(
                    physics: const NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: <Widget>[
                  const HomeTabPage(),
                      BuyTabPage(),
                      Container(),
                      Container(),
                ])),
          );
    });
  }

  Widget _builtBtmNavBar({
    AuthViewModel? authModel,
  }) {

    return Container(
      width: MediaQuery.of(context).size.width,
      height: 65,
      decoration: const BoxDecoration(
        boxShadow: [BoxShadow(color: AppColors.shadowColor, blurRadius: 40.0)],
        color: Colors.white,
      ),
      child: Material(
        color: Colors.transparent,
        child: TabBar(
          onTap: (int index) => onTabTapped(
            index: index,
            authModel: authModel,
          ),
          indicatorColor: AppColors.themePrimaryColor,
          labelPadding: EdgeInsets.zero,
          indicator: const UnderlineTabIndicator(
            borderSide: BorderSide(color: AppColors.mainTextColor, width: 1.0),
            insets: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 64.0),
          ),
          controller: _tabController,
          unselectedLabelColor: AppColors.subTextColor,
          labelColor: AppColors.themePrimaryColor,
          unselectedLabelStyle:
              AppStyle.black16BoldTextStyle.copyWith(fontSize: 13),
          labelStyle: AppStyle.black16BoldTextStyle.copyWith(fontSize: 13),
          tabs: <Widget>[
            getTabIcon(
                index: 0,
                authModel: authModel!,
                icon: _selectedIndex == 0 ? AppAssets.home : AppAssets.homeGrey,
                text: 'Home'),
            getTabIcon(
                index: 1,
                authModel: authModel,
                icon: _selectedIndex == 1 ? AppAssets.buy : AppAssets.buyGrey,
                text: 'Buy'),
            getTabIcon(
                index: 2,
                authModel: authModel,
                icon: _selectedIndex == 2
                    ? AppAssets.purchase
                    : AppAssets.purchaseGrey,
                text: 'Purchases'),
            getTabIcon(
                index: 3,
                authModel: authModel,
                icon: _selectedIndex == 3
                    ? AppAssets.userProfile
                    : AppAssets.userProfileGrey,
                text: 'Profile'),
          ],
        ),
      ),
    );
  }

  Widget getTabIcon(
      {String? icon,
      String? text,
      int? index,
      AuthViewModel? authModel,
      Color? color}) {
    return Container(
        color: color,
        height: double.infinity,
        // height: 90,
        alignment: Alignment.center,
        child: Tab(
            iconMargin: const EdgeInsets.all(0.0),
            height: 90,
            child: Column(
              children: [
                index == _selectedIndex ? getSpace(5, 0) : getSpace(10, 0),
                index == _selectedIndex ? SvgPicture.asset(
                  icon!,
                  fit: BoxFit.contain,
                  height: 28.0,
                ) : SvgPicture.asset(
                  icon!,
                  fit: BoxFit.contain,
                  height: 20.0,
                ),
                index == _selectedIndex ? getSpace(1, 0) : getSpace(5, 0),
                // Expanded(child: Text(text!)),
                Text(text!)
              ],
            )));
  }

  void onTabTapped({
    int? index,
    AuthViewModel? authModel,
  }) {
    setState(() {
      _selectedIndex = index;
      _tabController?.index = index!;
    });
    if (index == 0) {
    } else if (index == 1) {
    } else if (index == 2) {
    } else if (index == 3) {}
  }
}
