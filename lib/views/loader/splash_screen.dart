import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter_test_coe/global_widgets/common_containers.dart';
import 'package:flutter_test_coe/global_widgets/submit_button.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';
import 'package:flutter_test_coe/views/auth/login_page.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final PageController _controller = PageController(keepPage: false);
  int position = 0;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    Size dim = MediaQuery.of(context).size;

    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppAssets.backgroundImage),
              fit: BoxFit.fill
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                      padding: EdgeInsets.only(top: dim.height*0.05),
                      child: Row(
                        children: [
                          Expanded(child: emptyBox()),
                          Expanded(child: SvgPicture.asset(AppAssets.logoWithName, height: 31)),
                          Expanded(
                            child: languageChangingWidget(context: context),
                          )
                        ],
                      ),),
                  SizedBox(
                    height: dim.height*0.65,
                    child: NotificationListener<OverscrollNotification>(
                      onNotification: (OverscrollNotification notification) {
                        if(notification.overscroll < 15) {
                          return false;
                        }
                        setState(() {
                          position = 0;
                          _controller.jumpToPage(position);
                        });
                        return true;
                      },
                      child: PageView(
                          controller: _controller,
                          onPageChanged: (int val) {
                            setState(() {
                              position = val;
                            });
                          },
                          children: [
                            titleWithImage('Shop', 'It\'s yours', '', AppAssets.shop, dim),
                            titleWithImage('Pay', 'It\'s Easy', '', AppAssets.pay, dim),
                            titleWithImage('Save', 'It\'s all about trust', '', AppAssets.save, dim)
                          ]
                      ),
                    ),
                  ),
                  DotsIndicator(
                    dotsCount: 3,
                    position: position.toDouble(),
                    decorator: DotsDecorator(
                      spacing: const EdgeInsets.all(4),
                      color: Colors.white.withOpacity(0.3),
                      size: const Size(6,6),
                      activeColor: Colors.white,
                      activeSize: const Size(16,6),
                      activeShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: dim.height*0.05),
                    child: SubmitButtonTheme(
                      vertiPad: 0,
                      onTap: () => Navigator.of(context).push(
                          MaterialPageRoute(builder: (context) => const LoginPage())),
                      buttonColor: Colors.white,
                      buttonText: 'Get Started',
                      textColor: const Color(0xFF4537A0),
                    ),
                  ),
                ],
              ),
        ),
      ),
    );
  }
}

Widget titleWithImage(String title, String subtitle, String text, String asset, Size dim) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Column(
        children: [
          Text(title, style: AppStyle.black16BoldTextStyle.copyWith(fontSize: 48, color: Colors.white)),
          getSpace(10, 0),
          Text(subtitle, style: AppStyle.black16MediumTextStyle.copyWith(fontSize: 20, color: Colors.white)),
        ],
      ),
      SvgPicture.asset(asset, height: dim.height*0.32, fit: BoxFit.contain),
      Text(text, style: AppStyle.black16MediumTextStyle.copyWith(fontSize: 18, color: Colors.white),
        textAlign: TextAlign.center,)
    ],
  );
}