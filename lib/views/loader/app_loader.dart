import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter/material.dart';

class AppLoader extends StatefulWidget {
  final bool? timedDisplay;
  final dynamic route;

  const AppLoader({Key? key, this.timedDisplay = false, this.route}) : super(key: key);
  @override
  _AppLoaderState createState() => _AppLoaderState();
}

class _AppLoaderState extends State<AppLoader> {

  @override
  void initState() {
    super.initState();
    widget.timedDisplay != null && widget.timedDisplay == true
        ? Future.delayed(const Duration(milliseconds: 10)).then((value) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => widget.route));
    }) : null;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.themePrimaryColor,
        body: Stack(
          children: [
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              alignment: Alignment.center,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      scale: 4,
                      image: AssetImage(AppAssets.whiteLogo)
                  )
              ),
            )
          ],
        ),
      ),
    );
  }
}


