import 'package:flutter_test_coe/connector/auth/auth_connector.dart';
import 'package:flutter_test_coe/views/home/home_page.dart';
import 'package:flutter_test_coe/views/loader/app_loader.dart';
import 'package:flutter_test_coe/views/loader/splash_screen.dart';
import 'package:flutter/material.dart';

class InitPage extends StatelessWidget {
  const InitPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AuthConnector(
      builder: (BuildContext c, AuthViewModel model) {
        if (model.isInitializing == true) {
          return const AppLoader();
        }
        return model.currentUser.maxPurchasePower == null
            ? const SplashScreen()
            : const HomePage();
      },
    );
  }
}
