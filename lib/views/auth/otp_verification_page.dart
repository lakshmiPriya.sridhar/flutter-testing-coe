import 'dart:async';
import 'package:flutter_test_coe/core/theme/app_assets.dart';
import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/core/theme/app_styles.dart';
import 'package:flutter_test_coe/global_widgets/common_containers.dart';
import 'package:flutter_test_coe/global_widgets/progress_indicator_button.dart';
import 'package:flutter_test_coe/global_widgets/submit_button.dart';
import 'package:flutter_test_coe/global_widgets/toast_helper.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';
import 'package:flutter_test_coe/views/auth/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/connector/auth/auth_connector.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:otp_timer_button/otp_timer_button.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:flutter/services.dart';

class OtpVerification extends StatefulWidget {
  final String? mobileNo;
  final String? actualMobileNo;

  const OtpVerification(
      {Key? key, this.mobileNo, this.actualMobileNo})
      : super(key: key);

  @override
  State<OtpVerification> createState() => _OtpVerificationState();
}

class _OtpVerificationState extends State<OtpVerification> {
  final GlobalKey<FormState> _otpKey = GlobalKey<FormState>();
  final TextEditingController _otp = TextEditingController();
  final FocusNode focusNode = FocusNode();

  Timer? _timer;
  int _counter = 0;
  int duration = 30;
  ButtonState _state = ButtonState.timer;

  @override
  void initState() {
    super.initState();
    _startTimer();
  }

  _startTimer() {
    _timer?.cancel();
    _state = ButtonState.timer;
    _counter = duration;
    setState(() {});
    _timer = Timer.periodic(
      const Duration(seconds: 1),
      (Timer timer) {
        if (_counter == 0) {
          _state = ButtonState.enable_button;
          setState(() {
            timer.cancel();
          });
        } else {
          setState(() {
            _counter--;
          });
        }
      },
    );
  }

  @override
  void dispose() {
    _timer?.cancel();
    // _otp.dispose();
    if(focusNode.hasFocus == true) {
      focusNode.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
      onWillPop: () async {
        await SystemNavigator.pop();
        return true;
      },
      child: AuthConnector(
          builder: (BuildContext authContext, AuthViewModel authModel) {
        return SafeArea(
          child: Scaffold(
              backgroundColor: AppColors.bgWhiteColor,
              body: AbsorbPointer(
                absorbing: authModel.isLoading ?? false,
                child: Stack(
                  children: [
                    SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            appLogoContainer(context, fromOnboarding: true, enableContactUs: true,),
                            mainAndSubTextWithValueContainer(
                                    mainText: 'Verification',
                                    subText: 'Enter 4 digit code sent to',
                                    value: '${widget.mobileNo}',
                                    iCon: AppAssets.verification),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 30),
                              child: SizedBox(
                                height: 80,
                                child: KeyboardActions(
                                  config: keyboardActionsConfig(context, focusNode),
                                  child: Form(
                                      key: _otpKey,
                                      child: Directionality(
                                        textDirection: TextDirection.ltr,
                                        child: PinCodeTextField(
                                          key: const Key('otp_field'),
                                          keyboardType: TextInputType.number,
                                          focusNode: focusNode,
                                          appContext: context,
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          controller: _otp,
                                          enabled: true,
                                          length: 4,
                                          obscureText: false,
                                          cursorWidth: 1,
                                          cursorColor: AppColors.themePrimaryColor,
                                          textStyle: AppStyle.grey16RegularTextStyle
                                              .copyWith(
                                                  color: AppColors.textBlackColor),
                                          inputFormatters: <TextInputFormatter>[
                                            FilteringTextInputFormatter.digitsOnly
                                          ],
                                          textInputAction: TextInputAction.done,
                                          animationType: AnimationType.fade,
                                          pinTheme: PinTheme(
                                            selectedFillColor: AppColors.bgWhiteColor,
                                            activeColor: AppColors.greenColor,
                                            inactiveColor: AppColors.shadowColor,
                                            disabledColor: AppColors.shadowColor,
                                            selectedColor: AppColors.themePrimaryColor,
                                            shape: PinCodeFieldShape.box,
                                            borderRadius: BorderRadius.circular(16),
                                            fieldHeight: 64,
                                            fieldWidth: 64,
                                            activeFillColor: Colors.white,
                                          ),
                                          animationDuration:
                                              const Duration(milliseconds: 300),
                                          onChanged: (value) {},
                                          boxShadows: const [
                                            BoxShadow(
                                                color: AppColors.shadowColor,
                                                blurRadius: 60.0,
                                                offset: Offset(0, 1))
                                          ],
                                        ),
                                      )),
                                ),
                              ),
                            ),
                            _state == ButtonState.enable_button
                                ? InkWell(
                                    onTap: () {
                                      // _startTimer();
                                      //   authModel.resendOtpAction(false, {
                                      //     "customer": {
                                      //       "resend_otp_type": "M",
                                      //       "mobile_number": widget.actualMobileNo,
                                      //       "country_code_id": authModel.userCountryCode
                                      //     }
                                      //   });
                                    },
                                    child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                        child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SvgPicture.asset(AppAssets.resendOtp,
                                                  fit: BoxFit.contain,
                                                  width: 20.0,
                                                  height: 20.0),
                                              const SizedBox(width: 10),
                                              Text('Resend OTP',
                                                  style: AppStyle
                                                      .grey16RegularTextStyle)
                                            ])),
                                  )
                                : Padding(
                                    padding:
                                        const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                                    child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(AppAssets.resendOtp,
                                              fit: BoxFit.contain,
                                              width: 20.0,
                                              height: 20.0),
                                          const SizedBox(width: 10),
                                          Text(
                                              'Resend OTP in 00:${_counter.toString().padLeft(2, '0')}',
                                              style:
                                                  AppStyle.grey16RegularTextStyle)
                                        ])),
                            InkWell(
                                onTap: () {
                                    Navigator.of(context).pushReplacement(
                                        MaterialPageRoute(builder: (context) => const LoginPage()));
                                },
                                child: Padding(
                                    padding:
                                    const EdgeInsets.fromLTRB(20, 20, 20, 20),
                                    child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        children: [
                                          SvgPicture.asset(AppAssets.phoneSvg,
                                              fit: BoxFit.contain,
                                              width: 25.0,
                                              height: 25.0),
                                          const SizedBox(width: 10),
                                          Text('Change Mobile Number',
                                              style: AppStyle
                                                  .theme16SemiBoldTextStyle
                                                  .copyWith(
                                                  color: AppColors
                                                      .themePrimaryColor))
                                        ]))),
                            authModel.isLoading == true
                                ? const ProgressIndicatorButton(
                                buttonColor: AppColors.buttonColor)
                                : SubmitButtonTheme(
                                key: const Key('submit_otp'),
                                onTap: () async {
                                  FocusScope.of(context).unfocus();
                                  if (_otpKey.currentState!.validate()) {
                                      if (_otp.text.length != 4) {
                                        ToastHelper().getIntermittentToast(
                                            'Invalid OTP', context);
                                      } else {
                                        authModel.verifyMobileOtpAction({
                                          "customer": {
                                            "mobile_number":
                                            widget.actualMobileNo,
                                            "country_code_id": authModel.userCountryCode,
                                            "login_otp": _otp.text,
                                            "grant_type": "login_otp"
                                          }
                                        });
                                        // _otp.dispose();
                                      }
                                  }
                                },
                                buttonColor: AppColors.buttonColor,
                                textColor: Colors.white,
                                buttonText: 'Submit'),
                          ],
                        )),
                    globalLoader(authModel.isLoading == true)
                  ],
                ),
              )),
        );
      }),
    );
  }
}
