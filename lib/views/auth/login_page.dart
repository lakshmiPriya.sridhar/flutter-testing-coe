import 'dart:async';
import 'dart:io';

import 'package:flutter_test_coe/core/theme/app_colors.dart';
import 'package:flutter_test_coe/global_widgets/common_containers.dart';
import 'package:flutter_test_coe/global_widgets/progress_indicator_button.dart';
import 'package:flutter_test_coe/global_widgets/submit_button.dart';
import 'package:flutter_test_coe/global_widgets/widget_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_coe/connector/auth/auth_connector.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import '../../core/theme/app_assets.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _mobileNumber = TextEditingController();
  PhoneNumber number = PhoneNumber(isoCode: 'EG');
  final FocusNode focusNode = FocusNode();

  @override
  void dispose() {
    _mobileNumber.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return AuthConnector(
        builder: (BuildContext authContext, AuthViewModel authModel) {
      return SafeArea(
        child: Scaffold(
          backgroundColor: AppColors.bgWhiteColor,
          body: AbsorbPointer(
            absorbing: authModel.isLoading ?? false,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      appLogoContainer(context, fromOnboarding: true),
                      mainAndSubTextContainer(
                          mainText: 'Mobile Number',
                          subText: 'Please enter your mobile number',
                          iCon: AppAssets.mobile),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 30),
                        child: SizedBox(
                          height: 100,
                          child: KeyboardActions(
                            config: keyboardActionsConfig(context, focusNode),
                            child: Form(
                                autovalidateMode: AutovalidateMode.disabled,
                                key: _formKey,
                                child: Container(
                                    decoration: const BoxDecoration(
                                        boxShadow: <BoxShadow>[],
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(16.0)),
                                        color: Colors.transparent),
                                    child:

                                        ///testing
                                        InternationalPhoneNumberInput(
                                      focusNode: focusNode,
                                      key: const Key('mobile_number'),
                                      countries: authModel.countryCodeMeta
                                                      ?.length !=
                                                  null &&
                                              authModel.countryCodeMeta
                                                      ?.isNotEmpty ==
                                                  true
                                          ? List.generate(
                                              authModel.countryCodeMeta
                                                      ?.length ??
                                                  1,
                                              (index) =>
                                                  authModel
                                                      .countryCodeMeta?[index]
                                                      .flag ??
                                                  "")
                                          : ["EG"],
                                      selectorConfig: const SelectorConfig(
                                        selectorType:
                                            PhoneInputSelectorType.BOTTOM_SHEET,
                                        setSelectorButtonAsPrefixIcon: true,
                                        trailingSpace: false,
                                        leadingPadding: 20,
                                        useEmoji: true,
                                      ),
                                      onInputChanged: (PhoneNumber value) {
                                        setState(() {
                                          number = value;
                                        });
                                      },
                                      ignoreBlank: false,
                                      autoValidateMode:
                                          AutovalidateMode.disabled,
                                      textFieldController: _mobileNumber,
                                      formatInput: false,
                                      keyboardType: const TextInputType
                                          .numberWithOptions(),
                                      hintText:
                                          'Please enter your mobile number',
                                      validator: (String? value) {
                                        if (value != null &&
                                            value.isEmpty == true) {
                                          return getErrorMessage();
                                        } else {
                                          switch (number.dialCode ?? "+20") {
                                            case '+91':
                                              return RegExp(
                                                          r'^(?:(?:\+|0{0,2})91(\s*|[\-])?|[0]?)?([6789]\d{2}([ -]?)\d{3}([ -]?)\d{4})$')
                                                      .hasMatch(value!)
                                                  ? null
                                                  : getErrorMessage();

                                            case '+20':
                                              return RegExp(
                                                          r'^[0]?1[0125][0-9]{8}$')
                                                      .hasMatch(value!)
                                                  ? null
                                                  : getErrorMessage();

                                            case "+972":
                                              return RegExp(
                                                          r'^[0]?[5][0|2|3|4|5|9]{1}[-]{0,1}[0-9]{7}$')
                                                      .hasMatch(value!)
                                                  ? null
                                                  : getErrorMessage();

                                            case "+1":
                                              return RegExp(
                                                          r'^(1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s\-]?[\0-9]{3}[\s\-]?[0-9]{4}$')
                                                      .hasMatch(value!)
                                                  ? null
                                                  : getErrorMessage();

                                            default:
                                              return getErrorMessage();
                                          }
                                        }
                                      },
                                    ))),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                authModel.isLoading == true
                    ? const ProgressIndicatorButton(
                        buttonColor: AppColors.buttonColor)
                    : SubmitButtonTheme(
                        key: const Key('login_button'),
                        onTap: () async {
                          FocusScope.of(context).unfocus();
                          if (_formKey.currentState!.validate()) {
                            int? countryCodeId = authModel.countryCodeMeta
                                ?.where((countryCode) =>
                                    countryCode.flag == number.isoCode)
                                .toList()
                                .first
                                .id;
                            authModel.saveUserCountryCode(countryCodeId);
                            authModel.requestMobileOtpAction(
                                _mobileNumber.text.trim(),
                                {
                                  "customer": {
                                    "mobile_number": _mobileNumber.text.trim(),
                                    "country_code_id": countryCodeId
                                  }
                                },
                                number.dialCode);
                          }
                        },
                        buttonColor: AppColors.buttonColor,
                        textColor: Colors.white,
                        buttonText: 'Send Confirmation Code'),
              ],
            ),
          ),
        ),
      );
    });
  }
}

String? getErrorMessage() {
  return "Error! Enter valid Mobile Number";
}
