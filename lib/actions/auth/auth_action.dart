import 'package:built_collection/built_collection.dart';
import 'package:flutter_test_coe/models/models.dart';
import 'package:flutter/material.dart';

class CheckForUserInPrefs {
  bool? needRestart = true;

  CheckForUserInPrefs({this.needRestart});
}

class SaveNeedRestart {
  bool? needRestart = true;

  SaveNeedRestart({this.needRestart});
}

class ChangeAppLanguage {
  final String? locale;

  ChangeAppLanguage({this.locale});
}

class SaveAppLanguage {
  final String? locale;

  SaveAppLanguage({this.locale});
}

class SetUpFireBaseListener {}

class LoginWithRefreshToken {
  ValueChanged<String>? callbackFunc;

  LoginWithRefreshToken({this.callbackFunc});
}

//********************************* get-user **********************************//
class GetUserDetails {
  final ValueChanged<AppUser>? callbackFunc;

  GetUserDetails({this.callbackFunc});
}

//********************************* save-user *********************************//
class SaveUser {
  SaveUser({this.userDetails});

  final AppUser? userDetails;
}

//********************************* request-mobile-otp ************************//
class RequestMobileOtp {
  final Map<String, dynamic>? bodyToApi;
  final String? mobileNo;
  final String? country;

  RequestMobileOtp({this.bodyToApi, this.mobileNo, this.country});
}

//********************************* verify-mobile-otp *************************//
class VerifyMobileOtp {
  final Map<String, dynamic>? bodyToApi;

  VerifyMobileOtp({this.bodyToApi});
}

//***************************** get-country-code-meta *********************//
class GetCountryCodeMeta{}

class SaveCountryCodeMeta{
  final BuiltList<CountryCode>? countryCodeMeta;

  SaveCountryCodeMeta({required this.countryCodeMeta});
}

class SaveUserCountryCode {
  final int? userCountryCode;

  SaveUserCountryCode({this.userCountryCode});
}

//***************************** log-out ***************************************//
class LogOutUser {}

//*********************** force-log-out ***************************************//
class ForceLogOutUser {
  ForceLogOutUser({required this.status, this.callbackFunc});

  final String? status;
  ValueChanged<String>? callbackFunc;

}

//**************************** manage loading status **************************//
class SetLoader {
  SetLoader(this.isLoading);

  final bool isLoading;
}

//**************************** manage initializer status **********************//
class SetInitializer {
  SetInitializer(this.isInitializing);

  final bool isInitializing;
}

//**************************** manage error message ***************************//
class SetErrorMessage {
  SetErrorMessage({required this.message});

  final String message;
}
